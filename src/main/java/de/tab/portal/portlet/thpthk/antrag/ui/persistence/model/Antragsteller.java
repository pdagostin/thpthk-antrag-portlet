package de.tab.portal.portlet.thpthk.antrag.ui.persistence.model;

import de.tab.portal.client.persistence.model.BaseEntity;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AdresseComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.PostfachComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.SpezifischeDatenComponentModel;

/**
 * @author idekic
 * @since 28-Jan-16.
 */
public class Antragsteller extends BaseEntity {
    public SpezifischeDatenComponentModel spezifischeDaten;
    public AdresseComponentModel adresse;
    public PostfachComponentModel postfach;

    public Antragsteller() {
    }
}
