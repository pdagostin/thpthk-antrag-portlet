package de.tab.portal.portlet.thpthk.antrag.ui.views;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.UIScope;
import de.tab.portal.client.katalog.model.KatalogKeyText;
import de.tab.portal.commons.KeyText;
import de.tab.portal.portlet.thpthk.antrag.ui.services.AsyncKatalogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Presenter, der die ganzen Kataloge als BeanItemContainer vorhält und bei der Initialisierung asynchron lädt.
 * Created by Senf on 29.02.2016.
 */
@Component
@UIScope
public class ThpthkKatalogPresenterImpl implements ThpthkKatalogPresenter {
	@Inject
	private AsyncKatalogService asyncKatalogService;

	private final Logger logger = LoggerFactory.getLogger(ThpthkKatalogPresenterImpl.class);

	private final BeanItemContainer<KeyText> landContainer = new BeanItemContainer<>(KeyText.class);
	private final BeanItemContainer<KeyText> rollenAnsprechpartnerContainer = new BeanItemContainer<>(KeyText.class);
	private final BeanItemContainer<KeyText> rechtsformContainer = new BeanItemContainer<>(KeyText.class);
	private final BeanItemContainer<KeyText> beguenstigtenEinordnungContainer = new BeanItemContainer<>(KeyText.class);
	private final BeanItemContainer<KeyText> brancheContainer = new BeanItemContainer<>(KeyText.class);
	private final BeanItemContainer<KeyText> foerderperiodeContainer = new BeanItemContainer<>(KeyText.class);

	@PostConstruct
	public void initKatalogs() {
		this.logger.error("init Katalog");

		final StopWatch katalogStopWatch = new StopWatch("Katalog");
		katalogStopWatch.start();
		final Future<List<KatalogKeyText>> landList = this.asyncKatalogService.getKatalogKeyText("Laenderschluessel");
		final Future<List<KatalogKeyText>> rollenAnsprechpartnerList = this.asyncKatalogService.getKatalogKeyText("RollenAnsprechpartner");
		final Future<List<KatalogKeyText>> rechtsformList = this.asyncKatalogService.getKatalogKeyText("Rechtsform");
		final Future<List<KatalogKeyText>> beguenstigtenEinordnungList =
				this.asyncKatalogService.getKatalogKeyText("BeguenstigtenEinordnung");
		final Future<List<KatalogKeyText>> brancheList = this.asyncKatalogService.getKatalogKeyText("branche");
		final Future<List<KatalogKeyText>> foerderperiodeList = this.asyncKatalogService.getKatalogKeyText("FoerderperiodeOP");

		this.landContainer.removeAllItems();

		try {
			this.landContainer.addAll(landList.get());
			this.rollenAnsprechpartnerContainer.addAll(rollenAnsprechpartnerList.get());
			this.rechtsformContainer.addAll(rechtsformList.get());
			this.beguenstigtenEinordnungContainer.addAll(beguenstigtenEinordnungList.get());
			this.brancheContainer.addAll(brancheList.get());
			this.foerderperiodeContainer.addAll(foerderperiodeList.get());
		} catch (final InterruptedException | ExecutionException e) {
			this.logger.error(e.getMessage(), e);
		}

		katalogStopWatch.stop();
		this.logger.info(katalogStopWatch.prettyPrint());
	}

	@Override
	public BeanItemContainer<KeyText> getLandKatalog() {
		return this.landContainer;
	}

	@Override
	public BeanItemContainer<KeyText> getBeguenstigtenEinordnungKatalog() {
		return this.beguenstigtenEinordnungContainer;
	}

	@Override
	public BeanItemContainer<KeyText> getBrancheKatalog() {
		return this.brancheContainer;
	}

	@Override
	public BeanItemContainer<KeyText> getRechtsformKatalog() {
		return this.rechtsformContainer;
	}

	@Override
	public BeanItemContainer<KeyText> getRollenAnsprechpartnerKatalog() {
		return this.rollenAnsprechpartnerContainer;
	}

    @Override
    public BeanItemContainer<KeyText> getFoerderperiodeKatalog() {
        return this.foerderperiodeContainer;
    }
}