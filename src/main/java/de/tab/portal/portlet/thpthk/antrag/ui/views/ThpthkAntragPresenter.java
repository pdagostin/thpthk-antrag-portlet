package de.tab.portal.portlet.thpthk.antrag.ui.views;

import static org.slf4j.LoggerFactory.getLogger;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;

import de.tab.portal.client.workflow.entity.ws.WsAufgabe;
import de.tab.portal.client.workflow.service.WorkflowClientService;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AdresseComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AktuellerBewilligungsstandComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AnsprechpartnerMeldestelleComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AufbewahrungsortDerBelegeComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AufteilungDerAusgabenAufJahreComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.BewilligteAusgabenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.BrancheComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.FinanzierungComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.GeplanteAusgabenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.IndikatorenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.KontaktComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.OrtComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.PostfachComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.SpezifischeDatenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.StammdatenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.VorhabenszeitraumComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.components.AdresseComponent;
import de.tab.portal.portlet.thpthk.antrag.ui.persistence.model.ThpthkVorhaben;
import de.tab.portal.portlet.thpthk.antrag.ui.services.VorhabenService;
import de.tab.security.service.SecurityService;
import de.tab.vaadin.components.CustomBeanItem;
import de.tab.vaadin.components.TabBeanItemContainer;
import de.tab.vaadin.layout.VorhabenPresenterParameters;
import de.tab.vaadin.security.TabSecurityPermission;
import de.tab.vaadin.utils.CalculationUtil;

/**
 * Created by Vollmar on 18.06.2015.
 */
@SpringComponent
@UIScope
public class ThpthkAntragPresenter extends BaseVorhabenPresenter {

	private final Logger logger = getLogger(ThpthkAntragPresenter.class);
	/**
	 * Injections
	 */
	private final WorkflowClientService workflowClientService;

	private final SecurityService securityService;

	private final AdresseComponent.AdressenValidator adressenValidator;

	private final VorhabenService vorhabenService;

	/**
	 * CustomBeanItems
	 */
	private final CustomBeanItem<AdresseComponentModel> adresseBeanItem = new CustomBeanItem<>(new AdresseComponentModel());
	private final CustomBeanItem<PostfachComponentModel> postfachBeanItem = new CustomBeanItem<>(new PostfachComponentModel());
	private final CustomBeanItem<KontaktComponentModel> kontaktBeanItem = new CustomBeanItem<>(new KontaktComponentModel());
	private final CustomBeanItem<SpezifischeDatenComponentModel> spezifischeDatenBeanItem =
			new CustomBeanItem<>(new SpezifischeDatenComponentModel());
	private final CustomBeanItem<VorhabenszeitraumComponentModel> vorhabenszeitraumBeanItem =
			new CustomBeanItem<>(new VorhabenszeitraumComponentModel());
	private final CustomBeanItem<BrancheComponentModel> brancheBeanItem = new CustomBeanItem<>(new BrancheComponentModel());
	private final CustomBeanItem<AufbewahrungsortDerBelegeComponentModel> aufbewahrungsortBeanItem =
			new CustomBeanItem<>(new AufbewahrungsortDerBelegeComponentModel());
	private final CustomBeanItem<GeplanteAusgabenComponentModel> geplanteAusgabenBeanItem =
			new CustomBeanItem<>(new GeplanteAusgabenComponentModel());
	private final CustomBeanItem<AnsprechpartnerMeldestelleComponentModel> ansprechpartnerMeldestelleBeanItem =
			new CustomBeanItem<>(new AnsprechpartnerMeldestelleComponentModel());
	private final CustomBeanItem<StammdatenComponentModel> stammdatenBeanItem = new CustomBeanItem<>(new StammdatenComponentModel());
	private final CustomBeanItem<BewilligteAusgabenComponentModel> bewilligteAusgabenBeanItem =
			new CustomBeanItem<>(new BewilligteAusgabenComponentModel());
	private final CustomBeanItem<AktuellerBewilligungsstandComponentModel> aktuellerBewilligungsstandBeanItem =
			new CustomBeanItem<>(new AktuellerBewilligungsstandComponentModel());

	/**
	 * BeanItemContainers
	 */
	private final TabBeanItemContainer<IndikatorenComponentModel> indikatorContainer =
			new TabBeanItemContainer<>(IndikatorenComponentModel.class);

	private final TabBeanItemContainer<OrtComponentModel> ortDesVorhabensContainer =
			new TabBeanItemContainer<>(OrtComponentModel.class);

	private final TabBeanItemContainer<AufteilungDerAusgabenAufJahreComponentModel> aufteilungDerAusgabenAufJahreContainer =
			new TabBeanItemContainer<>(AufteilungDerAusgabenAufJahreComponentModel.class);

	private final TabBeanItemContainer<FinanzierungComponentModel> finanzierungContainer =
			new TabBeanItemContainer<>(FinanzierungComponentModel.class);

	private final TabBeanItemContainer<KontaktComponentModel> kontaktContainer =
			new TabBeanItemContainer<>(KontaktComponentModel.class);

	/**
	 * Containters for comboboxes
	 */
	private final TabBeanItemContainer<String> finanzierungJahrContainer =
			new TabBeanItemContainer<>(String.class);

	private final TabBeanItemContainer<String> aufteilungDerAusgabenAufJahreJahrContainer =
			new TabBeanItemContainer<>(String.class);

	@Inject
	public ThpthkAntragPresenter(final VorhabenPresenterParameters vorhabenPresenterParameters,
			final AdresseComponent.AdressenValidator adressenValidator,
			final VorhabenService vorhabenService,
			final SecurityService securityService,
			final WorkflowClientService workflowClientService) {
		super(vorhabenPresenterParameters);
		this.adressenValidator = adressenValidator;
		this.vorhabenService = vorhabenService;
		this.securityService = securityService;
		this.workflowClientService = workflowClientService;
	}

	@Override
	public TabSecurityPermission checkAccess() {
//		final String vorhabenId = VaadinPortletService.getCurrentRequest().getRequest().getParameter("vorhabenId");
//		final String aufgabeId = VaadinPortletService.getCurrentRequest().getRequest().getParameter("aufgabeId");
//
//		if (this.securityService.checkAccessToVorhabenAndAufgabe(vorhabenId, aufgabeId)) {
//			return new TabSecurityPermission(TabSecurityPermission.Permission.ALLOWED);
//		} else {
//			return new TabSecurityPermission(TabSecurityPermission.Permission.DENIED);
//		}

		return new TabSecurityPermission(TabSecurityPermission.Permission.ALLOWED);
	}

	@Override
	public void init() {
//		this.ortDesVorhabensContainer.addItemSetChangeListener(new DefaultTabItemSetChangeListener<OrtComponentModel>() {
//			private static final long serialVersionUID = -6749912540280429411L;
//
//			@Override
//			public void onItemAdded(final OrtComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.createOrtDesVorhabens(getVorhabenId(), item);
//			}
//
//			@Override
//			public void onItemRemoved(final OrtComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.deleteOrtDesVorhabens(item);
//			}
//
//			@Override
//			public void onItemUpdated(final OrtComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.updateOrtDesVorhabens(item);
//			}
//		});
//
//		this.aufteilungDerAusgabenAufJahreContainer
//				.addItemSetChangeListener(new DefaultTabItemSetChangeListener<AufteilungDerAusgabenAufJahreComponentModel>() {
//
//					private static final long serialVersionUID = -7944714313793388955L;
//
//					@Override
//					public void onItemAdded(final AufteilungDerAusgabenAufJahreComponentModel item) {
//						ThpthkAntragPresenter.this.vorhabenService.createAufteilungDerAusgabenAufJahre(getVorhabenId(), item);
//					}
//
//					@Override
//					public void onItemRemoved(final AufteilungDerAusgabenAufJahreComponentModel item) {
//						ThpthkAntragPresenter.this.vorhabenService.deleteAufteilungDerAusgabenAufJahre(item);
//					}
//
//					@Override
//					public void onItemUpdated(final AufteilungDerAusgabenAufJahreComponentModel item) {
//						ThpthkAntragPresenter.this.vorhabenService.updateAufteilungDerAusgabenAufJahre(item);
//					}
//				});
//
//		this.finanzierungContainer.addItemSetChangeListener(new DefaultTabItemSetChangeListener<FinanzierungComponentModel>() {
//
//			@Override
//			public void onItemAdded(final FinanzierungComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.createFinanzierung(getVorhabenId(), item);
//			}
//
//			@Override
//			public void onItemRemoved(final FinanzierungComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.deleteFinanzierung(item);
//			}
//
//			@Override
//			public void onItemUpdated(final FinanzierungComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.updateFinanzierung(item);
//			}
//		});
//
//		this.kontaktContainer.addItemSetChangeListener(new DefaultTabItemSetChangeListener<KontaktComponentModel>() {
//			@Override
//			public void onItemUpdated(KontaktComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.updateKontakt(item);
//			}
//
//			@Override
//			public void onItemRemoved(KontaktComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.deleteKontakt(getVorhabenId(), item);
//			}
//
//			@Override
//			public void onItemAdded(KontaktComponentModel item) {
//				ThpthkAntragPresenter.this.vorhabenService.createKontakt(getVorhabenId(), item);
//			}
//		});
	}

	@Override
	public void reset() {
//		super.reset();
//
//		final Future<AdresseComponentModel> findOrCreateVorhabenAdresse =
//				this.vorhabenService.findOrCreateVorhabenAdresse(this.getVorhabenId());
//
//		final Future<PostfachComponentModel> findOrCreatePostfach =
//				this.vorhabenService.findOrCreatePostfach(this.getVorhabenId());
//
//		final Future<SpezifischeDatenComponentModel> findOrCreateSpezifischeDaten =
//				this.vorhabenService.findOrCreateSpezifischeDaten(this.getVorhabenId());
//
//		final Future<VorhabenszeitraumComponentModel> findOrCreateVorhabenZeitraum =
//				this.vorhabenService.findOrCreateVorhabenZeitraum(this.getVorhabenId());
//
//		final Future<BrancheComponentModel> findOrCreateBranche =
//				this.vorhabenService.findOrCreateBranche(this.getVorhabenId());
//
//		final Future<Collection<? extends OrtComponentModel>> findOrtDesVorhabens =
//				this.vorhabenService.findOrCreateVorhabenOrtDesVorhabens(this.getVorhabenId());
//
//		final Future<Collection<? extends AufteilungDerAusgabenAufJahreComponentModel>> findAufteilungDerAusgabenAufJahre =
//				this.vorhabenService.findOrCreateAufteilungDerAusgabenAufJahre(this.getVorhabenId());
//
//		final Future<Collection<? extends IndikatorenComponentModel>> fetchIndikatorListe =
//				this.vorhabenService.fetchIndikatorListe(this.getVorhabenId());
//
//		final Future<Collection<? extends KontaktComponentModel>> findKontakt =
//				this.vorhabenService.findOrCreateVorhabenKontakt(this.getVorhabenId());
//
//		final Future<VorhabensbeschreibungComponentModel> findOrCreateVorhabenBeschreibung =
//				this.vorhabenService.findOrCreateVorhabenBeschreibung(this.getVorhabenId());
//
//		final Future<AufbewahrungsortDerBelegeComponentModel> findOrCreateAufbewahrungsortDerBelege =
//				this.vorhabenService.findOrCreateAufbewahrungsortDerBelege(this.getVorhabenId());
//
//		final Future<GeplanteAusgabenComponentModel> findOrCreateGeplanteAusgaben =
//				this.vorhabenService.findOrCreateGeplanteAusgaben(this.getVorhabenId());
//
//		final Future<StammdatenComponentModel> findOrCreateStammdaten =
//				this.vorhabenService.findOrCreateStammdaten(this.getVorhabenId());
//
//		final Future<Collection<? extends FinanzierungComponentModel>> findFinazierung =
//				this.vorhabenService.findOrCreateFinazierung(this.getVorhabenId());
//
//		final Future<AktuellerBewilligungsstandComponentModel> findOrCreateAktuellerBewilligungsstand =
//				this.vorhabenService.findOrCreateAktuellerBewilligungsstand(this.getVorhabenId());
//
//		final Future<BewilligteAusgabenComponentModel> findOrCreateBewilligteAusgaben =
//				this.vorhabenService.findOrCreateBewilligteAusgaben(this.getVorhabenId());
//
//		final Future<AnsprechpartnerMeldestelleComponentModel> findOrCreateAnsprechpartnerMeldestelle =
//				this.vorhabenService.findOrCreateAnsprechpartnerMeldestelle(this.getVorhabenId());
//
//		try {
//			this.adresseBeanItem.setBean(findOrCreateVorhabenAdresse.get());
//			this.postfachBeanItem.setBean(findOrCreatePostfach.get());
//			this.spezifischeDatenBeanItem.setBean(findOrCreateSpezifischeDaten.get());
//			this.vorhabenszeitraumBeanItem.setBean(findOrCreateVorhabenZeitraum.get());
//			this.brancheBeanItem.setBean(findOrCreateBranche.get());
//			this.vorhabensBeschreibungBeanItem.setBean(findOrCreateVorhabenBeschreibung.get());
//			this.aufbewahrungsortBeanItem.setBean(findOrCreateAufbewahrungsortDerBelege.get());
//			this.geplanteAusgabenBeanItem.setBean(findOrCreateGeplanteAusgaben.get());
//			this.stammdatenBeanItem.setBean(findOrCreateStammdaten.get());
//			this.aktuellerBewilligungsstandBeanItem.setBean(findOrCreateAktuellerBewilligungsstand.get());
//			this.bewilligteAusgabenBeanItem.setBean(findOrCreateBewilligteAusgaben.get());
//			this.ansprechpartnerMeldestelleBeanItem.setBean(findOrCreateAnsprechpartnerMeldestelle.get());
//			// tables
//			this.ortDesVorhabensContainer.removeAllItems(false);
//			this.ortDesVorhabensContainer.addAll(findOrtDesVorhabens.get(), false);
//			this.aufteilungDerAusgabenAufJahreContainer.removeAllItems(false);
//			this.aufteilungDerAusgabenAufJahreContainer.addAll(findAufteilungDerAusgabenAufJahre.get(), false);
//			this.finanzierungContainer.removeAllItems(false);
//			this.finanzierungContainer.addAll(findFinazierung.get(), false);
//			this.indikatorContainer.removeAllItems(false);
//			this.indikatorContainer.addAll(fetchIndikatorListe.get(), false);
//			this.kontaktContainer.removeAllItems(false);
//			this.kontaktContainer.addAll(findKontakt.get(), false);
//		} catch (final InterruptedException e) {
//			this.logger.error("Scheduled execution was interrupted", e);
//		} catch (final CancellationException e) {
//			this.logger.warn("Watcher thread has been cancelled", e);
//		} catch (final ExecutionException e) {
//			this.logger.error("Uncaught exception in scheduled execution", e.getCause());
//		}

	}

	@Override
	public void saveVorhaben() {
//		super.saveVorhaben();
//
//		this.logger.info("save()");
//
//		try {
//			this.vorhabenService.save(this.getVorhabenId(),
//					this.adresseBeanItem.getBean(),
//					this.postfachBeanItem.getBean(),
//					this.indikatorContainer.getItemIds(),
//					this.spezifischeDatenBeanItem.getBean(),
//					this.vorhabenszeitraumBeanItem.getBean(),
//					this.brancheBeanItem.getBean(),
//					this.vorhabensBeschreibungBeanItem.getBean(),
//					this.aufbewahrungsortBeanItem.getBean(),
//					this.geplanteAusgabenBeanItem.getBean(),
//					this.stammdatenBeanItem.getBean(),
//					this.aktuellerBewilligungsstandBeanItem.getBean(),
//					this.bewilligteAusgabenBeanItem.getBean(),
//					this.ansprechpartnerMeldestelleBeanItem.getBean());
//		} catch (final PortalPersistenceRestClientException e) {
//			this.logger.error("saveVorhaben ... ", e);
//		}
	}

	public ThpthkVorhaben getVorhaben() {
		return this.vorhabenService.getVorhaben(this.getVorhabenId()).getContent();
	}

	@Override
	public void formularIsValidChangedTo(final boolean valid) {
		this.logger.info("formularIsValidChangedTo()");
		final Date time;

		if (valid) {
			final Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.YEAR, 1);
			time = c.getTime();
		}
		// gültig zurücksetzen
		else {
			time = new Date(0);
		}

		this.vorhabenService.saveFormularIsValidTo(this.getVorhabenId(), time);
	}

	public AdresseComponent.AdressenValidator getAdressenValidator() {
		return this.adressenValidator;
	}

	public CustomBeanItem<AdresseComponentModel> getAdresseBeanItem() {
		return this.adresseBeanItem;
	}

	public CustomBeanItem<PostfachComponentModel> getPostfachBeanItem() {
		return this.postfachBeanItem;
	}

	public CustomBeanItem<SpezifischeDatenComponentModel> getSpezifischeDatenBeanItem() {
		return this.spezifischeDatenBeanItem;
	}

	public CustomBeanItem<VorhabenszeitraumComponentModel> getVorhabenszeitraumBeanItem() {
		return this.vorhabenszeitraumBeanItem;
	}

	public CustomBeanItem<BrancheComponentModel> getBrancheBeanItem() {
		return this.brancheBeanItem;
	}

	public CustomBeanItem<AufbewahrungsortDerBelegeComponentModel> getAufbewahrungsortBeanItem() {
		return this.aufbewahrungsortBeanItem;
	}

	public CustomBeanItem<GeplanteAusgabenComponentModel> getGeplanteAusgabenBeanItem() {
		return this.geplanteAusgabenBeanItem;
	}

	public CustomBeanItem<AnsprechpartnerMeldestelleComponentModel> getAnsprechpartnerMeldestelleBeanItem() {
		return this.ansprechpartnerMeldestelleBeanItem;
	}

	public CustomBeanItem<BewilligteAusgabenComponentModel> getBewilligteAusgabenBeanItem() {
		return this.bewilligteAusgabenBeanItem;
	}

	public CustomBeanItem<AktuellerBewilligungsstandComponentModel> getAktuellerBewilligungsstandBeanItem() {
		return this.aktuellerBewilligungsstandBeanItem;
	}

	public TabBeanItemContainer<AufteilungDerAusgabenAufJahreComponentModel> getAufteilungDerAusgabenAufJahreContainer() {
		return this.aufteilungDerAusgabenAufJahreContainer;
	}

	public TabBeanItemContainer<OrtComponentModel> getOrtDesVorhabensContainer() {
		return this.ortDesVorhabensContainer;
	}

	public TabBeanItemContainer<FinanzierungComponentModel> getFinanzierungContainer() {
		return this.finanzierungContainer;
	}

	public TabBeanItemContainer<String> getAufteilungDerAusgabenAufJahreJahrContainer() {
		return this.aufteilungDerAusgabenAufJahreJahrContainer;
	}

	public TabBeanItemContainer<KontaktComponentModel> getKontaktContainer() {
		return this.kontaktContainer;
	}

	public TabBeanItemContainer<IndikatorenComponentModel> getIndikatorContainer() {
		return this.indikatorContainer;
	}

	public String getFoerdergegenstand() {
		return vorhabenService.getFoerdergegenstand(this.getVorhabenId());
	}

	public CustomBeanItem<StammdatenComponentModel> getStammdatenBeanItem() {
		return stammdatenBeanItem;
	}

	public TabBeanItemContainer<String> getFinanzierungJahrContainer() {
		return this.finanzierungJahrContainer;
	}

	public BigDecimal calculateFinanzierungSumme(String column) {
		return CalculationUtil.getContainerPropertySum(finanzierungContainer, column);
	}

	public void populateJahrComboboxes(Calendar beginnCal, Calendar endeCal) {
		int yearOffset = 0;

		this.aufteilungDerAusgabenAufJahreJahrContainer.removeAllItems();
		this.finanzierungJahrContainer.removeAllItems();

		if ((beginnCal.get(Calendar.MONTH) < Calendar.AUGUST)) {
			beginnCal.add(Calendar.YEAR, -1);
			yearOffset = 1;
		}

		while (beginnCal.get(Calendar.YEAR) < endeCal.get(Calendar.YEAR)) {
			boolean yearExistsForAusgabeAufteilungDerAusgabenAufJahre = false;
			boolean yearExistsForAusgabeFinazierung = false;

			int year = beginnCal.get(java.util.Calendar.YEAR);

			String yearWithOffset = year + yearOffset + "";

			for (AufteilungDerAusgabenAufJahreComponentModel model : aufteilungDerAusgabenAufJahreContainer.getItemIds()) {
				if (model.getJahr().equals(yearWithOffset)) {
					yearExistsForAusgabeAufteilungDerAusgabenAufJahre = true;
				}
			}

			if (!yearExistsForAusgabeAufteilungDerAusgabenAufJahre) {
				this.aufteilungDerAusgabenAufJahreJahrContainer.addItem(yearWithOffset);
			}

			for (FinanzierungComponentModel model : finanzierungContainer.getItemIds()) {
				if (model.getJahr().equals(yearWithOffset)) {
					yearExistsForAusgabeFinazierung = true;
				}
			}

			if (!yearExistsForAusgabeFinazierung) {
				this.finanzierungJahrContainer.addItem(yearWithOffset);
			}

			beginnCal.add(Calendar.YEAR, 1);
		}
	}

	protected WsAufgabe getAufgabe() {
		return this.workflowClientService.getAufgabenService().getAufgabe(this.getAufgabeId());
	}

	public boolean isAufgabeAntragPruefen() {
		return getAufgabe().getTyp().equals("antragPruefen");
	}
}