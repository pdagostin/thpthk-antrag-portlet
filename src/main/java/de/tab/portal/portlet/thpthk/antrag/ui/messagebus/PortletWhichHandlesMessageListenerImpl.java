package de.tab.portal.portlet.thpthk.antrag.ui.messagebus;

import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageBusUtil;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import de.tab.portal.commons.messagebus.PortletWhichHandlesMessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Senf on 11.11.2015.
 */
public class PortletWhichHandlesMessageListenerImpl implements PortletWhichHandlesMessageListener, MessageListener {
	private final Logger logger = LoggerFactory.getLogger(PortletWhichHandlesMessageListenerImpl.class);

	@Override
	public String[] getDestinationNames() {
		// das Kürzel des Förderprogramms
		final String destinationName = DESTINATION_NAME_PREFIX + "/OP_THPTHK";

		// die Kürzel der einzelnen Webformulare aus den Aufgaben
		return new String[]{
				destinationName + "/ANTRAG",
				destinationName + "/VORHABEN",
				destinationName + "/ANTRAG_PRUEFEN"
		};
	}

	@Override
	public String payload() {
		return "thpthkantragportlet_WAR_thpthkantragportlet";
	}

	@Override
	public void receive(final Message message) throws MessageListenerException {
		try {
			final Message responseMessage = MessageBusUtil.createResponseMessage(message);

			responseMessage.setPayload(payload());

			MessageBusUtil.sendMessage(responseMessage.getDestinationName(), responseMessage);
		} catch (final Exception e) {
			this.logger.error("Unable to process message " + message, e);
		}
	}
}
