package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AnsprechpartnerMeldestelleComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

/**
 * Created by Nikola Sapun on 14/3/2016.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "name",
                caption = @Caption({"thpthk.antrag.bearbeiter.ansprechpartner.meldestelle.name"})),
        @PropertyCaption(propertyId = "telefon",
                caption = @Caption({"thpthk.antrag.bearbeiter.ansprechpartner.meldestelle.telefon"})),
        @PropertyCaption(propertyId = "email",
                caption = @Caption({"thpthk.antrag.bearbeiter.ansprechpartner.meldestelle.email"})
        )})
public class AnsprechpartnerMeldestelleComponent extends FormLayout implements Item.Editor {

    @PropertyId("name")
    private final TextField name = VaadinUtils.buildBasicTextField();

    @PropertyId("telefon")
    private final TextField telefon = VaadinUtils.buildBasicTextField();

    @PropertyId("email")
    private final TextField email = VaadinUtils.buildBasicTextField();


    public AnsprechpartnerMeldestelleComponent() {
        this.buildLayout();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.name, this.telefon, this.email);
    }

    private void initValidators() {
        VaadinUtils.configField(this.name, AnsprechpartnerMeldestelleComponentModel.class, "name");
        VaadinUtils.configField(this.telefon, AnsprechpartnerMeldestelleComponentModel.class, "telefon");
        VaadinUtils.configField(this.email, AnsprechpartnerMeldestelleComponentModel.class, "email");
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }
}
