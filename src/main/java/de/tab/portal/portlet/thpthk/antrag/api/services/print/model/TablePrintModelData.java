package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TablePrintModelData {

	public String dataCol1;
	public String dataCol2;
	public String dataCol3;

}
