package de.tab.portal.portlet.thpthk.antrag.ui.components;

import de.tab.portal.client.adressen.AdressenClient;
import de.tab.portal.client.adressen.model.AgsBundeslandLandkreis;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AdresseComponentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

/**
 * Created by Senf on 30.09.2015.
 */
@Component
public class DefaultAdressenValidator implements AdresseComponent.AdressenValidator {

    @Autowired
    private AdressenClient adressenClient;

    @Autowired
    private ConversionService conversionService;

    @Override
    public AdresseComponentModel validatePlzAndOrt(final String plz, final String ort) {
        final AgsBundeslandLandkreis agsBundeslandLandkreis =
                this.adressenClient.findAgsBundeslandLandkreis(ort, plz);

        final AdresseComponentModel model =
                this.conversionService.convert(agsBundeslandLandkreis, AdresseComponentModel.class);

        return model;
    }
}
