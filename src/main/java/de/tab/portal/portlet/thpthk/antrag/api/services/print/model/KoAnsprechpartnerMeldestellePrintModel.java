package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoAnsprechpartnerMeldestellePrintModel {

	public String title = "tcm.print.antrag.ansprechpartnerMeldestelle.title";

	public String[][] layout = {
			{"name", "telefon"},
			{"email"}
	};

	public String nameBezeichnung = "tcm.print.antrag.ansprechpartnerMeldestelle.nameBezeichnung";
	public String telefonBezeichnung = "tcm.print.antrag.ansprechpartnerMeldestelle.telefonBezeichnung";
	public String emailBezeichnung = "tcm.print.antrag.ansprechpartnerMeldestelle.emailBezeichnung";

	public String name;
	public String telefon;
	public String email;

}
