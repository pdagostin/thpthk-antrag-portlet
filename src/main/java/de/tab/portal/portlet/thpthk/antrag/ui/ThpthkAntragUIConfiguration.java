package de.tab.portal.portlet.thpthk.antrag.ui;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.messaging.Destination;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.SynchronousDestination;
import com.liferay.portal.kernel.messaging.config.PluginMessagingConfigurator;
import com.vaadin.spring.annotation.EnableVaadin;
import de.tab.portal.client.adressen.config.AdressenClientConfiguration;
import de.tab.portal.client.katalog.config.KatalogClientConfiguration;
import de.tab.portal.client.persistence.KeyTextModule;
import de.tab.portal.client.persistence.config.PortalPersistenceClientConfiguration;
import de.tab.portal.client.print.config.PrintConfiguration;
import de.tab.portal.client.workflow.config.WcConfiguration;
import de.tab.portal.commons.config.CommonsConfiguration;
import de.tab.portal.commons.messagebus.PortletApiMessageListener;
import de.tab.portal.commons.messagebus.PortletWhichHandlesMessageListener;
import de.tab.portal.portlet.thpthk.antrag.ui.messagebus.PortletApiMessageListenerImpl;
import de.tab.portal.portlet.thpthk.antrag.ui.messagebus.PortletWhichHandlesMessageListenerImpl;
import de.tab.security.SecurityConfiguration;
import de.tab.vaadin.config.DefaultConfiguration;
import de.tab.vaadin.config.VaadinConfiguration;
import de.vollmar.vaadin.spring.i18n.annotations.EnableI18n;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.commonj.WorkManagerTaskExecutor;

import java.util.*;

@Configuration
@EnableVaadin
@EnableAsync
@EnableI18n
@Import({CommonsConfiguration.class, WcConfiguration.class, VaadinConfiguration.class,
        KatalogClientConfiguration.class, PrintConfiguration.class, AdressenClientConfiguration.class,
        PortalPersistenceClientConfiguration.class, SecurityConfiguration.class})
@ComponentScan({"de.tab.portal.portlet.thpthk.antrag.ui", "de.tab.portal.portlet.thpthk.antrag.ui.components",
        "de.tab.portal.portlet.thpthk.antrag.ui.services"})
public class ThpthkAntragUIConfiguration extends DefaultConfiguration {
    private final PortletWhichHandlesMessageListener portletWhichHandlesMessageListener = new PortletWhichHandlesMessageListenerImpl();
    private final PortletApiMessageListener portletApiMessageListener = new PortletApiMessageListenerImpl();

    public ThpthkAntragUIConfiguration() {
        super("de/tab/portal/portlet/thpthk/antrag/ui/messages");
    }

    @Bean
    public ObjectMapper objectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.registerModule(new KeyTextModule());

        return objectMapper;
    }

    @Bean(name = "taskExecutor")
    public WorkManagerTaskExecutor taskExecutor() {
        final WorkManagerTaskExecutor workManagerTaskExecutor = new WorkManagerTaskExecutor();
        workManagerTaskExecutor.setWorkManagerName("java:comp/env/wm/ServiceWorkManager");
        workManagerTaskExecutor.setResourceRef(true);

        return workManagerTaskExecutor;
    }

    @Bean(destroyMethod = "destroy")
    public PluginMessagingConfigurator pluginMessagingConfigurator() {
        final PluginMessagingConfigurator configurator = new PluginMessagingConfigurator();
        final List<Destination> destinations = new ArrayList<>();
        final Map<String, List<MessageListener>> messageListeners = new HashMap<>();

        for (final String destinationName : this.portletApiMessageListener.getDestinationNames()) {
            messageListeners.put(destinationName,
                    Collections.<MessageListener> singletonList((MessageListener) this.portletApiMessageListener));
            destinations.add(this.createSynchronousDestination(destinationName));
        }
        for (final String destinationName : this.portletWhichHandlesMessageListener.getDestinationNames()) {
            messageListeners.put(destinationName,
                    Collections.<MessageListener> singletonList((MessageListener) this.portletWhichHandlesMessageListener));
            destinations.add(this.createSynchronousDestination(destinationName));
        }

        configurator.setMessageListeners(messageListeners);
        configurator.setDestinations(destinations);
        configurator.afterPropertiesSet();
        return configurator;
    }

    private SynchronousDestination createSynchronousDestination(final String name) {
        final SynchronousDestination e = new SynchronousDestination();
        e.setName(name);
        e.afterPropertiesSet();
        return e;
    }
}
