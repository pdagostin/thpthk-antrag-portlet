package de.tab.portal.portlet.thpthk.antrag.ui.services;

import de.tab.portal.client.persistence.exceptions.PortalPersistenceRestClientException;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.*;
import de.tab.portal.portlet.thpthk.antrag.ui.persistence.model.ThpthkVorhaben;
import org.springframework.hateoas.Resource;
import org.springframework.scheduling.annotation.Async;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by Senf on 26.06.2015.
 */
public interface VorhabenService {

    Future<Collection<? extends IndikatorenComponentModel>> fetchIndikatorListe(Long vorhabenId);

    @Async(value = "taskExecutor")
    Future<AdresseComponentModel> findOrCreateVorhabenAdresse(long id);

    @Async(value = "taskExecutor")
    Future<PostfachComponentModel> findOrCreatePostfach(long id);

    @Async(value = "taskExecutor")
    Future<SpezifischeDatenComponentModel> findOrCreateSpezifischeDaten(final Long vorhabenId);

    @Async(value = "taskExecutor")
    Future<VorhabenszeitraumComponentModel> findOrCreateVorhabenZeitraum(final Long vorhabenId);

    @Async(value = "taskExecutor")
    Future<BrancheComponentModel> findOrCreateBranche(final Long vorhabenId);

    @Async(value = "taskExecutor")
    Future<Collection<? extends OrtComponentModel>> findOrCreateVorhabenOrtDesVorhabens(long vorhabenId);

    @Async(value = "taskExecutor")
    Future<Collection<? extends AufteilungDerAusgabenAufJahreComponentModel>> findOrCreateAufteilungDerAusgabenAufJahre(long vorhabenId);

    @Async(value = "taskExecutor")
    Future<Collection<? extends KontaktComponentModel>> findOrCreateVorhabenKontakt(final Long vorhabenId);

    void createAufteilungDerAusgabenAufJahre(Long vorhabenId, AufteilungDerAusgabenAufJahreComponentModel item);

    void deleteAufteilungDerAusgabenAufJahre(AufteilungDerAusgabenAufJahreComponentModel item);

    void updateAufteilungDerAusgabenAufJahre(AufteilungDerAusgabenAufJahreComponentModel item);

    void createOrtDesVorhabens(Long vorhabenId, OrtComponentModel item);

    void deleteOrtDesVorhabens(OrtComponentModel item);

    void updateOrtDesVorhabens(OrtComponentModel item);

    void createKontakt(Long vorhabenId, KontaktComponentModel item);

    void deleteKontakt(Long vorhabenId, KontaktComponentModel item);

    void updateKontakt(KontaktComponentModel item);

	@Async(value = "taskExecutor")
	Future<AufbewahrungsortDerBelegeComponentModel> findOrCreateAufbewahrungsortDerBelege(final Long vorhabenId);

	@Async(value = "taskExecutor")
	Future<GeplanteAusgabenComponentModel> findOrCreateGeplanteAusgaben(final Long vorhabenId);

    @Async(value = "taskExecutor")
    Future<StammdatenComponentModel> findOrCreateStammdaten(final Long vorhabenId);


    @Async(value = "taskExecutor")
    void save(final Long vorhabenId,
              final AdresseComponentModel adresseComponentModel,
              final PostfachComponentModel postfachComponentModel,
              final List<IndikatorenComponentModel> indikatorList,
              final SpezifischeDatenComponentModel spezifischeDatenComponentModel,
              final VorhabenszeitraumComponentModel vorhabenszeitraumComponentModel,
              final BrancheComponentModel brancheComponentModel,
              final AufbewahrungsortDerBelegeComponentModel aufbewahrungsortDerBelegeComponentModel,
              final GeplanteAusgabenComponentModel geplanteAusgabenComponentModel,
              final StammdatenComponentModel stammdatenComponentModel,
              final AktuellerBewilligungsstandComponentModel aktuellerBewilligungsstandComponentModel,
              final BewilligteAusgabenComponentModel bewilligteAusgabenComponentModel,
              final AnsprechpartnerMeldestelleComponentModel ansprechpartnerMeldestelleComponentModel) throws PortalPersistenceRestClientException;

    @Async(value = "taskExecutor")
    Future<String> getFoerderFallId(Long vorhabenId);

    Resource<ThpthkVorhaben> getVorhaben(final Long vorhabenId);

    String getFoerdergegenstand(Long vorhabenId);

    void createFinanzierung(Long vorhabenId, FinanzierungComponentModel item);

    void deleteFinanzierung(FinanzierungComponentModel item);

    void updateFinanzierung(FinanzierungComponentModel item);

    @Async(value = "taskExecutor")
    Future<Collection<? extends FinanzierungComponentModel>> findOrCreateFinazierung(long vorhabenId);

    @Async(value = "taskExecutor")
    Future<AktuellerBewilligungsstandComponentModel> findOrCreateAktuellerBewilligungsstand(final Long vorhabenId);

    @Async(value = "taskExecutor")
    Future<BewilligteAusgabenComponentModel> findOrCreateBewilligteAusgaben(final Long vorhabenId);

    @Async(value = "taskExecutor")
    Future<AnsprechpartnerMeldestelleComponentModel> findOrCreateAnsprechpartnerMeldestelle(Long vorhabenId);

    void saveFormularIsValidTo(Long vorhabenId, Date time);
}
