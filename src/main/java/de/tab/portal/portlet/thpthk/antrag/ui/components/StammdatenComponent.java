package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.*;
import de.tab.portal.commons.DefaultKeyText;
import de.tab.portal.commons.KeyText;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.StammdatenComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

/**
 * @author idekic
 * @since 14-Mar-16.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "aktenzeichen",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.aktenzeichen"})),
        @PropertyCaption(propertyId = "euProjekt",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.euProjekt"})),
        @PropertyCaption(propertyId = "foerderperiode",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.foerderperiode"})),
        @PropertyCaption(propertyId = "antragsdatum",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.antragsdatum"})),
        @PropertyCaption(propertyId = "eingangsdatum",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.eingangsdatum"})),
        @PropertyCaption(propertyId = "bewilligungsdatum",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.bewilligungsdatum"})),
        @PropertyCaption(propertyId = "zweckbindefrist",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.zweckbindefristdatum"})),
        @PropertyCaption(propertyId = "vwneingang",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.vwnEingang"})),
        @PropertyCaption(propertyId = "vwnpruefung",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.vwnPruefung"})),
        @PropertyCaption(propertyId = "insolvenzdatum",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.insolvenzdatum"})),
        @PropertyCaption(propertyId = "registriernummer",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.registriernummer"})),
        @PropertyCaption(propertyId = "endeAbruffrist",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.endeAbruffrist"})),
        @PropertyCaption(propertyId = "archivierungsdatum",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.archivierungsdatum"})),
        @PropertyCaption(propertyId = "physischerAbschluss",
                caption = @Caption({"thpthk.pruefen.stammdaten.grunddaten.stammdaten.physischerAbschluss"})

        )})
public class StammdatenComponent extends FormLayout implements Item.Editor {

    private final static String FGS_THP = "THPU";

    private final static String FGS_THK = "THKS";

    @PropertyId("aktenzeichen")
    private final TextField aktenzeichen = VaadinUtils.buildBasicTextField();

    @PropertyId("euProjekt")
    private final OptionGroup euProjekt = VaadinUtils.buildBasicYesNoOptionGroup();

    @PropertyId("foerderperiode")
    private final ComboBox foerderperiode = VaadinUtils.buildBasicComboBox();

    @PropertyId("antragsdatum")
    private final DateField antragsdatum = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("eingangsdatum")
    private final DateField eingangsdatum = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("bewilligungsdatum")
    private final DateField bewilligungsdatum = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("zweckbindefrist")
    private final DateField zweckbindefrist = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("vwneingang")
    private final DateField vwneingang = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("vwnpruefung")
    private final DateField vwnpruefung = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("insolvenzdatum")
    private final DateField insolvenzdatum = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("registriernummer")
    private final TextField registriernummer = VaadinUtils.buildBasicTextField();

    @PropertyId("endeAbruffrist")
    private final DateField endeAbruffrist = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("archivierungsdatum")
    private final DateField archivierungsdatum = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("physischerAbschluss")
    private final DateField physischerAbschluss = VaadinUtils.buildBasicPopupDateField();

    private TranslationService translationService;

    public StammdatenComponent() {
        this.buildLayout();
        this.initValidators();
    }

    private void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.aktenzeichen, this.euProjekt, this.foerderperiode, this.antragsdatum, this.eingangsdatum,
                this.bewilligungsdatum, this.zweckbindefrist, this.vwneingang, this.vwnpruefung, this.insolvenzdatum,
                this.registriernummer, this.endeAbruffrist, this.archivierungsdatum, this.physischerAbschluss);
    }

    private void initValidators() {
        VaadinUtils.configField(this.aktenzeichen, StammdatenComponentModel.class, "aktenzeichen");
        VaadinUtils.configField(this.euProjekt, StammdatenComponentModel.class, "euProjekt");
        VaadinUtils.configField(this.foerderperiode, StammdatenComponentModel.class, "foerderperiode");
        VaadinUtils.configField(this.antragsdatum, StammdatenComponentModel.class, "antragsdatum");
        VaadinUtils.configField(this.eingangsdatum, StammdatenComponentModel.class, "eingangsdatum");
        VaadinUtils.configField(this.bewilligungsdatum, StammdatenComponentModel.class, "bewilligungsdatum");
        VaadinUtils.configField(this.zweckbindefrist, StammdatenComponentModel.class, "zweckbindefrist");
        VaadinUtils.configField(this.vwneingang, StammdatenComponentModel.class, "vwneingang");
        VaadinUtils.configField(this.vwnpruefung, StammdatenComponentModel.class, "vwnpruefung");
        VaadinUtils.configField(this.insolvenzdatum, StammdatenComponentModel.class, "insolvenzdatum");
        VaadinUtils.configField(this.registriernummer, StammdatenComponentModel.class, "registriernummer");
        VaadinUtils.configField(this.endeAbruffrist, StammdatenComponentModel.class, "endeAbruffrist");
        VaadinUtils.configField(this.archivierungsdatum, StammdatenComponentModel.class, "archivierungsdatum");
        VaadinUtils.configField(this.physischerAbschluss, StammdatenComponentModel.class, "physischerAbschluss");
    }

    public void setValidationType(final String foerdergegenstand) {
        this.aktenzeichen.removeAllValidators();
        this.aktenzeichen.addValidator(new Validator() {
            @Override
            public void validate(Object o) throws InvalidValueException {
                if (!((String) o).matches("^\\d{4} " + foerdergegenstand.substring(0, 3) + " \\d{4}$")) {
                    if (StammdatenComponent.FGS_THP.equals(foerdergegenstand)) {
                        throw new InvalidValueException(StammdatenComponent.this.translationService.translate("thpthk.pruefen.stammdaten.grunddaten.stammdaten.aktenzeichen.errorTHP"));
                    } else if (StammdatenComponent.FGS_THK.equals(foerdergegenstand)) {
                        throw new InvalidValueException(StammdatenComponent.this.translationService.translate("thpthk.pruefen.stammdaten.grunddaten.stammdaten.aktenzeichen.errorTHK"));
                    }
                }
            }
        });
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }

    public void setFoerderperiodeContainer(Container c) {
        this.foerderperiode.setContainerDataSource(c);
    }

    public void setTranslationService(TranslationService translationService) {
        this.translationService = translationService;
    }

    public ComboBox getFoerderperiode() {
        return this.foerderperiode;
    }
}