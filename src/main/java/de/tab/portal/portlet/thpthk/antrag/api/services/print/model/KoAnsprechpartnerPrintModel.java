package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import java.util.ArrayList;
import java.util.List;

public class KoAnsprechpartnerPrintModel {

	public String title = "Ansprechpartner";

	public String emailBezeichnung = "E-Mail-Adresse";
	public String faxBezeichnung = "Fax";
	public String nameBezeichnung = "Ansprechpartner";
	public String telefonBezeichnung = "Telefon";
	public String rolleBezeichnung = "Rolle des Ansprechpartners";

	public List<KoAnsprechpartnerPrintModelData> data = new ArrayList<KoAnsprechpartnerPrintModelData>();

}
