package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoAdressePrintModel {
	public String title = "Adresse des Antragstellers";

	public String nameBezeichnung = "Name Antragsteller";
	public String name;

	public String strassenrBezeichnung = "Straße/Nr.";
	public String strassenr;

	public String plzBezeichnung = "PLZ";
	public String plz;

	public String ortBezeichnung = "Ort";
	public String ort;

	public String landBezeichnung = "Land";
	public String land;

	public String agsBezeichnung = "Amtlicher Gemeindeschlüssel";
	public String ags;

	public String landkreisBezeichnung = "Landkreis";
	public String landkreis;

	public String regionBezeichnung = "Region";
	public String region;
}
