package de.tab.portal.portlet.thpthk.antrag.api.services.print;

import de.tab.portal.client.print.model.Content;

import java.util.List;

public interface VorhabenPrintService {

	List<Content> getAntragPrintContents(Long vorhabenId, boolean antragPruefen);

	String getAktenzeichen(final Long vorhabenId);

}