package de.tab.portal.portlet.thpthk.antrag.ui.persistence.model;

import java.math.BigDecimal;
import java.util.Date;

import de.tab.portal.commons.KeyText;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.OrtComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.StammdatenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.VorhabenszeitraumComponentModel;

/**
 * Created by Senf on 01.03.2016.
 */
public class ThpthkVorhaben {

	public VorhabenszeitraumComponentModel zeitraum;
	public OrtComponentModel adresse;
	public String belegeAufbewahrungsort;
	public KeyText massnahme;
	public StammdatenComponentModel stammdaten;
	public BigDecimal gesamtausgaben;
	public BigDecimal gesamtausgabenFoerderfaehig;
	public BigDecimal foerdersatz;
	public BigDecimal gesamtausgabenBew;
	public BigDecimal foerderAusgabenBew;
	public BigDecimal foerdersatzBew;
	public BigDecimal gesamtausgabenPlan;
    public String wfFoerderfallId;
    public String branche;
    public String beschreibungKurz;
    public String beschreibungLang;
    public String foerdergegenstand;
    public String aktenzeichen;
    public Date antragFormularIsValidUntil;
	public String einordnungAntragsteller;
}
