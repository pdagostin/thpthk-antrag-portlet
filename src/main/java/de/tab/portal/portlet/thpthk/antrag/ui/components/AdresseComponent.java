package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import de.tab.portal.commons.KeyText;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AdresseComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author hrvoje.krot
 * @since 25.9.2015.
 * <p>
 * Component representing Adresse UI section backed by data holder {@link AdresseComponentModel}
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "name",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.adresse.name"})),
        @PropertyCaption(propertyId = "land",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.adresse.land"})),
        @PropertyCaption(propertyId = "plz",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.adresse.plz"})),
        @PropertyCaption(propertyId = "ort",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.adresse.ort"})),
        @PropertyCaption(propertyId = "strassenr",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.adresse.strassenr"})),
        @PropertyCaption(propertyId = "landkreis",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.adresse.landkreis"})),
        @PropertyCaption(propertyId = "region",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.adresse.region"})),
        @PropertyCaption(propertyId = "ags",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.adresse.ags"})
        )})
public class AdresseComponent extends FormLayout implements FieldEvents.BlurListener, Item.Editor {

    private static final long serialVersionUID = -2650388649189731017L;
    private final Logger logger = LoggerFactory.getLogger(AdresseComponent.class);

	public static final String LAND_KEY_DE = "DE";

    private AdressenValidator adressenValidator;

    @PropertyId("name")
    private final TextField name = VaadinUtils.buildBasicTextField();

    @PropertyId("plz")
    private final TextField plz = VaadinUtils.buildBasicTextField();

    @PropertyId("land")
    private final ComboBox land = VaadinUtils.buildBasicComboBox();

    @PropertyId("ort")
    private final TextField ort = VaadinUtils.buildBasicTextField();

    @PropertyId("strassenr")
    private final TextField strassenr = VaadinUtils.buildBasicTextField();

    @PropertyId("ags")
    private final TextField ags = VaadinUtils.buildBasicTextField();

    @PropertyId("landkreis")
    private final TextField landkreis = VaadinUtils.buildBasicTextField();

    @PropertyId("region")
    private final TextField region = VaadinUtils.buildBasicTextField();

    @PropertyId("test")
    private final TextField test = VaadinUtils.buildBasicTextField();

    public AdresseComponent() {
        this.buildLayout();
        this.initValidators();
        this.initListeners();
        this.setGkzLandkreisRegionVisible(false);
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.test, this.name, this.strassenr, this.plz, this.ort, this.land, this.ags, this.landkreis, this.region);
    }

    private void initValidators() {
        VaadinUtils.configField(this.name, AdresseComponentModel.class, "name");
        VaadinUtils.configField(this.strassenr, AdresseComponentModel.class, "strassenr");
        VaadinUtils.configField(this.plz, AdresseComponentModel.class, "plz");
        VaadinUtils.configField(this.ort, AdresseComponentModel.class, "ort");
        VaadinUtils.configField(this.land, AdresseComponentModel.class, "land");
        VaadinUtils.configField(this.ags, AdresseComponentModel.class, "ags");
        VaadinUtils.configField(this.landkreis, AdresseComponentModel.class, "landkreis");
        VaadinUtils.configField(this.region, AdresseComponentModel.class, "region");
        VaadinUtils.configField(this.test, AdresseComponentModel.class, "test");
    }

    private void initListeners() {
        ShortcutListener enterKeyListener = new ShortcutListener("ENTER - shortcut", 13, (int[]) null) {
            private static final long serialVersionUID = 9000134037177348555L;

            public void handleAction(Object sender, Object target) {
                enterKeyPressed();
            }
        };

        this.plz.addBlurListener(this);
        this.ort.addBlurListener(this);

        this.plz.addShortcutListener(enterKeyListener);
        this.ort.addShortcutListener(enterKeyListener);
    }

	public void setPlzValidator(final String exceptionMessage) {
		land.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				if (land.getValue() != null && LAND_KEY_DE.equals(((KeyText) land.getValue()).getKey())) {
                    setGkzLandkreisRegionVisible(true);
                    plz.addValidator(new Validator() {
						@Override
						public void validate(Object value) throws InvalidValueException {
							if (value != null && !(((String) value).matches("\\d+")))
								throw new InvalidValueException(exceptionMessage);
						}
					});
                    validateValues();
				} else {
                    setGkzLandkreisRegionVisible(false);
                    ags.setValue(null);
                    landkreis.setValue(null);
                    region.setValue(null);
                    plz.removeAllValidators();
				}
			}
		});
	}

    public void setGkzLandkreisRegionVisible(boolean value) {
        this.ags.setVisible(value);
        this.landkreis.setVisible(value);
        this.region.setVisible(value);

        if (!value) {
            ags.setValue(null);
            landkreis.setValue(null);
            region.setValue(null);
        }
    }
    public void blur(FieldEvents.BlurEvent event) {
        validateValues();
    }

    private void enterKeyPressed() {
        validateValues();
    }

    private void validateValues() {
//        String plz = this.plz.getValue();
//        String ort = this.ort.getValue();
//        KeyText value = (KeyText) this.land.getValue();
//
//        if (this.adressenValidator != null && value != null) {
//            this.logger.info("prüfe plz={} ort={}", plz, ort);
//            try {
//                this.ags.setValue(null);
//                this.landkreis.setValue(null);
//                this.region.setValue(null);
//                AdresseComponentModel e = this.adressenValidator.validatePlzAndOrt(plz, ort);
//                if (e == null) {
//                    Notification.show("Achtung", "Ort/Plz nicht gefunden.", Notification.Type.HUMANIZED_MESSAGE);
//                } else {
//                    if (e.getPlz() != null) {
//                        this.plz.setValue(e.getPlz());
//                    }
//
//                    if (e.getOrt() != null) {
//                        this.ort.setValue(e.getOrt());
//                    }
//
//                    this.ags.setValue(e.getAgs());
//                    this.landkreis.setValue(e.getLandkreis());
//                    this.region.setValue(e.getRegion());
//                }
//            } catch (Exception var5) {
//                this.logger.warn("", var5);
//                Notification.show("Achtung", var5.getMessage(), Notification.Type.WARNING_MESSAGE);
//            }
//        }

    }

    public void setAdressenValidator(AdressenValidator adressenValidator) {
        this.adressenValidator = adressenValidator;
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }

    public interface AdressenValidator {
        AdresseComponentModel validatePlzAndOrt(String var1, String var2);
    }

    public void setLandContainer(Container c) {
        this.land.setContainerDataSource(c);
    }
}
