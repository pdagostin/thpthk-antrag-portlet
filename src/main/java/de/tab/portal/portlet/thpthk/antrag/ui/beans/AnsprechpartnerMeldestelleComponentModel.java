package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Nikola Sapun on 14/3/2016.
 */
public class AnsprechpartnerMeldestelleComponentModel implements Serializable {

    @NotNull
	@Size(max = 255, message = "{tab.validation.Size.message}")
	@NotBlank(message = "{tab.validation.NotBlank.message}")
	private String name;

    @NotNull
	@Size(max = 255, message = "{tab.validation.Size.message}")
	@NotBlank(message = "{tab.validation.NotBlank.message}")
	@Pattern(regexp = "([0-9+/]*\\)*\\(*\\s*)+", message = "{tab.validation.NumbersOnly.message}")
	private String telefon;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
	@Size(max = 255, message = "{tab.validation.Size.message}")
	@Email(message = "{tab.validation.Email.message}")
	private String email;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "MeldestelleComponentModel{" +
				"name='" + name + '\'' +
				", telefon='" + telefon + '\'' +
				", email='" + email + '\'' +
				'}';
	}
}
