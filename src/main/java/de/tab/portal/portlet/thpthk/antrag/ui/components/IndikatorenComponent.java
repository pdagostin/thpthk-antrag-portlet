package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.UserError;
import com.vaadin.ui.*;
import de.tab.portal.commons.DefaultKeyText;
import de.tab.portal.commons.KeyText;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.IndikatorenComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

import java.util.Iterator;

/**
 * Ideally this class and its model should be part of common components for usage over all tenders.
 *
 * @author DejVu
 * @since 28.10.2015.
 */
public class IndikatorenComponent extends VerticalLayout {

    private static final long serialVersionUID = 354364332585959495L;

    private final Table table = new Table();
    private BeanItemContainer<IndikatorenComponentModel> tableContainer;

    private TranslationService translationService;

    public IndikatorenComponent() {
        this.buildLayout();
    }

    public void buildLayout() {
        this.setSizeFull();
        this.setMargin(true);

        this.table.setImmediate(true);

        this.table.addContainerProperty("sollwert", String.class, null);
        this.table.addContainerProperty("istwert", String.class, null);

        this.table.addGeneratedColumn("sollwert", new Table.ColumnGenerator() {

            private static final long serialVersionUID = -5181747253267080902L;

            @Override
            public Object generateCell(final Table table, final Object itemId, Object columnId) {
                final IndikatorenComponentModel model = (IndikatorenComponentModel) itemId;

                if ("ANZAHL".equals(model.getIndikatorTyp())) {
                    final TextField textField = VaadinUtils.buildBasicTextField();
                    if (Boolean.TRUE.equals(model.getRequired())) {
                        textField.setRequired(true);
                        textField.setRequiredError("Pflichtfeld");
                    }
                    textField.setValue(model.getSollwert());
                    textField.addValueChangeListener(new Property.ValueChangeListener() {
                        private static final long serialVersionUID = -1344987216181353566L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            if (textField.isValid()) {
                                model.setSollwert((String) event.getProperty().getValue());
                            }
                            table.markAsDirty();
                        }
                    });
                    textField.addValidator(new Validator() {
                        private static final long serialVersionUID = -4920886578782783855L;

                        @Override
                        public void validate(Object o) throws InvalidValueException {
                            if (o != null && !((String) o).matches("\\d{0,4}")) {
                                throw new InvalidValueException(IndikatorenComponent.this.translationService.translate("tab.validation.Indikatoren.Anzahl"));
                            }
                        }
                    });

                    return textField;
                } else if ("BETRAG".equals(model.getIndikatorTyp())) {
                    final TextField textField = VaadinUtils.buildBasicTextField();
                    if (Boolean.TRUE.equals(model.getRequired())) {
                        textField.setRequired(true);
                        textField.setRequiredError("Pflichtfeld");
                    }
                    textField.setValue(model.getSollwert());
                    textField.addValueChangeListener(new Property.ValueChangeListener() {
                        private static final long serialVersionUID = 7005784333904251844L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            model.setSollwert((String) event.getProperty().getValue());
                            table.markAsDirty();
                        }
                    });

                    return textField;
                } else if ("DDLIST".equals(model.getIndikatorTyp())) {
                    ComboBox comboBox = VaadinUtils.buildBasicComboBox();
                    comboBox.setContainerDataSource(new BeanItemContainer(KeyText.class, model.getKatalog()));
                    comboBox.setInputPrompt("Bitte auswählen!");
                    comboBox.setValue(new DefaultKeyText(model.getSollwert()));
                    if (Boolean.TRUE.equals(model.getRequired())) {
                        comboBox.setRequired(true);
                        comboBox.setRequiredError("Pflichtfeld");
                    }
                    comboBox.addValueChangeListener(new Property.ValueChangeListener() {
                        private static final long serialVersionUID = -1023669283743341295L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            model.setSollwert(((KeyText) event.getProperty().getValue()).getKey());
                        }
                    });

                    return comboBox;
                } else if ("AUSWAHL".equals(model.getIndikatorTyp())) {
                    OptionGroup yesNo = VaadinUtils.buildBasicYesNoOptionGroup();
                    if (Boolean.TRUE.equals(model.getRequired())) {
                        yesNo.setRequired(true);
                        yesNo.setRequiredError("Pflichtfeld");
                    }
                    yesNo.setValue(Boolean.valueOf(model.getSollwert()));
                    yesNo.addValueChangeListener(new Property.ValueChangeListener() {
                        private static final long serialVersionUID = 7182935156560571241L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            model.setSollwert(event.getProperty().getValue().toString());
                        }
                    });
                    return yesNo;
                }

                return null;
            }
        });

        this.table.addGeneratedColumn("istwert", new Table.ColumnGenerator() {

            private static final long serialVersionUID = 5808582306873059258L;

            @Override
            public Object generateCell(final Table table, final Object itemId, Object columnId) {
                final IndikatorenComponentModel model = (IndikatorenComponentModel) itemId;

                if ("ANZAHL".equals(model.getIndikatorTyp())) {
                    final TextField textField = VaadinUtils.buildBasicTextField();
                    textField.setValue(model.getIstwert());
                    textField.addValueChangeListener(new Property.ValueChangeListener() {
                        private static final long serialVersionUID = -1344987216181353566L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            if (textField.isValid()) {
                                model.setIstwert((String) event.getProperty().getValue());
                            }
                            table.markAsDirty();
                        }
                    });
                    textField.addValidator(new Validator() {
                        private static final long serialVersionUID = -4920886578782783855L;

                        @Override
                        public void validate(Object o) throws InvalidValueException {
                            if (o != null && !((String) o).matches("\\d{0,4}")) {
                                throw new InvalidValueException(IndikatorenComponent.this.translationService.translate("tab.validation.Indikatoren.Anzahl"));
                            }
                        }
                    });

                    return textField;
                } else if ("BETRAG".equals(model.getIndikatorTyp())) {
                    final TextField textField = VaadinUtils.buildBasicTextField();
                    textField.setValue(model.getIstwert());
                    textField.addValueChangeListener(new Property.ValueChangeListener() {
                        private static final long serialVersionUID = 7005784333904251844L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            model.setIstwert((String) event.getProperty().getValue());
                            table.markAsDirty();
                        }
                    });

                    return textField;
                } else if ("DDLIST".equals(model.getIndikatorTyp())) {
                    ComboBox comboBox = VaadinUtils.buildBasicComboBox();
                    comboBox.setContainerDataSource(new BeanItemContainer(KeyText.class, model.getKatalog()));
                    comboBox.setInputPrompt("Bitte auswählen!");
                    comboBox.setValue(new DefaultKeyText(model.getIstwert()));
                    comboBox.addValueChangeListener(new Property.ValueChangeListener() {
                        private static final long serialVersionUID = -1023669283743341295L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            model.setIstwert(((KeyText) event.getProperty().getValue()).getKey());
                        }
                    });

                    return comboBox;
                } else if ("AUSWAHL".equals(model.getIndikatorTyp())) {
                    OptionGroup yesNo = VaadinUtils.buildBasicYesNoOptionGroup();
                    yesNo.setValue(Boolean.valueOf(model.getIstwert()));
                    yesNo.addValueChangeListener(new Property.ValueChangeListener() {
                        private static final long serialVersionUID = 7182935156560571241L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            model.setIstwert(event.getProperty().getValue().toString());
                        }
                    });
                    return yesNo;
                }

                return null;
            }
        });

        this.table.setImmediate(true);

        this.table.addValidator(new Validator() {
            private static final long serialVersionUID = 1961583457919792598L;

            @Override
            public void validate(Object o) throws InvalidValueException {
                Iterator<Component> iterator = IndikatorenComponent.this.table.iterator();

                while (iterator.hasNext()) {
                    Object current = iterator.next();
                    if (current instanceof TextField) {
                        TextField field = (TextField) current;
                        if (field.isRequired() && field.getValue() == null) {
                            throw new InvalidValueException("Pflichtfeld");
                        } else if (!field.isValid()) {
                            throw new InvalidValueException(IndikatorenComponent.this.translationService.translate("tab.validation.Indikatoren.Anzahl"));
                        }
                    }
                }
            }
        });

        this.addComponent(this.table);
        this.setExpandRatio(this.table, 1.0F);
    }

    public void setContainerDataSource(Container newDataSource) {
        if (newDataSource instanceof BeanItemContainer) {
            this.tableContainer = (BeanItemContainer) newDataSource;
            this.table.setContainerDataSource(newDataSource);

            this.table.setColumnHeader("text", "Indikator");
            this.table.setColumnHeader("sollwert", "Sollwert");
            this.table.setColumnHeader("istwert", "Istwert");
            this.table.setColumnWidth("text", 700);
            this.table.setColumnWidth("sollwert", 310);
            this.table.setColumnWidth("istwert", 310);

            this.table.setPageLength(this.tableContainer.size());
        } else {
            throw new IllegalArgumentException("newDataSource must be instanceof BeanItemContainer");
        }
    }

    public void setShowIstWert(final boolean showIstwert) {
        if (showIstwert) {
			this.table.setVisibleColumns("text", "sollwert", "istwert");
		} else {
			this.table.setVisibleColumns("text", "sollwert");
		}
    }

    public void setTranslationService(TranslationService translationService) {
        this.translationService = translationService;
    }

    public Table getTable() {
        return this.table;
    }
}
