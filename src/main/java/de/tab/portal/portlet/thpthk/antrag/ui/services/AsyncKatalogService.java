package de.tab.portal.portlet.thpthk.antrag.ui.services;

import de.tab.portal.client.katalog.model.KatalogKeyText;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by vollmar on 08.01.2016.
 */
public interface AsyncKatalogService {

	@Async(value = "taskExecutor")
	Future<List<KatalogKeyText>> getKatalogKeyText(String name);
}
