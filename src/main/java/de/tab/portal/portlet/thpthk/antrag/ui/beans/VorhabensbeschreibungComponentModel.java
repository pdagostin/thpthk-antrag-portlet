package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by paolo.dagostin on 3.5.2016..
 */
public class VorhabensbeschreibungComponentModel implements Serializable {

	private static final long serialVersionUID = -6467396582155413522L;

	@NotNull
	@NotBlank(message = "{tab.validation.NotBlank.message}")
	@Size(max = 90, message = "{tab.validation.Size.message}")
	private String beschreibungKurz;

	@NotNull
	@NotBlank(message = "{tab.validation.NotBlank.message}}")
	@Size(max = 600, message = "{tab.validation.Size.message}}")
	private String beschreibungLang;

	public String getBeschreibungKurz() {
		return beschreibungKurz;
	}

	public void setBeschreibungKurz(String beschreibungKurz) {
		this.beschreibungKurz = beschreibungKurz;
	}

	public String getBeschreibungLang() {
		return beschreibungLang;
	}

	public void setBeschreibungLang(String beschreibungLang) {
		this.beschreibungLang = beschreibungLang;
	}

	@Override
	public String toString(){
		return "VorhabensbeschreibungComponentModel{" +
				"beschreibungKurz='" + this.beschreibungKurz + '\'' +
				", beschreibungLang='" + this.beschreibungLang + '\'' +
				'}';
	}
}
