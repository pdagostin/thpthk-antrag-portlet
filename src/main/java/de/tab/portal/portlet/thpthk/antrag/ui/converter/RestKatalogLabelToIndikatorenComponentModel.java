package de.tab.portal.portlet.thpthk.antrag.ui.converter;

import de.tab.portal.client.katalog.model.RestKatalogLabel;
import de.tab.portal.commons.converter.TypeConverter;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.IndikatorenComponentModel;
import org.springframework.core.convert.converter.Converter;

/**
 * @author hrvoje.krot on 4.3.2016.
 */
@TypeConverter
public class RestKatalogLabelToIndikatorenComponentModel implements Converter<RestKatalogLabel, IndikatorenComponentModel> {

    @Override
    public IndikatorenComponentModel convert(final RestKatalogLabel source) {
        final IndikatorenComponentModel model = new IndikatorenComponentModel();
        model.setKey(source.key);
        model.setIndikator(source.key);
        model.setText(source.text);
        model.setIndikatorTyp(source.indikatorTyp);
        model.setIndikatorKatalogName(source.indikatorKatalogName);
        model.setRequired(source.required);

        return model;
    }
}
