package de.tab.portal.portlet.thpthk.antrag.api.services;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;

import de.tab.portal.client.print.model.Content;
import de.tab.portal.client.print.model.GenericPrintModel;
import de.tab.portal.client.print.service.PrintClient;
import de.tab.portal.client.workflow.entity.ws.WsAufgabe;
import de.tab.portal.client.workflow.service.WorkflowClientService;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.VorhabenPrintService;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

@Service
public class PdfPrinter {
	private final Logger logger = LoggerFactory.getLogger(PdfPrinter.class);

	private final WorkflowClientService workflowClientService;

	private final VorhabenPrintService vorhabenPrintService;

	private final PrintClient printClient;

	@Inject
	private TranslationService translationService;

	@Autowired
	public PdfPrinter(final WorkflowClientService workflowClientService, final VorhabenPrintService vorhabenPrintService,
			final PrintClient printClient) {
		super();
		this.workflowClientService = workflowClientService;
		this.vorhabenPrintService = vorhabenPrintService;
		this.printClient = printClient;
	}

	public class PrintResult {
		public InputStreamResource body = null;
		public long contentLength = 0;
	}

	public PrintResult print(final Long vorhabenId, final String aufgabenId, final String dokumentKlassenKennung) {
		this.logger.info("starte print des Antrags-pdf");

		Assert.notNull(dokumentKlassenKennung);
		Assert.notNull(vorhabenId);

		final StopWatch sw = new StopWatch();
		sw.start();

		final boolean antrag = dokumentKlassenKennung.equalsIgnoreCase("antrag");

		final List<Content> contents = this.vorhabenPrintService.getAntragPrintContents(vorhabenId, antrag);

		final GenericPrintModel genericPrintModel = new GenericPrintModel();

		// genericPrintModel.metadata.subtitle = this.translationService.translate("print.antrag.subtitle");

		// TODO Antragpruefen Kopf ist noch nicht definiert
		// if (antrag) {
		genericPrintModel.metadata.title =
				"Antrag auf Mittel für Technische Hilfe\naus dem Europäischen Fonds\nfür regionale Entwicklung (EFRE)";
		// } else {
		// String aktenzeichen = this.vorhabenPrintService.getAktenzeichen(vorhabenId);
		// if (StringUtils.hasText(aktenzeichen)) {
		// genericPrintModel.metadata.title = "Details zum Vorhaben: " + aktenzeichen;
		// } else {
		// genericPrintModel.metadata.title = "Details zum Vorhaben: " + this.getAufgabe(aufgabenId).getFoerderFallId();
		// }
		// }
		genericPrintModel.metadata.id = this.getAufgabe(aufgabenId).getFoerderFallId();
		genericPrintModel.metadata.logo = "genericJasperReports/images/EFRE-EU.jpg";

		genericPrintModel.content.addAll(contents);

		final GenericPrintModel[] printmodels = new GenericPrintModel[]{genericPrintModel};

		final byte[] genericPdf = this.printClient.printGenericPDFDocumentsToByteArray(printmodels);

		final PrintResult printResult = new PrintResult();
		printResult.body = new InputStreamResource(new ByteArrayInputStream(genericPdf));
		printResult.contentLength = genericPdf.length;

		sw.stop();
		this.logger.info("print finished\n {}", sw.prettyPrint());

		return printResult;
	}

	private WsAufgabe getAufgabe(final String aufgabenId) {
		return this.workflowClientService.getAufgabenService().getAufgabe(aufgabenId);
	}

}
