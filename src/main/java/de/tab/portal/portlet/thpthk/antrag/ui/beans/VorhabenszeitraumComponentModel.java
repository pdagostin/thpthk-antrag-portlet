package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author abeblek
 * @since 09-Dec-15.
 */
public class VorhabenszeitraumComponentModel implements Serializable {

    private static final long serialVersionUID = 429029053169662425L;

    @NotNull
    private Date beginn;

    @NotNull
    private Date ende;

    public Date getBeginn() {
        return this.beginn;
    }

    public void setBeginn(final Date beginn) {
        this.beginn = beginn;
    }

    public Date getEnde() {
        return this.ende;
    }

    public void setEnde(final Date ende) {
        this.ende = ende;
    }

    @Override
    public String toString() {
        return "VorhabenszeitraumModel [beginn=" + this.beginn + ", ende=" + this.ende + "]";
    }

}
