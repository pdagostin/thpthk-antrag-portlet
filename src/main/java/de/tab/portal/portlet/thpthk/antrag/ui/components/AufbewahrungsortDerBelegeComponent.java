package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AufbewahrungsortDerBelegeComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

/**
 * Created by Nikola Sapun on 3/2/2016.
 */

@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "belegeAufbewahrungsort",
                caption = @Caption({"thpthk.antrag.vorhaben.inhalt.aufbewahrungsortDerBelegeComponent.aufbewahrungsort"})
        )})
public class AufbewahrungsortDerBelegeComponent extends FormLayout implements Item.Editor {

    private static final long serialVersionUID = 7247407479585006515L;

    @PropertyId("belegeAufbewahrungsort")
    private final TextField belegeAufbewahrungsort = VaadinUtils.buildBasicTextField();

    public AufbewahrungsortDerBelegeComponent() {
        this.buildLayout();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.belegeAufbewahrungsort.setWidth(100, Sizeable.Unit.EX);
        this.addComponents(this.belegeAufbewahrungsort);
    }

    private void initValidators() {
        VaadinUtils.configField(this.belegeAufbewahrungsort, AufbewahrungsortDerBelegeComponentModel.class, "belegeAufbewahrungsort");
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }
}
