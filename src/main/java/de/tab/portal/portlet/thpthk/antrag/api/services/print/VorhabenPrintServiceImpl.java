package de.tab.portal.portlet.thpthk.antrag.api.services.print;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.tab.portal.client.katalog.KatalogClientService;
import de.tab.portal.client.persistence.PortalPersistenceRestClient;
import de.tab.portal.client.persistence.model.kontakt.Adresse;
import de.tab.portal.client.persistence.model.kontakt.Antragsteller;
import de.tab.portal.client.persistence.model.kontakt.AntragstellerSpezifischeDaten;
import de.tab.portal.client.persistence.model.kontakt.Person;
import de.tab.portal.client.persistence.model.kontakt.Postfach;
import de.tab.portal.client.persistence.model.vorhaben.VorhabenOrt;
import de.tab.portal.client.print.common.PrintFormatUtil;
import de.tab.portal.client.print.model.Content;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.AdresseStempelPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.GridPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoAdressePrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoAnsprechpartnerPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoAnsprechpartnerPrintModelData;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoAufbewahrungsortPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoAusgabenAufteilungPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoAusgabenAufteilungPrintModelData;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoBranchePrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoErklaerungenPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoErklaerungenPrintModelData;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoFinanzierungsplanMittelPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoFinanzierungsplanMittelPrintModelData;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoPostfachPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoSpezifischeDatenPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoUnterschriftPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoVorhabensbeschreibungKurzLangPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoVorhabensortePrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoVorhabensortePrintModelData;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.KoVorhabenszeitraumPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.StyledTextPrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.TablePrintModel;
import de.tab.portal.portlet.thpthk.antrag.api.services.print.model.TablePrintModelData;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AufteilungDerAusgabenAufJahreComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.IndikatorenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.StammdatenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.VorhabenszeitraumComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.persistence.model.ThpthkVorhaben;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

/**
 *
 * @author Kloss
 *
 */
@Component
public class VorhabenPrintServiceImpl implements VorhabenPrintService {

	private final PortalPersistenceRestClient portalPersistenceRestClient;

	private final KatalogClientService katalogClientService;

	private final TranslationService translationService;

	final ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	public VorhabenPrintServiceImpl(final PortalPersistenceRestClient portalPersistenceRestClient,
			final KatalogClientService katalogClientService, final TranslationService translationService) {
		super();
		this.portalPersistenceRestClient = portalPersistenceRestClient;
		this.katalogClientService = katalogClientService;
		this.translationService = translationService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see VorhabenPrintService#getAktenzeichen(java.lang.Long)
	 */
	@Override
	public String getAktenzeichen(final Long vorhabenId) {
		final String vorhabenUrl = this.portalPersistenceRestClient.rel("thpthkVorhabens").concat(vorhabenId.toString());
		final Resource<ThpthkVorhaben> tcmVorhabenResource =
				this.portalPersistenceRestClient.toResource(vorhabenUrl, new ParameterizedTypeReference<Resource<ThpthkVorhaben>>() {
				});
		final ThpthkVorhaben tcmVorhaben = tcmVorhabenResource.getContent();
		return tcmVorhaben.aktenzeichen;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.tab.portal.portlet.thpthk.antrag.ui.services.VorhabenPrintService#getAntragPrintContents(java.lang.Long)
	 */
	@Override
	public List<Content> getAntragPrintContents(final Long vorhabenId, final boolean antrag) {
		Assert.notNull(vorhabenId);

		final ArrayList<Content> contents = new ArrayList<Content>();

		final String vorhabenUrl = this.portalPersistenceRestClient.rel("thpthkVorhabens").concat(vorhabenId.toString());

		final Resource<ThpthkVorhaben> thpthkVorhabenResource =
				this.portalPersistenceRestClient.toResource(vorhabenUrl, new ParameterizedTypeReference<Resource<ThpthkVorhaben>>() {
				});

		final ThpthkVorhaben thpthkVorhaben = thpthkVorhabenResource.getContent();

		Antragsteller antragsteller = null;
		final String antragstellerUrl = thpthkVorhabenResource.getLink("antragsteller").getHref();
		final Resource<Antragsteller> antragstellerResource =
				this.portalPersistenceRestClient.toResource(antragstellerUrl, new ParameterizedTypeReference<Resource<Antragsteller>>() {
				});
		if (antragstellerResource != null) {
			antragsteller = antragstellerResource.getContent();
		}
		if (antragsteller == null) {
			antragsteller = new Antragsteller();
		}

		// Antragsteller --> Grunddaten
		contents.add(this.getAdressfeldPrintContent());
		contents.add(new Content("styledText", new StyledTextPrintModel(null)));
		contents.add(this.getEinordnungPrintContent(thpthkVorhaben, antragsteller));
		contents.add(this.getAdressePrintContent(antragsteller));
		contents.add(this.getPostfachPrintContent(antragsteller));
		contents.add(this.getAnsprechpartnerPrintContent(thpthkVorhabenResource));
		// Antragsteller --> spezifische Daten
		contents.add(this.getSpezifischeDatenPrintContent(antragsteller));

		// Vorhaben --> Grunddaten
		contents.add(this.getVorhabenszeitraumPrintContent(thpthkVorhaben));
		contents.add(this.getBranchePrintContent(thpthkVorhaben));
		contents.add(this.getVorhabensortPrintContent(thpthkVorhabenResource));
		// Vorhaben --> Inhalt
		contents.add(this.getVorhabensbeschreibungPrintContent(thpthkVorhaben));
		contents.add(this.getAufbewahrungsortPrintContent(thpthkVorhaben));
		// Vorhaben --> Ausgaben
		contents.add(this.getGesamtausgabenPrintContent(thpthkVorhaben));
		contents.add(this.getAusgabenAufteilungPrintContent(thpthkVorhabenResource));
		// Vorhaben --> Indikatoren
		contents.add(this.getIndikatorenPrintContent(thpthkVorhabenResource, antrag));

		if (!antrag) {
			contents.add(this.getVorhabenStammdatenPrint(thpthkVorhaben));
			contents.add(this.getBewilligteAusgabenPrintContent(thpthkVorhaben));
			contents.add(this.getAktuellerBewilligungsstandPrintContent(thpthkVorhaben));
			contents.add(this.getFinanzierungMittelaufteilungPrintContent(thpthkVorhabenResource));
			contents.add(this.getAnsprechpartnerMeldestellePrintContent(thpthkVorhabenResource));
		} else {
			contents.add(this.getErklaerungenPrintContent());
			contents.add(new Content("styledText", new StyledTextPrintModel("\n\n")));
			contents.add(this.getUnterschriftPrintContent());
		}

		return contents;
	}

	private Content getEinordnungPrintContent(final ThpthkVorhaben thpthkVorhaben, final Antragsteller antragsteller) {
		final GridPrintModel gridPrintModel = new GridPrintModel();

		String foerdergegenstand = this.katalogClientService.getKatalogTextForKey("Foerdergegenstand_THP", thpthkVorhaben.foerdergegenstand);
		if(StringUtils.isEmpty(foerdergegenstand)){
			foerdergegenstand = this.katalogClientService.getKatalogTextForKey("Foerdergegenstand_THK", thpthkVorhaben.foerdergegenstand);
		}

		gridPrintModel.data.add(new String[]{
				"Einordnung des Antragstellers",
				this.katalogClientService.getKatalogTextForKey("EinordnungAntragstellerTHP_THK", thpthkVorhaben.einordnungAntragsteller),
				"Fördergegenstand", foerdergegenstand
		});

		return new Content("grid2Col", gridPrintModel);
	}

	private Content getAdressfeldPrintContent() {
		final AdresseStempelPrintModel adresseStempelPrintModel = new AdresseStempelPrintModel();

		adresseStempelPrintModel.text = this.translationService.translate("print.antrag.subtitle.adresse");
		adresseStempelPrintModel.text1 = "Nicht vom Antragsteller auszufüllen";
		adresseStempelPrintModel.text2 = "Eingangsstempel";
		adresseStempelPrintModel.text3 = "Vorhaben-Nr.:";

		return new Content("adresseStempel", adresseStempelPrintModel);
	}

	private Content getAdressePrintContent(final Antragsteller antragsteller) {
		if (antragsteller.adresse == null) {
			antragsteller.adresse = new Adresse();
		}

		final KoAdressePrintModel koAdressePrintModel =
				this.objectMapper.convertValue(antragsteller.adresse, KoAdressePrintModel.class);

		koAdressePrintModel.land = this.katalogClientService.getKatalogTextForKey("Laenderschluessel", koAdressePrintModel.land);

		return new Content("adresseGes", koAdressePrintModel);
	}

	private Content getPostfachPrintContent(final Antragsteller antragsteller) {
		if (antragsteller.postfach == null) {
			antragsteller.postfach = new Postfach();
		}

		final KoPostfachPrintModel koPostfachPrintModel =
				this.objectMapper.convertValue(antragsteller.postfach, KoPostfachPrintModel.class);

		final Content content = new Content("postfach", koPostfachPrintModel);

		return content;
	}

	private Content getAnsprechpartnerPrintContent(final Resource<ThpthkVorhaben> tcmVorhabenResource) {
		final String ansprechpartnerUrl = tcmVorhabenResource.getLink("ansprechpartner").getHref();

		final Resources<Resource<KoAnsprechpartnerPrintModelData>> ansprechpartnerResources =
				this.portalPersistenceRestClient.toResources(ansprechpartnerUrl,
						new ParameterizedTypeReference<Resources<Resource<KoAnsprechpartnerPrintModelData>>>() {
						});

		final KoAnsprechpartnerPrintModel koAnsprechpartnerPrintModel = new KoAnsprechpartnerPrintModel();

		for (final Resource<KoAnsprechpartnerPrintModelData> resource : ansprechpartnerResources.getContent()) {
			final KoAnsprechpartnerPrintModelData content = resource.getContent();
			content.rolle = this.katalogClientService.getKatalogTextForKey("RollenAnsprechpartner", content.rolle);
			koAnsprechpartnerPrintModel.data.add(content);
		}

		final Content content = new Content("ansprechpartnerT5", koAnsprechpartnerPrintModel);

		return content;
	}

	private Content getSpezifischeDatenPrintContent(final Antragsteller antragsteller) {
		if (antragsteller.spezifischeDaten == null) {
			antragsteller.spezifischeDaten = new AntragstellerSpezifischeDaten();
		}

		final KoSpezifischeDatenPrintModel koSpezifischeDatenPrintModel = new KoSpezifischeDatenPrintModel();

		koSpezifischeDatenPrintModel.erstattungsfaehigkeitMwst =
				PrintFormatUtil.format(antragsteller.spezifischeDaten.erstattungsfaehigkeitMwst);
		koSpezifischeDatenPrintModel.gruendungsdatum =
				PrintFormatUtil.format(antragsteller.spezifischeDaten.gruendungsdatum);
		koSpezifischeDatenPrintModel.branche =
				this.katalogClientService.getKatalogTextForKey("branche", antragsteller.spezifischeDaten.branche);
		koSpezifischeDatenPrintModel.einordnungVertragspartner =
				this.katalogClientService.getKatalogTextForKey("BeguenstigtenEinordnung",
						antragsteller.spezifischeDaten.einordnung);
		koSpezifischeDatenPrintModel.rechtsform =
				this.katalogClientService.getKatalogTextForKey("Rechtsform", antragsteller.spezifischeDaten.rechtsform);

		final Content content = new Content("spezifischeDaten", koSpezifischeDatenPrintModel);

		return content;
	}

	private Content getVorhabenszeitraumPrintContent(final ThpthkVorhaben tcmVorhaben) {
		if (tcmVorhaben.zeitraum == null) {
			tcmVorhaben.zeitraum = new VorhabenszeitraumComponentModel();
		}

		final KoVorhabenszeitraumPrintModel koVorhabenszeitraumPrintModel =
				this.objectMapper.convertValue(tcmVorhaben.zeitraum, KoVorhabenszeitraumPrintModel.class);

		koVorhabenszeitraumPrintModel.beginn = PrintFormatUtil.format(tcmVorhaben.zeitraum.getBeginn());
		koVorhabenszeitraumPrintModel.ende = PrintFormatUtil.format(tcmVorhaben.zeitraum.getEnde());

		final Content content = new Content("vorhabenszeitraum", koVorhabenszeitraumPrintModel);

		return content;
	}

	private Content getBranchePrintContent(final ThpthkVorhaben tcmVorhaben) {
		final KoBranchePrintModel koBranchePrintModel =
				this.objectMapper.convertValue(tcmVorhaben, KoBranchePrintModel.class);

		koBranchePrintModel.branche = this.katalogClientService.getKatalogTextForKey("branche", koBranchePrintModel.branche);

		final Content content = new Content("branche", koBranchePrintModel);

		return content;
	}

	private Content getVorhabensortPrintContent(final Resource<ThpthkVorhaben> tcmVorhabenResource) {

		final String orteUrl = tcmVorhabenResource.getLink("orte").getHref();
		final Resources<Resource<VorhabenOrt>> ortResources =
				this.portalPersistenceRestClient.toResources(orteUrl,
						new ParameterizedTypeReference<Resources<Resource<VorhabenOrt>>>() {
						});
		final Collection<Resource<VorhabenOrt>> resourceCollection = ortResources.getContent();

		final KoVorhabensortePrintModel koVorhabensortePrintModel = new KoVorhabensortePrintModel();

		for (final Resource<VorhabenOrt> resource : resourceCollection) {
			final VorhabenOrt content = resource.getContent();

			if (content.adresse == null) {
				content.adresse = new Adresse();
			}

			final KoVorhabensortePrintModelData koVorhabensortePrintModelData =
					this.objectMapper.convertValue(content.adresse, KoVorhabensortePrintModelData.class);

			koVorhabensortePrintModelData.land =
					this.katalogClientService.getKatalogTextForKey("Laenderschluessel", koVorhabensortePrintModelData.land);

			koVorhabensortePrintModel.data.add(koVorhabensortePrintModelData);
		}

		final Content content = new Content("vorhabensorte", koVorhabensortePrintModel);

		return content;
	}

	private Content getVorhabensbeschreibungPrintContent(final ThpthkVorhaben tcmVorhaben) {
		final KoVorhabensbeschreibungKurzLangPrintModel koVorhabensbeschreibungKurzLangPrintModel =
				this.objectMapper.convertValue(tcmVorhaben, KoVorhabensbeschreibungKurzLangPrintModel.class);

		final Content content = new Content("vorhabensbeschreibungKurzLang", koVorhabensbeschreibungKurzLangPrintModel);

		return content;
	}

	private Content getAufbewahrungsortPrintContent(final ThpthkVorhaben tcmVorhaben) {
		final KoAufbewahrungsortPrintModel koAufbewahrungsortPrintModel =
				this.objectMapper.convertValue(tcmVorhaben, KoAufbewahrungsortPrintModel.class);

		final Content content = new Content("aufbewahrungsort", koAufbewahrungsortPrintModel);

		return content;
	}

	private Content getGesamtausgabenPrintContent(final ThpthkVorhaben tcmVorhaben) {
		final GridPrintModel gridPrintModel = new GridPrintModel();

		gridPrintModel.title = "Gesamtausgaben";
		gridPrintModel.data.add(new String[]{"Gesamtausgaben (€)", PrintFormatUtil.format(tcmVorhaben.gesamtausgabenPlan)});

		return new Content("grid1Col", gridPrintModel);
	}

	private Content getAusgabenAufteilungPrintContent(final Resource<ThpthkVorhaben> tcmVorhabenResource) {
		final String ausgabenUrl = tcmVorhabenResource.getLink("ausgabenplan").getHref();

		final Resources<Resource<AufteilungDerAusgabenAufJahreComponentModel>> ausgabenResources =
				this.portalPersistenceRestClient.toResources(ausgabenUrl,
						new ParameterizedTypeReference<Resources<Resource<AufteilungDerAusgabenAufJahreComponentModel>>>() {
						});

		final KoAusgabenAufteilungPrintModel koAusgabenAufteilungPrintModel = new KoAusgabenAufteilungPrintModel();

		for (final Resource<AufteilungDerAusgabenAufJahreComponentModel> resource : ausgabenResources.getContent()) {
			final AufteilungDerAusgabenAufJahreComponentModel koAusgabenAufteilungPrintModelData = resource.getContent();

			final KoAusgabenAufteilungPrintModelData koAusgabenAufteilungPrintModelData2 = new KoAusgabenAufteilungPrintModelData();
			koAusgabenAufteilungPrintModelData2.jahr = koAusgabenAufteilungPrintModelData.getJahr();
			koAusgabenAufteilungPrintModelData2.foerderfaehigAeb =
					PrintFormatUtil.format(koAusgabenAufteilungPrintModelData.getZuwendungsausgaben());
			koAusgabenAufteilungPrintModelData2.gesamtInvAeb =
					PrintFormatUtil.format(koAusgabenAufteilungPrintModelData.getGesamtausgaben());

			koAusgabenAufteilungPrintModel.data.add(koAusgabenAufteilungPrintModelData2);
		}

		final Content content = new Content("ausgabenAufteilung", koAusgabenAufteilungPrintModel);

		return content;
	}

	private Content getIndikatorenPrintContent(final Resource<ThpthkVorhaben> thpthkVorhabenResource, final boolean antrag) {
		final String indikatorenUrl = thpthkVorhabenResource.getLink("indikatoren").getHref();
		final Resources<Resource<IndikatorenComponentModel>> resources = this.portalPersistenceRestClient.toResources(indikatorenUrl,
				new ParameterizedTypeReference<Resources<Resource<IndikatorenComponentModel>>>() {
				});

		final TablePrintModel tablePrintModel = new TablePrintModel();
		tablePrintModel.title = "Indikatoren";
		tablePrintModel.titleColumn1 = "Indikator";
		tablePrintModel.titleColumn2 = "Sollwert";
		tablePrintModel.titleColumn3 = "Istwert";

		for (final Resource<IndikatorenComponentModel> resource : resources) {
			final IndikatorenComponentModel content = resource.getContent();

			final TablePrintModelData tablePrintModelData = new TablePrintModelData();

			tablePrintModelData.dataCol2 = content.getSollwert();
			tablePrintModelData.dataCol3 = content.getIstwert();

			// Keys sind auch über mehrere Kataloge eindeutig, weshalb durch alle Kataloge iteriert werden kann
			// --> Fördergegenstand spielt keine Rolle
			String text = null;
			text =
					this.katalogClientService.getKatalogTextForKey("Indikatoren_Verwaltung_und_KontrolleTHP", content.getIndikator());
			if (StringUtils.isEmpty(text)) {
				text =
						this.katalogClientService.getKatalogTextForKey("Indikatoren_Elektronisches_THP", content.getIndikator());
			}
			if (StringUtils.isEmpty(text)) {
				text =
						this.katalogClientService.getKatalogTextForKey("Indikatoren_Informations_und_KommunikationsmassnahmenTHK",
								content.getIndikator());
			}
			if (StringUtils.isEmpty(text)) {
				text =
						this.katalogClientService.getKatalogTextForKey("Indikatoren_Monitoring_und_EvaluierungTHP", content.getIndikator());
			}

			tablePrintModelData.dataCol1 = text;

			tablePrintModel.data.add(tablePrintModelData);
		}

		// istwert im antrag nicht sichtbar
		if (antrag) {
			return new Content("Table2Col", tablePrintModel);
		} else {
			return new Content("Table3Col", tablePrintModel);
		}
	}

	private Content getErklaerungenPrintContent() {
		final KoErklaerungenPrintModel koErklaerungenPrintModel = new KoErklaerungenPrintModel();

		koErklaerungenPrintModel.data
				.add(new KoErklaerungenPrintModelData(
						"Beschreibung des Vorhabens (Bezeichnung, Inhalt, Beitrag zur Gewährleistung einer effizienten Programmumsetzung/ öffentlichkeitswirksame Umsetzung des OP sowie Gleichstellung und Nachhaltigkeit)"));
		koErklaerungenPrintModel.data.add(new KoErklaerungenPrintModelData(
				"Angaben, dass Finanzierung aus den anderen Prioritätsachsen nicht möglich ist"));
		koErklaerungenPrintModel.data.add(new KoErklaerungenPrintModelData("Finanzierungsplan und Finanzierungsnachweis"));
		koErklaerungenPrintModel.data.add(new KoErklaerungenPrintModelData("Ausgabenplanung nach Einzelpositionen"));
		koErklaerungenPrintModel.data
				.add(new KoErklaerungenPrintModelData(
						"Angaben, dass das Vorhaben insofern rechtzeitig fertiggestellt werden wird, dass das Projektcontrolling vor dem 31.12.2023 abgeschlossen werden kann"));
		koErklaerungenPrintModel.data.add(new KoErklaerungenPrintModelData("Angaben zur Zuverlässigkeit, Eignung des Begünstigten"));
		koErklaerungenPrintModel.data.add(new KoErklaerungenPrintModelData("Ausführungen zur Vergabe; soweit zutreffend"));
		koErklaerungenPrintModel.data.add(new KoErklaerungenPrintModelData("Angaben zur Inanspruchnahme anderer Förderprogramme"));

		return new Content("erklaerungen", koErklaerungenPrintModel);
	}

	private Content getUnterschriftPrintContent() {
		return new Content("unterschriftMitHinweis", new KoUnterschriftPrintModel());
	}

	private Content getVorhabenStammdatenPrint(final ThpthkVorhaben thpthkVorhaben) {
		if (thpthkVorhaben.stammdaten == null) {
			thpthkVorhaben.stammdaten = new StammdatenComponentModel();
		}

		final GridPrintModel stammdaten = new GridPrintModel();

		stammdaten.title = "Stammdaten";
		stammdaten.data.add(new String[]{
				"Aktenzeichen", thpthkVorhaben.aktenzeichen,
				"EU-Projekt", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getEuProjekt())
		});
		stammdaten.data.add(new String[]{
				"Förderperiode",
				this.katalogClientService.getKatalogTextForKey("FoerderperiodeOP", thpthkVorhaben.stammdaten.getFoerderperiode() != null
						? thpthkVorhaben.stammdaten.getFoerderperiode().getKey() : null),
				"Antragsdatum", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getAntragsdatum())
		});
		stammdaten.data.add(new String[]{
				"Eingangsdatum", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getEingangsdatum()),
				"Bewilligungsdatum", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getBewilligungsdatum())
		});
		stammdaten.data.add(new String[]{
				"Ende Zweckbindefrist", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getZweckbindefrist()),
				"VWN - Eingang", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getVwneingang())
		});
		stammdaten.data.add(new String[]{
				"VWN - Prüfung", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getVwnpruefung()),
				"Insolvenzdatum", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getInsolvenzdatum())
		});
		stammdaten.data.add(new String[]{
				"Registriernummer", PrintFormatUtil.format("%s", thpthkVorhaben.stammdaten.getRegistriernummer()),
				"Ende Abruffrist", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getEndeAbruffrist())
		});
		stammdaten.data.add(new String[]{
				"Archivierungsdatum", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getArchivierungsdatum()),
				"Physischer Abschluss des Projektes", PrintFormatUtil.format(thpthkVorhaben.stammdaten.getPhysischerAbschluss())
		});

		return new Content("grid2Col", stammdaten);
	}

	private Content getBewilligteAusgabenPrintContent(final ThpthkVorhaben tcmVorhaben) {
		final GridPrintModel gridPrintModel = new GridPrintModel();

		gridPrintModel.title = "Bewilligte Ausgaben";
		gridPrintModel.data.add(new String[]{
				"Gesamtausgaben Bewilligung", PrintFormatUtil.format(tcmVorhaben.gesamtausgaben),
				"förderfähige Ausgaben Bewilligung", PrintFormatUtil.format(tcmVorhaben.gesamtausgabenFoerderfaehig),
				"Fördersatz", PrintFormatUtil.format(tcmVorhaben.foerdersatz)
		});

		return new Content("grid3Col", gridPrintModel);
	}

	private Content getAktuellerBewilligungsstandPrintContent(final ThpthkVorhaben tcmVorhaben) {
		final GridPrintModel gridPrintModel = new GridPrintModel();

		gridPrintModel.title = "Aktueller Bewilligungsstand";
		gridPrintModel.data.add(new String[]{
				"Gesamtausgaben Bewilligung / Änderung (€)", PrintFormatUtil.format(tcmVorhaben.gesamtausgabenBew),
				"förderfähige Ausgaben Bewilligung / Änderung (€)", PrintFormatUtil.format(tcmVorhaben.foerderAusgabenBew),
				"Fördersatz (%)", PrintFormatUtil.format(tcmVorhaben.foerdersatzBew)
		});

		return new Content("grid3Col", gridPrintModel);
	}

	private Content getFinanzierungMittelaufteilungPrintContent(final Resource<ThpthkVorhaben> tcmVorhabenResource) {
		final String finanzierungsplanLink = tcmVorhabenResource.getLink("finanzierungplan").getHref();

		final Resources<Resource<KoFinanzierungsplanMittelPrintModelData>> finanzierungResources =
				this.portalPersistenceRestClient.toResources(finanzierungsplanLink,
						new ParameterizedTypeReference<Resources<Resource<KoFinanzierungsplanMittelPrintModelData>>>() {
						});

		final KoFinanzierungsplanMittelPrintModel koFinanzierungsplanMittelPrintModel = new KoFinanzierungsplanMittelPrintModel();

		for (final Resource<KoFinanzierungsplanMittelPrintModelData> resource : finanzierungResources.getContent()) {
			final KoFinanzierungsplanMittelPrintModelData koFinanzierungsplanMittelPrintModelData = resource.getContent();

			koFinanzierungsplanMittelPrintModelData.bundesmittel =
					PrintFormatUtil.formatBigDecimalString(koFinanzierungsplanMittelPrintModelData.bundesmittel);
			koFinanzierungsplanMittelPrintModelData.eumittel =
					PrintFormatUtil.formatBigDecimalString(koFinanzierungsplanMittelPrintModelData.eumittel);
			koFinanzierungsplanMittelPrintModelData.foerderfaehigInvest =
					PrintFormatUtil.formatBigDecimalString(koFinanzierungsplanMittelPrintModelData.foerderfaehigInvest);
			koFinanzierungsplanMittelPrintModelData.grunderwerb =
					PrintFormatUtil.formatBigDecimalString(koFinanzierungsplanMittelPrintModelData.jahr);
			koFinanzierungsplanMittelPrintModelData.kommunalmittel =
					PrintFormatUtil.formatBigDecimalString(koFinanzierungsplanMittelPrintModelData.kommunalmittel);
			koFinanzierungsplanMittelPrintModelData.landesmittel =
					PrintFormatUtil.formatBigDecimalString(koFinanzierungsplanMittelPrintModelData.landesmittel);
			koFinanzierungsplanMittelPrintModelData.privatemittel =
					PrintFormatUtil.formatBigDecimalString(koFinanzierungsplanMittelPrintModelData.privatemittel);
			koFinanzierungsplanMittelPrintModelData.zuschuss =
					PrintFormatUtil.formatBigDecimalString(koFinanzierungsplanMittelPrintModelData.zuschuss);

			koFinanzierungsplanMittelPrintModel.data.add(koFinanzierungsplanMittelPrintModelData);
		}

		final Content content = new Content("finanzierungsplanMittel", koFinanzierungsplanMittelPrintModel);

		return content;
	}

	private Content getAnsprechpartnerMeldestellePrintContent(
			final Resource<ThpthkVorhaben> tcmVorhabenResource) {
		final String urlAnsprechpartnerMeldestelle = tcmVorhabenResource.getLink("ansprechpartnerMeldestelle").getHref();

		final Resource<Person> resource =
				this.portalPersistenceRestClient.toResource(urlAnsprechpartnerMeldestelle,
						new ParameterizedTypeReference<Resource<Person>>() {
						});

		final Person ansprechpartner = (resource != null) && (resource.getContent() != null) ? resource.getContent() : new Person();

		final GridPrintModel gridPrintModel = new GridPrintModel();

		gridPrintModel.title = "Ansprechpartner Meldestelle";
		gridPrintModel.data.add(new String[]{"Name, Vorname", ansprechpartner.name, "Telefon", ansprechpartner.telefon});
		gridPrintModel.data.add(new String[]{"E-Mail-Adresse", ansprechpartner.email});

		return new Content("grid2Col", gridPrintModel);
	}

}
