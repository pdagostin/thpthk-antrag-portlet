package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoAusgabenAufteilungPrintModelData {

	public String jahr;
	public String gesamtInvAeb;
	public String foerderfaehigAeb;

}
