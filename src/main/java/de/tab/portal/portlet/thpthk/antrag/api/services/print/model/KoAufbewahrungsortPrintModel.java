package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoAufbewahrungsortPrintModel {

	public String title = "Aufbewahrungsort";
	public String belegeAufbewahrungsortBezeichnung =
			"Aufbewahrungsort der Belege";
	public String belegeAufbewahrungsort;

}
