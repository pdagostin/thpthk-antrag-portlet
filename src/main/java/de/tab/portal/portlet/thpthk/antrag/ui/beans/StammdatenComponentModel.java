package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import de.tab.portal.commons.DefaultKeyText;
import de.tab.portal.commons.KeyText;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author idekic
 * @since 14-Mar-16.
 */
public class StammdatenComponentModel implements Serializable {

    @NotNull
    private String aktenzeichen;

    @NotNull
    private Boolean euProjekt;

    private KeyText foerderperiode = new DefaultKeyText("EU4");;

    @NotNull
    private Date antragsdatum;

    @NotNull
    private Date eingangsdatum;

    @NotNull
    private Date bewilligungsdatum;

    private Date zweckbindefrist;

    private Date vwneingang;

    private Date vwnpruefung;

    private Date insolvenzdatum;

    @Size(max = 255, message = "{tab.validation.Size.message}")
    private String registriernummer;

    private Date endeAbruffrist;

    private Date archivierungsdatum;

    private Date physischerAbschluss;

    public String getAktenzeichen() {
        return aktenzeichen;
    }

    public void setAktenzeichen(String aktenzeichen) {
        this.aktenzeichen = aktenzeichen;
    }

    public Boolean getEuProjekt() {
        return euProjekt;
    }

    public void setEuProjekt(Boolean euProjekt) {
        this.euProjekt = euProjekt;
    }

    public KeyText getFoerderperiode() {
        return this.foerderperiode;
    }

    public void setFoerderperiode(KeyText foerderperiode) {
        this.foerderperiode = foerderperiode;
    }

    public Date getAntragsdatum() {
        return antragsdatum;
    }

    public void setAntragsdatum(Date antragsdatum) {
        this.antragsdatum = antragsdatum;
    }

    public Date getEingangsdatum() {
        return eingangsdatum;
    }

    public void setEingangsdatum(Date eingangsdatum) {
        this.eingangsdatum = eingangsdatum;
    }

    public Date getBewilligungsdatum() {
        return bewilligungsdatum;
    }

    public void setBewilligungsdatum(Date bewilligungsdatum) {
        this.bewilligungsdatum = bewilligungsdatum;
    }

    public Date getInsolvenzdatum() {
        return insolvenzdatum;
    }

    public void setInsolvenzdatum(Date insolvenzdatum) {
        this.insolvenzdatum = insolvenzdatum;
    }

    public String getRegistriernummer() {
        return registriernummer;
    }

    public void setRegistriernummer(String registriernummer) {
        this.registriernummer = registriernummer;
    }

    public Date getEndeAbruffrist() {
        return endeAbruffrist;
    }

    public void setEndeAbruffrist(Date endeAbruffrist) {
        this.endeAbruffrist = endeAbruffrist;
    }

    public Date getArchivierungsdatum() {
        return archivierungsdatum;
    }

    public void setArchivierungsdatum(Date archivierungsdatum) {
        this.archivierungsdatum = archivierungsdatum;
    }

    public Date getPhysischerAbschluss() {
        return physischerAbschluss;
    }

    public void setPhysischerAbschluss(Date physischerAbschluss) {
        this.physischerAbschluss = physischerAbschluss;
    }

    public Date getZweckbindefrist() {
        return zweckbindefrist;
    }

    public void setZweckbindefrist(Date zweckbindefrist) {
        this.zweckbindefrist = zweckbindefrist;
    }

    public Date getVwneingang() {
        return vwneingang;
    }

    public void setVwneingang(Date vwneingang) {
        this.vwneingang = vwneingang;
    }

    public Date getVwnpruefung() {
        return vwnpruefung;
    }

    public void setVwnpruefung(Date vwnpruefung) {
        this.vwnpruefung = vwnpruefung;
    }

    @Override
    public String toString() {
        return "StammdatenComponentModel{" +
                "aktenzeichen=" + this.aktenzeichen +
                ", euProjekt=" + this.euProjekt +
                ", foerderperiode=" + this.foerderperiode +
                ", antragsdatum=" + this.antragsdatum +
                ", eingangsdatum=" + this.eingangsdatum +
                ", bewilligungsdatum=" + this.bewilligungsdatum +
                ", zweckbindefrist=" + this.zweckbindefrist +
                ", vwnEingang=" + this.vwneingang +
                ", vwnPruefung=" + this.vwnpruefung +
                ", insolvenzdatum=" + this.insolvenzdatum +
                ", registriernummer=" + this.registriernummer +
                ", endeAbruffrist=" + this.endeAbruffrist +
                ", archivierungsdatum=" + this.archivierungsdatum +
                ", physischerAbschluss=" + this.physischerAbschluss +
                '}';
    }
}
