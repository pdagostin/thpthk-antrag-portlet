package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoAnsprechpartnerPrintModelData {

	public String email;
	public String fax;
	public String name;
	public String telefon;
	public String rolle;

}
