package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoErklaerungenPrintModel {

	public String title =
			"\n\nFolgende Erläuterungen zum Vorhaben sind dem Antrag beigefügt <style size=\"7\">(Unterlagen stehen im Förderportal in der Historie zur Verfügung)</style>\n\n";

	public List<KoErklaerungenPrintModelData> data = new ArrayList<KoErklaerungenPrintModelData>();

}
