package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Nikola Sapun on 3/1/2016.
 */
public class PostfachComponentModel implements Serializable {

    private static final long serialVersionUID = -3269903088339461641L;
    @Pattern(regexp = "\\d{5}", message = "{tab.validation.PLZ.Pattern.message}")
	private String plz;

	@Size(max = 255, message = "{tab.validation.Size.message}")
	private String postfach;


	public String getPlz() {
		return this.plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getPostfach() {
		return this.postfach;
	}

	public void setPostfach(String postfach) {
		this.postfach = postfach;
	}

	@Override
	public String toString() {
		return "PostfachComponentModel{" +
				"plz='" + this.plz + '\'' +
				", postfach='" + this.postfach + '\'' +
				'}';
	}
}
