package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoBewilligteAusgabenPrintContent {

	public String title = "Bewilligte Ausgaben";

	public String gesamtinvestitionBewilligungBezeichnung = "Gesamtausgaben Bewilligung (€)";
	public String foerderfaehigBewilligungBezeichnung = "zuwendungsfähige Ausgaben Bewilligung(€)";
	public String foerdersatzBewilligungBezeichnung = "Fördersatz (%)";

	public String gesamtinvestitionBewilligung;
	public String foerderfaehigBewilligung;
	public String foerdersatzBewilligung;

}
