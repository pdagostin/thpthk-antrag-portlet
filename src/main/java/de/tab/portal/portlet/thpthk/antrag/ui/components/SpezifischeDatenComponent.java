package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PopupDateField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.SpezifischeDatenComponentModel;
import de.tab.vaadin.components.BrancheField;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

/**
 * Created by Nikola Sapun on 3/1/2016.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "rechtsform",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.spezifischeDaten.rechtsform"})),
        @PropertyCaption(propertyId = "einordnung",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.spezifischeDaten.einordnungAntragsteller"})),
        @PropertyCaption(propertyId = "gruendungsdatum",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.spezifischeDaten.gruendungsdatum"})),
        @PropertyCaption(propertyId = "branche",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.spezifischeDaten.brancheAntragsteller"})),
        @PropertyCaption(propertyId = "erstattungsfaehigkeitMwst",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.spezifischeDaten.erstattungsfaehigkeit"})

        )})
public class SpezifischeDatenComponent extends FormLayout implements Item.Editor {

    private static final long serialVersionUID = 1373106798055054304L;

    @PropertyId("rechtsform")
    private final ComboBox rechtsform = VaadinUtils.buildBasicComboBox();

    @PropertyId("einordnung")
    private final ComboBox einordnung = VaadinUtils.buildBasicComboBox();

    @PropertyId("gruendungsdatum")
    private final PopupDateField gruendungsdatum = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("branche")
    private final BrancheField branche = new BrancheField();

    @PropertyId("erstattungsfaehigkeitMwst")
    private final OptionGroup erstattungsfaehigkeitMwst = VaadinUtils.buildBasicYesNoOptionGroup();

    public SpezifischeDatenComponent() {
        this.buildLayout();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.rechtsform, this.einordnung, this.gruendungsdatum, this.branche, this.erstattungsfaehigkeitMwst);
    }

    private void initValidators() {
        VaadinUtils.configField(this.rechtsform, SpezifischeDatenComponentModel.class, "rechtsform");
        VaadinUtils.configField(this.einordnung, SpezifischeDatenComponentModel.class, "einordnung");
        VaadinUtils.configField(this.gruendungsdatum, SpezifischeDatenComponentModel.class, "gruendungsdatum");
        VaadinUtils.configField(this.branche, SpezifischeDatenComponentModel.class, "branche");
        VaadinUtils.configField(this.erstattungsfaehigkeitMwst, SpezifischeDatenComponentModel.class, "erstattungsfaehigkeitMwst");
    }

    public void setRechtsformContainer(Container c) {
        this.rechtsform.setContainerDataSource(c);
    }

    public void setEinordnungAntragstellerContainer(Container c) {
        this.einordnung.setContainerDataSource(c);
    }

    public void setBrancheAntragstellerContainer(Container c) {
        this.branche.setContainerDataSource(c);
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }
}
