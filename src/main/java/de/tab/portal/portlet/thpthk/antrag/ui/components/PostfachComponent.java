package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.PostfachComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

/**
 * Created by Nikola Sapun on 3/1/2016.
 */

@ComponentTranslation(propertyCaptions = {
		@PropertyCaption(propertyId = "plz",
				caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.postfach.plz"})),
		@PropertyCaption(propertyId = "postfach",
				caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.postfach.postfach"})
		)})
public class PostfachComponent extends FormLayout implements Item.Editor {

    private static final long serialVersionUID = 731695820183720757L;

    @PropertyId("plz")
	private final TextField plz = VaadinUtils.buildBasicTextField();

	@PropertyId("postfach")
	private final TextField postfach = VaadinUtils.buildBasicTextField();

	public PostfachComponent(){
        this.buildLayout();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.plz, this.postfach);
    }

    private void initValidators() {
        VaadinUtils.configField(this.plz, PostfachComponentModel.class, "plz");
        VaadinUtils.configField(this.postfach, PostfachComponentModel.class, "postfach");
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }
}
