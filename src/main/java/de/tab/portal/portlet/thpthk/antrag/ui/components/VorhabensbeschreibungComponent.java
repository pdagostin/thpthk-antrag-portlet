package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.VorhabensbeschreibungComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

import java.text.Normalizer;

/**
 * Created by paolo.dagostin on 3.5.2016..
 */

@ComponentTranslation(propertyCaptions = {
		@PropertyCaption(propertyId = "hinweistext", caption = @Caption({"thpthk.tab.test.subtest.vorhabensbeschreibung.hinweistext"})),
		@PropertyCaption(propertyId = "beschreibungKurz", caption = @Caption({"thpthk.tab.test.subtest.vorhabensbeschreibung.kurzbeschreibung"})),
		@PropertyCaption(propertyId = "beschreibungLang", caption = @Caption({"thpthk.tab.test.subtest.vorhabensbeschreibung.detaillierteBeschreibung"}))
})
public class VorhabensbeschreibungComponent extends VerticalLayout implements Item.Editor {

	private static final long serialVersionUID = 6551186141690191249L;

	@PropertyId("hinweistext")
	private final Label hinweistext = new Label();

	@PropertyId("beschreibungKurz")
	private final TextField beschreibungKurz = VaadinUtils.buildBasicTextField();

	@PropertyId("beschreibungLang")
	private final TextArea beschreibungLang = VaadinUtils.buildBasicTextArea();

	public VorhabensbeschreibungComponent() {
		this.buildLayout();
		this.initValidators();
	}

	private void buildLayout(){
		this.setMargin(true);
		this.setSpacing(true);

		this.beschreibungKurz.setWidth(110, Sizeable.UNITS_EX);
		this.beschreibungLang.setWidth(100, Sizeable.UNITS_EX);

		final HorizontalLayout horizontalLayout = VaadinUtils.buildBasicHorizontalLayout();
		MarginInfo horizontalMarginInfo = new MarginInfo(true, true, false, true);
		horizontalLayout.setMargin(horizontalMarginInfo);
		this.hinweistext.setCaptionAsHtml(true);
		horizontalLayout.addComponent(this.hinweistext);

		final FormLayout formLayout = VaadinUtils.buildFormLayout();
		MarginInfo formLayoutMarginInfo = new MarginInfo(false, true, false, true);
		formLayout.setMargin(formLayoutMarginInfo);
		formLayout.addComponents(this.beschreibungKurz, this.beschreibungLang);

		this.addComponents(horizontalLayout, formLayout);
	}

	private void initValidators(){
		VaadinUtils.configField(this.beschreibungKurz, VorhabensbeschreibungComponentModel.class, "beschreibungKurz");
		VaadinUtils.configField(this.beschreibungLang, VorhabensbeschreibungComponentModel.class, "beschreibungLang");
	}

	@Override
	public void setItemDataSource(Item item) {

	}

	@Override
	public Item getItemDataSource() {
		return null;
	}
}
