package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import de.tab.portal.commons.KeyText;
import de.tab.portal.portlet.thpthk.antrag.ui.components.AdresseComponent;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author hrvoje.krot
 * @since 25.9.2015.
 * <p>
 * Model for the data storage of {@link AdresseComponent}
 */
public class AdresseComponentModel implements Serializable {
    private static final long serialVersionUID = 4745034788662834970L;

    private String test;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
    @Size(max = 105, message = "{tab.validation.Size.message}")
    private String name;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
    private String plz;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
    @Size(max = 40, message = "{tab.validation.Size.message}")
    private String ort;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
    @Size(max = 35, message = "{tab.validation.Size.message}")
    private String strassenr;

    private KeyText land;

    private String ags;

    private String landkreis;

    private String region;

    public AdresseComponentModel() {
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlz() {
        return this.plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return this.ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getStrassenr() {
        return this.strassenr;
    }

    public void setStrassenr(String strassenr) {
        this.strassenr = strassenr;
    }

    public String getLandkreis() {
        return this.landkreis;
    }

    public void setLandkreis(String landkreis) {
        this.landkreis = landkreis;
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public KeyText getLand() {
        return this.land;
    }

    public void setLand(KeyText land) {
        this.land = land;
    }

    public String getAgs() {
        return this.ags;
    }

    public void setAgs(String ags) {
        this.ags = ags;
    }

    @Override
    public String toString() {
        return "AdresseComponentModel{" +
                "test='" + test + '\'' +
                ", name='" + name + '\'' +
                ", plz='" + plz + '\'' +
                ", ort='" + ort + '\'' +
                ", strassenr='" + strassenr + '\'' +
                ", land=" + land +
                ", ags='" + ags + '\'' +
                ", landkreis='" + landkreis + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
