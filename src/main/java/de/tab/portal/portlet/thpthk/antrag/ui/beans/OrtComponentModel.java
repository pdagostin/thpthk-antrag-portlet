package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import de.tab.portal.commons.DefaultKeyText;
import de.tab.portal.commons.KeyText;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by abeblek on 09.12.2015.
 */
public class OrtComponentModel implements Serializable {

    private static final long serialVersionUID = 639904883400512290L;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
    @Pattern(regexp = "(04|06|07|36|37|96|98|99)\\d{3}", message = "{tab.validation.PLZ.thurigen.message}")
    private String plz;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
    @Size(max = 40, message = "{tab.validation.Size.message}")
    private String ort;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
    @Size(max = 35, message = "{tab.validation.Size.message}")
    private String strassenr;

    @NotNull
    private KeyText land = new DefaultKeyText("DE");

    private String selfHref;

    public String getSelfHref() {
        return this.selfHref;
    }

    public void setSelfHref(String selfHref) {
        this.selfHref = selfHref;
    }

    public String getPlz() {
        return this.plz;
    }

    public void setPlz(final String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return this.ort;
    }

    public void setOrt(final String ort) {
        this.ort = ort;
    }

    public String getStrassenr() {
        return this.strassenr;
    }

    public void setStrassenr(final String strassenr) {
        this.strassenr = strassenr;
    }

    public KeyText getLand() {
        return this.land;
    }

    public void setLand(KeyText land) {
        this.land = land;
    }

    @Override
    public String toString() {
        return "OrtComponentModel{" +
                "strassenr='" + this.strassenr + '\'' +
                ", plz='" + this.plz + '\'' +
                ", ort='" + this.ort + '\'' +
                ", land='" + this.land + '\'' +
                ", selfHref='" + this.selfHref + '\'' +
                '}';
    }

}
