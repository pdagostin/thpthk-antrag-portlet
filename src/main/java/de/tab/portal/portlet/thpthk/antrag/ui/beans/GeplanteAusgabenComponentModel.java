package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Nikola Sapun on 3/3/2016.
 */
public class GeplanteAusgabenComponentModel  implements Serializable {

    private static final long serialVersionUID = -9015886449086724577L;

    @NotNull
    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
	@DecimalMin(value = "0", message = "{tab.validation.DecimalMin.message}")
	private BigDecimal gesamtausgabenPlan;

	public BigDecimal getGesamtausgabenPlan() {
		return this.gesamtausgabenPlan;
	}

	public void setGesamtausgabenPlan(BigDecimal gesamtausgabenPlan) {
		this.gesamtausgabenPlan = gesamtausgabenPlan;
	}

	@Override
	public String toString() {
		return "GeplanteAusgabenComponentModel{" +
				"gesamtausgabenPlan=" + this.gesamtausgabenPlan +
				'}';
	}
}
