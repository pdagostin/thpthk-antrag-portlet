package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import de.tab.portal.commons.KeyText;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Nikola Sapun on 3/1/2016.
 */
public class KontaktComponentModel implements Serializable {

    private static final long serialVersionUID = -4268615726228870011L;

    @NotNull
    @Size(max = 65, message = "{tab.validation.Size.message}")
	@NotBlank(message = "{tab.validation.NotBlank.message}")
	private String name;

    @NotNull
	@Size(max = 20, message = "{tab.validation.Size.message}")
	@NotBlank(message = "{tab.validation.NotBlank.message}")
	@Pattern(regexp = "([0-9+/]*\\)*\\(*\\s*)+", message = "{tab.validation.NumbersOnly.message}")
	private String telefon;

	@Size(max = 20, message = "{tab.validation.Size.message}")
	@Pattern(regexp = "([0-9+/]*\\)*\\(*\\s*)*", message = "{tab.validation.NumbersOnly.message}")
	private String fax;

	@Size(max = 65, message = "{tab.validation.Size.message}")
	@Email(message = "{tab.validation.Email.message}")
	private String email;

    @NotNull
	private KeyText rolle;

	private String selfHref;

	public String getSelfHref() {
		return this.selfHref;
	}

	public void setSelfHref(String selfHref) {
		this.selfHref = selfHref;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public KeyText getRolle() {
		return this.rolle;
	}

	public void setRolle(KeyText rolle) {
		this.rolle = rolle;
	}

	@Override
	public String toString() {
		return "KontaktComponentModel{" +
				"name='" + this.name + '\'' +
				", telefon='" + this.telefon + '\'' +
				", fax='" + this.fax + '\'' +
				", email='" + this.email + '\'' +
				", rolle='" + this.rolle + '\'' +
				'}';
	}
}
