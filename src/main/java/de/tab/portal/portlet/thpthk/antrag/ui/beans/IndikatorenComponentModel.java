package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import de.tab.portal.client.katalog.model.KatalogKeyText;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Model for Indikatoren UI component.
 * Hybrid model consisting of:
 * <pre>
 *     Acutal model for persisting values
 *     Catalog descriptor/model for table
 *     optional - katalog for dropdown if applicable
 * </pre>
 *
 * @author DejVu
 * @since 28.10.2015.
 */
public class IndikatorenComponentModel implements Serializable {

    private static final long serialVersionUID = -6505631539242614341L;

    private String indikator;

    private String sollwert;

    private String istwert;

    private String selfHref;

    private String key;

    private String text;

    private String indikatorTyp;

    private String indikatorKatalogName;

    private Boolean required;

    private List<KatalogKeyText> katalog;

    public IndikatorenComponentModel() {
        this.katalog = new ArrayList<>();
    }

    public String getIndikator() {
        return this.indikator;
    }

    public void setIndikator(String indikator) {
        this.indikator = indikator;
    }

    public String getIstwert() {
        return this.istwert;
    }

    public void setIstwert(String istwert) {
        this.istwert = istwert;
    }

    public String getSollwert() {
        return this.sollwert;
    }

    public void setSollwert(String sollwert) {
        this.sollwert = sollwert;
    }

    public String getSelfHref() {
        return this.selfHref;
    }

    public void setSelfHref(String selfHref) {
        this.selfHref = selfHref;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIndikatorTyp() {
        return this.indikatorTyp;
    }

    public void setIndikatorTyp(String indikatorTyp) {
        this.indikatorTyp = indikatorTyp;
    }

    public String getIndikatorKatalogName() {
        return this.indikatorKatalogName;
    }

    public void setIndikatorKatalogName(String indikatorKatalogName) {
        this.indikatorKatalogName = indikatorKatalogName;
    }

    public Boolean getRequired() {
        return this.required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public List<KatalogKeyText> getKatalog() {
        return this.katalog;
    }

    public void setKatalog(List<KatalogKeyText> katalog) {
        this.katalog = katalog;
    }

    @Override
    public String toString() {
        return "IndikatorenComponentModel{" +
                "indikator='" + this.indikator + '\'' +
                ", sollwert='" + this.sollwert + '\'' +
                ", istwert='" + this.istwert + '\'' +
                ", key='" + this.key + '\'' +
                ", text='" + this.text + '\'' +
                ", indikatorTyp='" + this.indikatorTyp + '\'' +
                ", indikatorKatalogName='" + this.indikatorKatalogName + '\'' +
                ", required=" + this.required +
                ", katalog=" + this.katalog +
                '}';
    }

}
