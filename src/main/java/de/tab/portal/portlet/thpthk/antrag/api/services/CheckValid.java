package de.tab.portal.portlet.thpthk.antrag.api.services;

import de.tab.portal.portlet.thpthk.antrag.ui.persistence.model.ThpthkVorhaben;
import de.tab.portal.portlet.thpthk.antrag.ui.services.VorhabenService;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Date;

@Service
public class CheckValid {

	@Inject
	private VorhabenService vorhabenService;

	public boolean isValid(final Long vorhabenId) {
		boolean ret = false;
		final Resource<ThpthkVorhaben> vorhaben = this.vorhabenService.getVorhaben(vorhabenId);

		if (vorhaben != null && vorhaben.getContent() != null) {
			final Date antragFormularIsValidUntil = vorhaben.getContent().antragFormularIsValidUntil;

			if (antragFormularIsValidUntil != null && new Date().before(antragFormularIsValidUntil)) {
				ret = true;
			}
		}
		return ret;
	}

}
