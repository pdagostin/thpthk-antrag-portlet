package de.tab.portal.portlet.thpthk.antrag.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import de.tab.portal.portlet.thpthk.antrag.ui.views.ThpthkAntragView;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
@Theme("tab-theme")
public class ThpthkAntragPortletUI extends UI {
    private static final long serialVersionUID = 4032483936224967671L;
    private final Logger logger = LoggerFactory.getLogger(ThpthkAntragPortletUI.class);

    @Autowired
    ThpthkAntragView view;

    @Override
    protected void init(final VaadinRequest vaadinRequest) {
        this.view.enter(null);
        this.setContent(this.view);

        UI.getCurrent().setErrorHandler(new DefaultErrorHandler() {
            private static final long serialVersionUID = 3029571198225512018L;

            @Override
            public void error(final com.vaadin.server.ErrorEvent event) {
				ThpthkAntragPortletUI.this.logger.info("", event.getThrowable());
				doDefault(event);
            }
        });
    }

    @Override
    protected void refresh(final VaadinRequest request) {
        super.refresh(request);
        this.view.enter(null);
    }

}
