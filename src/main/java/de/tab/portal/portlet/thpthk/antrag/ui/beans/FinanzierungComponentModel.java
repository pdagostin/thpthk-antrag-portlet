package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Nikola Sapun on 14/3/2016.
 */
public class FinanzierungComponentModel implements Serializable {

    private static final long serialVersionUID = 5306848686457277289L;

    @NotNull
    private String jahr;

    @NotNull
    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal eumittel;

    @NotNull
    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal bundesmittel;

    @NotNull
    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal landesmittel;

    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal zuschuss;

    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal grunderwerb;

    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal kommunalmittel;

    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal privatemittel;

    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal foerderfaehigInvest;

    private String selfHref;

    public String getSelfHref() {
        return this.selfHref;
    }

    public void setSelfHref(String selfHref) {
        this.selfHref = selfHref;
    }

    public String getJahr() {
        return jahr;
    }

    public void setJahr(String jahr) {
        this.jahr = jahr;
    }

    public BigDecimal getEumittel() {
        return eumittel;
    }

    public void setEumittel(BigDecimal eumittel) {
        this.eumittel = eumittel;
    }

    public BigDecimal getBundesmittel() {
        return bundesmittel;
    }

    public void setBundesmittel(BigDecimal bundesmittel) {
        this.bundesmittel = bundesmittel;
    }

    public BigDecimal getLandesmittel() {
        return landesmittel;
    }

    public void setLandesmittel(BigDecimal landesmittel) {
        this.landesmittel = landesmittel;
    }

    public BigDecimal getZuschuss() {
        return zuschuss;
    }

    public void setZuschuss(BigDecimal zuschuss) {
        this.zuschuss = zuschuss;
    }

    public BigDecimal getGrunderwerb() {
        return grunderwerb;
    }

    public void setGrunderwerb(BigDecimal grunderwerb) {
        this.grunderwerb = grunderwerb;
    }

    public BigDecimal getKommunalmittel() {
        return kommunalmittel;
    }

    public void setKommunalmittel(BigDecimal kommunalmittel) {
        this.kommunalmittel = kommunalmittel;
    }

    public BigDecimal getPrivatemittel() {
        return privatemittel;
    }

    public void setPrivatemittel(BigDecimal privatemittel) {
        this.privatemittel = privatemittel;
    }

    public BigDecimal getFoerderfaehigInvest() {
        return foerderfaehigInvest;
    }

    public void setFoerderfaehigInvest(BigDecimal foerderfaehigInvest) {
        this.foerderfaehigInvest = foerderfaehigInvest;
    }

    @Override
    public String toString() {
        return "FinanzierungComponentModel{" +
                "jahr='" + jahr + '\'' +
                ", eumittel=" + eumittel +
                ", bundesmittel=" + bundesmittel +
                ", landesmittel=" + landesmittel +
                ", zuschuss=" + zuschuss +
                ", grunderwerb=" + grunderwerb +
                ", kommunalmittel=" + kommunalmittel +
                ", privatemittel=" + privatemittel +
                ", foerderfaehigInvest=" + foerderfaehigInvest +
                ", selfHref='" + selfHref + '\'' +
                '}';
    }
}
