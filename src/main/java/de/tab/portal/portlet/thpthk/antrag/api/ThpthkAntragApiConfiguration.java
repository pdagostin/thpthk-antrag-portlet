package de.tab.portal.portlet.thpthk.antrag.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.tab.portal.client.katalog.config.KatalogClientConfiguration;
import de.tab.portal.client.persistence.KeyTextModule;
import de.tab.portal.client.persistence.config.PortalPersistenceClientConfiguration;
import de.tab.portal.client.print.config.PrintConfiguration;
import de.tab.portal.client.workflow.config.WcConfiguration;
import de.vollmar.vaadin.spring.i18n.factories.LocaleServiceFactory;
import de.vollmar.vaadin.spring.i18n.services.LocaleService;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;
import de.vollmar.vaadin.spring.i18n.services.TranslationServiceImpl;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Locale;

@Configuration
@EnableWebMvc
@ComponentScan({"de.tab.portal.portlet.thpthk.antrag.api", "de.tab.portal.portlet.thpthk.antrag.ui.services"})
@Import({WcConfiguration.class, PortalPersistenceClientConfiguration.class, PrintConfiguration.class, KatalogClientConfiguration.class})
public class ThpthkAntragApiConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public LocaleServiceFactory localeServiceFactory() {
		return new LocaleServiceFactory();
	}

	@Bean
	public TranslationService translationService() {
		return new TranslationServiceImpl();
	}

	@Bean
	public ObjectMapper objectMapper() {
		final ObjectMapper objectMapper = new ObjectMapper();

		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.registerModule(new KeyTextModule());

		return objectMapper;
	}

	@Bean
	public LocaleService localeService() {
		return new LocaleService() {

			@Override
			public Locale getCurrentLocale() {
				return Locale.GERMAN;
			}
		};
	}

	@Bean
	public MessageSource messageSource() {
		final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasenames("de/tab/portal/portlet/thpthk/antrag/api/messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}
}
