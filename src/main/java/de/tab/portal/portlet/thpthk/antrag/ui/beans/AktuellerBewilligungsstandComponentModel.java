package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ana.beblek
 * @since 14.3.2016.
 */
public class AktuellerBewilligungsstandComponentModel implements Serializable {

    private static final long serialVersionUID = -4576486633856603810L;

    @NotNull
    @DecimalMin(value = "0", message = "{tab.validation.DecimalMin.message}")
    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal gesamtausgaben;

    @NotNull
    @DecimalMin(value = "0", message = "{tab.validation.DecimalMin.message}")
    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal gesamtausgabenFoerderfaehig;

    @DecimalMin(value = "0", message = "{tab.validation.DecimalMin.message}")
    @DecimalMax(value = "100", message = "{tab.validation.DecimalMax.message}")
    @Digits(integer = 3, fraction = 0, message = "{tab.validation.Digits.message}")
    private BigDecimal foerdersatz;

    public BigDecimal getGesamtausgaben() {
        return this.gesamtausgaben;
    }

    public void setGesamtausgaben(BigDecimal gesamtausgaben) {
        this.gesamtausgaben = gesamtausgaben;
    }

    public BigDecimal getGesamtausgabenFoerderfaehig() {
        return this.gesamtausgabenFoerderfaehig;
    }

    public void setGesamtausgabenFoerderfaehig(BigDecimal gesamtausgabenFoerderfaehig) {
        this.gesamtausgabenFoerderfaehig = gesamtausgabenFoerderfaehig;
    }

    public BigDecimal getFoerdersatz() {
        return this.foerdersatz;
    }

    public void setFoerdersatz(BigDecimal foerdersatz) {
        this.foerdersatz = foerdersatz;
    }

    @Override
    public String toString() {
        return "AktuellerBewilligungsstandComponentModel{" +
                "gesamtausgaben=" + gesamtausgaben +
                ", gesamtausgabenFoerderfaehig=" + gesamtausgabenFoerderfaehig +
                ", foerdersatz=" + foerdersatz +
                '}';
    }
}
