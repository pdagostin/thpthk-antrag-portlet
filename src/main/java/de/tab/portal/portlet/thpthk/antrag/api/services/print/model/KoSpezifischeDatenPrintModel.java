package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoSpezifischeDatenPrintModel {

	public String title = "Spezifische Daten";
	public String rechtsformBezeichnung = "Rechtsform";
	public String rechtsform;
	public String einordnungVertragspartnerBezeichnung = "Einordnung des Vertragspartners";
	public String einordnungVertragspartner;
	public String gruendungsdatumBezeichnung = "Gründungsdatum";
	public String gruendungsdatum;
	public String brancheBezeichnung = "Branche des Vertragspartners";
	public String branche;
	public String erstattungsfaehigkeitMwstBezeichnung = "Erstattungsfähigkeit Mwst";
	public String erstattungsfaehigkeitMwst;

}
