package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.FormLayout;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.BrancheComponentModel;
import de.tab.vaadin.components.BrancheField;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

/**
 * Created by ana.beblek on 1.3.2016..
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "branche", caption = @Caption({"thpthk.antrag.vorhaben.grunddaten.branche.branche"}))})
public class BrancheComponent extends FormLayout implements Item.Editor {
    private static final long serialVersionUID = 925691777987506555L;

    @PropertyId("branche")
    private final BrancheField branche = new BrancheField();

    public BrancheComponent() {
        this.buildLayout();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.branche);
    }

    private void initValidators() {
        VaadinUtils.configField(this.branche, BrancheComponentModel.class, "branche");
    }

    public void setBrancheContainer(Container c) {
        this.branche.setContainerDataSource(c);
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }
}
