package de.tab.portal.portlet.thpthk.antrag.ui.services;

import de.tab.portal.client.katalog.KatalogClientService;
import de.tab.portal.client.katalog.model.KatalogKeyText;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by vollmar on 08.01.2016.
 */
@Service
public class AsyncKatalogServiceImpl implements AsyncKatalogService {

	@Inject
	private KatalogClientService katalogClientService;

	@Override
	@Async(value = "taskExecutor")
	public Future<List<KatalogKeyText>> getKatalogKeyText(String name){

		final List<KatalogKeyText> katalogKeyText = this.katalogClientService.getKatalogKeyText(name);

		return new AsyncResult<>(katalogKeyText);
	}

}
