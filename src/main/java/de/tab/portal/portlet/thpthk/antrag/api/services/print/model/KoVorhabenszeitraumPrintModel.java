package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoVorhabenszeitraumPrintModel {

	public String title = "Vorhabenszeitraum";
	public String beginnBezeichnung = "Beginn";
	public String beginn;
	public String endeBezeichnung = "Ende";
	public String ende;

}
