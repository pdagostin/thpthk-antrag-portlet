package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.PopupDateField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.VorhabenszeitraumComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author abeblek
 * @since 01-Mar-16.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "beginn", caption = @Caption({"thpthk.antrag.vorhaben.grunddaten.vorhabenszeitraum.beginn"})),
        @PropertyCaption(propertyId = "ende", caption = @Caption({"thpthk.antrag.vorhaben.grunddaten.vorhabenszeitraum.ende"}))})
public class VorhabenszeitraumComponent extends FormLayout implements Item.Editor, ValueChangeListener {

    private static final long serialVersionUID = -2892066922929175866L;

    @PropertyId("beginn")
    private final PopupDateField beginn = VaadinUtils.buildBasicPopupDateField();

    @PropertyId("ende")
    private final PopupDateField ende = VaadinUtils.buildBasicPopupDateField();

    public VorhabenszeitraumComponent() {
        this.buildLayout();
        this.initListeners();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.beginn, this.ende);
    }

    private void initValidators() {
        VaadinUtils.configField(this.beginn, VorhabenszeitraumComponentModel.class, "beginn");
        VaadinUtils.configField(this.ende, VorhabenszeitraumComponentModel.class, "ende");
    }

    @Override
    public void valueChange(final ValueChangeEvent event) {
        final Date beginn = (Date) this.beginn.getConvertedValue();
        if (beginn != null) {
            Calendar startRangeDate = new GregorianCalendar();
            startRangeDate.setTime(beginn);
            this.ende.setRangeStart(startRangeDate.getTime());
        }
    }

    public void initListeners() {
        final Calendar startRangeDate = new GregorianCalendar(2014, 0, 1);
        this.beginn.setRangeStart(startRangeDate.getTime());
        this.ende.setRangeStart(startRangeDate.getTime());
        this.beginn.addValueChangeListener(this);
        this.ende.addValueChangeListener(this);
    }

    public PopupDateField getBeginn() {
        return this.beginn;
    }

    public PopupDateField getEnde() {
        return this.ende;
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }
}
