package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.VertretungsberechtigerComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

/**
 * Created by paolo.dagostin on 3.5.2016..
 */
@ComponentTranslation(propertyCaptions = {
		@PropertyCaption(propertyId = "name", caption = @Caption({"thpthk.tab.test.subtest.vertretungsberechtiger.name"})),
		@PropertyCaption(propertyId = "rolle", caption = @Caption({"thpthk.tab.test.subtest.vertretungsberechtiger.rolle"})
		)})
public class VertretungsberechtigerComponent extends AbstractCustomComponent<VertretungsberechtigerComponentModel>{

	private static final long serialVersionUID = -6782083703549552220L;

	@PropertyId("name")
	private final TextField name = VaadinUtils.buildBasicTextField();

	@PropertyId("rolle")
	private final TextField rolle = VaadinUtils.buildBasicTextField();

	public VertretungsberechtigerComponent(){
		this.componentModelClass = VertretungsberechtigerComponentModel.class;
	}

	@Override
	public void init() {
		this.setMargin(true);
		this.setSpacing(true);

		final FormLayout layout = VaadinUtils.buildFormLayout();
		layout.addComponents(this.name, this.rolle);
		this.addComponent(layout);
	}

	public void initializeValidators() {
		VaadinUtils.configField(this.name, VertretungsberechtigerComponentModel.class, "name");
		VaadinUtils.configField(this.rolle, VertretungsberechtigerComponentModel.class, "rolle");
	}

	public void setRolleContainer(Container c) {
	}
}
