package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.VerticalLayout;
import de.tab.vaadin.components.EditItemDialogContent;

import java.util.ArrayList;

/**
 * Created by Nikola Sapun on 2/3/2016.
 */
public abstract class AbstractCustomComponent<K> extends VerticalLayout implements EditItemDialogContent {
    private static final long serialVersionUID = -5921257189556029788L;
    private BeanFieldGroup<K> formFieldBindings;
    private K bean;
    private boolean buffered = true;
    protected Class<?> componentModelClass;
    protected Item itemDataSource;

    public void commit() throws FieldGroup.CommitException {
        this.formFieldBindings.commit();
    }

    public Class getComponentModelClass() {
        return this.componentModelClass;
    }

    public void setFieldGroupBuffered(boolean buffered) {
        this.buffered = buffered;
    }

    public void initializeValidators() {
    }

    public Component getContent() {
        return this;
    }

    public void resetData() {
        BeanItem beanItem = new BeanItem(this.createComponentModel(), this.componentModelClass);
        this.setItemDataSource(beanItem);
    }

    private Object createComponentModel() {
        try {
            return this.componentModelClass.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void setItemDataSource(Item item) {
        this.itemDataSource = item;
        this.bean = ((BeanItem<K>) item).getBean();
        if (bean != null) {
            if (this.formFieldBindings != null) {
                for (final Field<?> field : new ArrayList<>(this.formFieldBindings.getFields())) {
                    this.formFieldBindings.unbind(field);
                }
            }
            if (this.buffered) {
                this.formFieldBindings = BeanFieldGroup.bindFieldsBuffered(bean, this);
            } else {
                this.formFieldBindings = BeanFieldGroup.bindFieldsUnbuffered(bean, this);
            }
        }
		this.disableFields();
    }

    public Item getItemDataSource() {
        return this.itemDataSource;
    }

    protected void disableFields() {
    }
}
