package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Nikola Sapun on 3/2/2016.
 */
public class AufbewahrungsortDerBelegeComponentModel implements Serializable {

    private static final long serialVersionUID = -7988035478591316151L;

    @NotNull
    @NotBlank(message = "{tab.validation.NotBlank.message}")
    @Size(max = 255, message = "{tab.validation.Size.message}")
	private String belegeAufbewahrungsort;

	public String getBelegeAufbewahrungsort() {
		return this.belegeAufbewahrungsort;
	}

	public void setBelegeAufbewahrungsort(String belegeAufbewahrungsort) {
		this.belegeAufbewahrungsort = belegeAufbewahrungsort;
	}

	@Override
	public String toString() {
		return "AufbewahrungsortDerBelegeComponentModel{" +
				"belegeAufbewahrungsort='" + this.belegeAufbewahrungsort + '\'' +
				'}';
	}
}
