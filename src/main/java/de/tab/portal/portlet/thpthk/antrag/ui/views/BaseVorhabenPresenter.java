package de.tab.portal.portlet.thpthk.antrag.ui.views;

/**
 * @author hrvoje.krot on 27.4.2016.
 */
import de.tab.vaadin.beans.BottomNavigationBarModel;
import de.tab.vaadin.layout.BasePresenter;
import de.tab.vaadin.layout.VorhabenPresenterParameters;
import de.tab.vaadin.ui.HasVorhabenIdAndAufgabeId;
import de.tab.vaadin.ui.TabPruefungPageStateListener;
import de.tab.vaadin.ui.VorhabenSaveable;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public abstract class BaseVorhabenPresenter extends BasePresenter implements HasVorhabenIdAndAufgabeId, VorhabenSaveable, TabPruefungPageStateListener {
	private final VorhabenPresenterParameters vorhabenPresenterParameters;
	private final BottomNavigationBarModel bottonNavigationBarModel = new BottomNavigationBarModel();

	protected BaseVorhabenPresenter(VorhabenPresenterParameters vorhabenPresenterParameters) {
		this.vorhabenPresenterParameters = vorhabenPresenterParameters;
	}

	private void initializeParamters() {
	}

	public void reset() {
		this.initializeParamters();
	}

	public Long getVorhabenId() {
		return this.bottonNavigationBarModel.vorhabenId;
	}

	public String getAufgabeId() {
		return this.bottonNavigationBarModel.aufgabeId;
	}

	public void saveVorhaben() {
		Assert.notNull(this.bottonNavigationBarModel.vorhabenId, "VorhabenId == null");
	}
}

