package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoAusgabenAufteilungPrintModel {
	public String title = "Aufteilung der Ausgaben auf Jahre";

	public String jahrBezeichnung = "Jahr";
	public String gesamtInvAebBezeichnung = "Gesamtausgaben (€)";
	public String foerderfaehigAebBezeichnung = "Zuwendungsfähige Gesamtausgaben (€)";

	public List<KoAusgabenAufteilungPrintModelData> data = new ArrayList<KoAusgabenAufteilungPrintModelData>();
}
