package de.tab.portal.portlet.thpthk.antrag.ui.converter;

import de.tab.portal.client.adressen.model.AgsBundeslandLandkreis;
import de.tab.portal.commons.converter.TypeConverter;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AdresseComponentModel;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by Senf on 30.09.2015.
 */
@TypeConverter
public class AgsBundeslandLandkreisToAdresseComponentModel implements Converter<AgsBundeslandLandkreis, AdresseComponentModel> {
	@Override
	public AdresseComponentModel convert(final AgsBundeslandLandkreis source) {
		final AdresseComponentModel m = new AdresseComponentModel();
		m.setAgs(source.ags);
		m.setLandkreis(source.landkreis);
		m.setRegion(source.bundesland);
		m.setPlz(source.plz);
		m.setOrt(source.ort);
		return m;
	}
}
