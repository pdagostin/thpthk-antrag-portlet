package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoAktuellerBewilligungsstandPrintContent {

	public String title = "aktueller Bewilligungsstand";

	public String foerdersatzAebBezeichnung = "Fördersatz (%)";
	public String foerderfaehigAebBezeichnung = "zuwendungsfähige Ausgaben Bewilligung / Änderung (€)";
	public String gesamtinvestitionAebBezeichnung = "Gesamtausgaben Bewilligung / Änderung (€)";

	public String foerdersatzAeb;
	public String foerderfaehigAeb;
	public String gesamtinvestitionAeb;

}
