package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoVorhabensortePrintModel {
	public String title = "Ort des Vorhabens";

	public String strassenrBezeichnung = "Straße / Nr.";
	public String plzBezeichnung = "PLZ";
	public String ortBezeichnung = "Ort";
	public String landBezeichnung = "Land";

	public List<KoVorhabensortePrintModelData> data = new ArrayList<KoVorhabensortePrintModelData>();
}
