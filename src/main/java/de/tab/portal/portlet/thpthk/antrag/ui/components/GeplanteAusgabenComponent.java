package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.GeplanteAusgabenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.util.ComponentUtil;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

/**
 * Created by Nikola Sapun on 3/3/2016.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "gesamtausgabenPlan", caption = @Caption({"thpthk.antrag.vorhaben.ausgaben.geplanteausgaben.gesamtausgabenPlan"}))})
public class GeplanteAusgabenComponent extends FormLayout implements Item.Editor {

    private static final long serialVersionUID = 7106437306214288234L;

    @PropertyId("gesamtausgabenPlan")
    private final TextField gesamtausgabenPlan = VaadinUtils.buildBasicTextField();

    private TranslationService translationService;

    public GeplanteAusgabenComponent() {
        this.buildLayout();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.gesamtausgabenPlan);
    }

    private void initValidators() {
        VaadinUtils.configField(this.gesamtausgabenPlan, GeplanteAusgabenComponentModel.class, "gesamtausgabenPlan");
    }

    public void initConverters() {
        ComponentUtil.setCurrencyConverter(this.gesamtausgabenPlan);
        ComponentUtil.setConversionErrorMsg(translationService.translate("tab.validation.Number.message"), this.gesamtausgabenPlan);
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }

    public void setTranslationService(TranslationService translationService) {
        this.translationService = translationService;
    }
}
