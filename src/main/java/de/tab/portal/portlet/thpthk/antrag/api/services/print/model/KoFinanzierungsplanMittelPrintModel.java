package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoFinanzierungsplanMittelPrintModel {
	public String title = "Finanzierungsplan";

	public String jahrBezeichnung = "Jahr";
	public String eumittelBezeichnung = "EU - Mittel (€)";
	public String kommunalmittelBezeichnung = "Kommunale Mittel (€)";
	public String bundesmittelBezeichnung = "Bundesmittel (€)";
	public String privatemittelBezeichnung = "Private Mittel (€)";
	public String landesmittelBezeichnung = "Landesmittel (€)";
	public String grunderwerbBezeichnung = "Grunderwerb (€)";
	public String foerderfaehigInvestBezeichnung = "förderfähige Ausgaben (€)";
	public String zuschussBezeichnung = "";

	public List<KoFinanzierungsplanMittelPrintModelData> data = new ArrayList<KoFinanzierungsplanMittelPrintModelData>();
}
