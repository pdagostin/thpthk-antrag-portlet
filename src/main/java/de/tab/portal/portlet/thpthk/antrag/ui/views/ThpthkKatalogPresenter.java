package de.tab.portal.portlet.thpthk.antrag.ui.views;

import com.vaadin.data.util.BeanItemContainer;
import de.tab.portal.commons.KeyText;

/**
 * Created by Senf on 29.02.2016.
 */
public interface ThpthkKatalogPresenter {
	BeanItemContainer<KeyText> getLandKatalog();

	BeanItemContainer<KeyText> getBeguenstigtenEinordnungKatalog();

	BeanItemContainer<KeyText> getBrancheKatalog();

	BeanItemContainer<KeyText> getRechtsformKatalog();

	BeanItemContainer<KeyText> getRollenAnsprechpartnerKatalog();

    BeanItemContainer<KeyText> getFoerderperiodeKatalog();
}
