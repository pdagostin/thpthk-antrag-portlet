package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoGesamtausgabenPrintModel {
	public String title = "Gesamtausgaben";

	public String gesamtausgabenBezeichnung = "Gesamtausgaben (€)";
	public String gesamtausgaben;
}
