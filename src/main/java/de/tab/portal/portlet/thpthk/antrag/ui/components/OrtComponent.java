package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.OrtComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

/**
 * @author abeblek
 * @since 09-Dec-15.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "plz", caption = @Caption({"thpthk.antrag.vorhaben.grunddaten.ort.plz"})),
        @PropertyCaption(propertyId = "ort", caption = @Caption({"thpthk.antrag.vorhaben.grunddaten.ort.ort"})),
        @PropertyCaption(propertyId = "land", caption = @Caption({"thpthk.antrag.vorhaben.grunddaten.ort.land"})),
        @PropertyCaption(propertyId = "strassenr", caption = @Caption({"thpthk.antrag.vorhaben.grunddaten.ort.strasse"}))})
public class OrtComponent extends AbstractCustomComponent<OrtComponentModel> {

    private static final long serialVersionUID = -1415710341373899471L;

    @PropertyId("plz")
    private final TextField plz = VaadinUtils.buildBasicTextField();

    @PropertyId("ort")
    private final TextField ort = VaadinUtils.buildBasicTextField();

    @PropertyId("strassenr")
    private final TextField strassenr = VaadinUtils.buildBasicTextField();

    @PropertyId("land")
    private final ComboBox land = VaadinUtils.buildBasicComboBox();

    public OrtComponent() {
        this.componentModelClass = OrtComponentModel.class;
    }

    @Override
    public void init() {
        this.setMargin(true);
        this.setSpacing(true);

        final FormLayout layout = VaadinUtils.buildFormLayout();
        layout.addComponents(this.strassenr, this.plz, this.ort, this.land);
        this.addComponent(layout);
    }

    public void initializeValidators() {
        VaadinUtils.configField(this.strassenr, OrtComponentModel.class, "strassenr");
        VaadinUtils.configField(this.plz, OrtComponentModel.class, "plz");
        VaadinUtils.configField(this.ort, OrtComponentModel.class, "ort");
		VaadinUtils.configField(this.land, OrtComponentModel.class, "land");
    }

    public void setLandContainer(Container c) {
        this.land.setContainerDataSource(c);
    }

    protected void disableFields() {
        this.land.setEnabled(false);
    }
}
