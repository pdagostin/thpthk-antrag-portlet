package de.tab.portal.portlet.thpthk.antrag.ui.views;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.KontaktComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.OrtComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.VertretungsberechtigerComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.components.*;
import de.tab.vaadin.components.BottomNavigationBar;
import de.tab.vaadin.components.ConfirmEvents;
import de.tab.vaadin.layout.TABMainNavigationSheet;
import de.tab.vaadin.layout.TABRootNavigationSheet;
import de.tab.vaadin.security.PortletPermissionSwitcher;
import de.tab.vaadin.security.TabSecurityPermission;
import de.tab.vaadin.util.converter.KeyTextToStringConverter;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;
import de.vollmar.vaadin.spring.i18n.annotations.ScanI18n;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.GregorianCalendar;
import java.util.Locale;

import static org.slf4j.LoggerFactory.getLogger;

@ScanI18n
@SpringComponent
@UIScope
public class ThpthkAntragView extends VerticalLayout implements View {

    private final Logger logger = getLogger(ThpthkAntragView.class);

    private static final long serialVersionUID = -7857696789825269053L;

    /**
     * Injections
     */
    @Autowired
    private ThpthkAntragPresenter presenter;
    @Autowired
    private ThpthkKatalogPresenter katalogPresenter;
    @Autowired
    private TranslationService translationService;

    /**
     * Components
     */
    private final AdresseComponent adresseComponent = new AdresseComponent();
    private final PostfachComponent postfachComponent = new PostfachComponent();
    private final SpezifischeDatenComponent spezifischeDatenComponent = new SpezifischeDatenComponent();
    private final IndikatorenComponent indikatorTable = new IndikatorenComponent();
    private final VorhabenszeitraumComponent vorhabenszeitraumComponent = new VorhabenszeitraumComponent();
    private final BrancheComponent brancheComponent = new BrancheComponent();
    private final AufbewahrungsortDerBelegeComponent aufbewahrungsortDerBelegeComponent = new AufbewahrungsortDerBelegeComponent();
    private final GeplanteAusgabenComponent geplanteAusgabenComponent = new GeplanteAusgabenComponent();

    private final VorhabensbeschreibungComponent vorhabensbeschreibungComponent = new VorhabensbeschreibungComponent();

    /**
     * Tables and their components
     */
    private final OrtComponent ortDesVorhabens = new OrtComponent();
    private final KontaktComponent kontaktComponent = new KontaktComponent();
    private final VertretungsberechtigerComponent vertretungsberechtigerComponent = new VertretungsberechtigerComponent();

    private final AufteilungDerAusgabenAufJahreStellenComponent aufteilungDerAusgabenAufJahreStellenComponent = new AufteilungDerAusgabenAufJahreStellenComponent();

    @ComponentTranslation(propertyCaptions = {
            @PropertyCaption(propertyId = "confirmWindow",
                    caption = @Caption({"thpthk.antrag.vorhaben.grunddaten.ortdesvorhabens"})
            )})
    private CustomEditTableComponent<OrtComponentModel> ortDesVorhabensTable = new CustomEditTableComponent<>(ortDesVorhabens);

    @ComponentTranslation(propertyCaptions = {
            @PropertyCaption(propertyId = "confirmWindow",
                    caption = @Caption({"thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre"})
            )})
    private CustomEditTableComponent<AufteilungDerAusgabenAufJahreStellenComponent> aufteilungDerAusgabenAufJahreStellenTable =
            new CustomEditTableComponent<>(aufteilungDerAusgabenAufJahreStellenComponent);

    @ComponentTranslation(propertyCaptions = {
            @PropertyCaption(propertyId = "confirmWindow",
                    caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.kontakt"})
            )})
    private final CustomEditTableComponent<KontaktComponentModel> kontaktTable = new CustomEditTableComponent<>(kontaktComponent);

    @ComponentTranslation(propertyCaptions = {
            @PropertyCaption(propertyId = "confirmWindow",
                    caption = @Caption({"thpthk.tab.test.subtest.vertretungsberechtiger.input"})
            )})
    private final CustomEditTableComponent<VertretungsberechtigerComponentModel> vertretungsberechtigerTable = new CustomEditTableComponent<>(vertretungsberechtigerComponent);

    /**
     * Antrag Prüfen components
     */
    private final StammdatenComponent stammdatenComponent = new StammdatenComponent();
    private final BewilligteAusgabenComponent bewilligteAusgabenComponent = new BewilligteAusgabenComponent();
    private final AktuellerBewilligungsstandComponent aktuellerBewilligungsstandComponent = new AktuellerBewilligungsstandComponent();
    private final AnsprechpartnerMeldestelleComponent ansprechpartnerMeldestelleComponent = new AnsprechpartnerMeldestelleComponent();

    /**
     * Antrag Prüfen tables and their components
     */
    private final AufteilungDerAusgabenAufJahrePruefenComponent aufteilungDerAusgabenAufJahrePruefenComponent = new AufteilungDerAusgabenAufJahrePruefenComponent();

    private final FinanzierungComponent finanzierungComponent = new FinanzierungComponent();

    @ComponentTranslation(propertyCaptions = {
            @PropertyCaption(propertyId = "confirmWindow",
                    caption = @Caption({"thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre"})
            )})
    private final CustomEditTableComponent<AufteilungDerAusgabenAufJahrePruefenComponent> aufteilungDerAusgabenAufJahrePruefenTable =
            new CustomEditTableComponent<>(aufteilungDerAusgabenAufJahrePruefenComponent);


    @ComponentTranslation(propertyCaptions = {
            @PropertyCaption(propertyId = "confirmWindow",
                    caption = @Caption({"thpthk.antrag.stammdaten.finanzierung"})
            )})
    private final CustomEditTableComponent<FinanzierungComponent> finanzierungTable = new CustomEditTableComponent<>(finanzierungComponent);

    /**
     * Others
     */
    private final TABRootNavigationSheet rootSheet = new TABRootNavigationSheet();
    private final PortletPermissionSwitcher pps = new PortletPermissionSwitcher(this);

    private final KeyTextToStringConverter landToStringConverter = new KeyTextToStringConverter();
    private final KeyTextToStringConverter rolleToStringConverter = new KeyTextToStringConverter();
    private final Label currentViewNameLabel = new Label();

    /**
     * Antrag Prüfen tabs
     */
    private TabSheet.Tab stammdatenTab;
    private TabSheet.Tab bearbeiterTab;
    private TabSheet.Tab testTab;

    @PostConstruct
    private void init() {
        this.presenter.init();
        this.setSpacing(true);

        this.addComponent(this.currentViewNameLabel);

        this.aktuellerBewilligungsstandComponent.setTranslationService(this.translationService);
        this.aufteilungDerAusgabenAufJahrePruefenComponent.setTranslationService(this.translationService);
        this.aufteilungDerAusgabenAufJahreStellenComponent.setTranslationService(this.translationService);
        this.bewilligteAusgabenComponent.setTranslationService(this.translationService);
        this.finanzierungComponent.setTranslationService(this.translationService);
        this.geplanteAusgabenComponent.setTranslationService(this.translationService);
        this.indikatorTable.setTranslationService(this.translationService);
        this.stammdatenComponent.setTranslationService(this.translationService);

        this.rootSheet.addTab(createTabAntragsteller(), this.translationService.translate("thpthk.tab.antragsteller"));
        this.rootSheet.addTab(createTabVorhaben(), this.translationService.translate("thpthk.tab.vorhaben"));

        this.stammdatenTab = this.rootSheet.addTab(createTabStammdaten(), this.translationService.translate("thpthk.tab.stammdaten"));
        this.bearbeiterTab = this.rootSheet.addTab(createTabBearbeiter(), this.translationService.translate("thpthk.tab.bearbeiter"));

        this.testTab = this.rootSheet.addTab(createTabTest(), this.translationService.translate("thpthk.tab.test"));

        this.rootSheet.addTabChangeSaveHandler(this.presenter);
//        this.rootSheet.setVisiblePruefungPage(true, this.presenter);

        addComponent(this.rootSheet);

        final BottomNavigationBar bottomNavigationBar = new BottomNavigationBar(this.rootSheet, this.presenter);
        this.addComponent(bottomNavigationBar);

        this.adresseComponent.setAdressenValidator(this.presenter.getAdressenValidator());

        this.initializeComponents(this.rootSheet);

        this.setEveryFieldWithContainer();
        this.initValidatorsAndListeners();
        this.initConverters();
    }

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        this.pps.reset();

        final TabSecurityPermission tabSecurityPermission = this.presenter.checkAccess();
        if (TabSecurityPermission.Permission.ALLOWED.equals(tabSecurityPermission.getPermission())) {
            this.presenter.reset();
            onEnter();
        } else {
            this.pps.setAccessDeniedMessage(tabSecurityPermission.getMessage());
            this.pps.accessDenied();
        }
    }

    private Component createTabAntragsteller() {
        final TABMainNavigationSheet ts = new TABMainNavigationSheet();

        ts.addTab(createSubTabAntragstellerGrunddaten(), this.translationService.translate("thpthk.tab.antragsteller.grunddaten"));
        ts.addTab(createSubTabAntragstellerSpezifischeDaten(), this.translationService.translate("thpthk.tab.antragsteller.spezifischedaten"));

        return ts;
    }

    private Component createTabVorhaben() {
        final TABMainNavigationSheet ts = new TABMainNavigationSheet();

        ts.addTab(createSubTabVorhabenGrunddaten(), this.translationService.translate("thpthk.tab.vorhaben.grunddaten"));
        ts.addTab(createSubTabVorhabenInhalt(), this.translationService.translate("thpthk.tab.vorhaben.inhalt"));
        ts.addTab(createSubTabVorhabenAusgaben(), this.translationService.translate("thpthk.tab.vorhaben.ausgaben"));
        ts.addTab(createSubTabVorhabenIndikatoren(), this.translationService.translate("thpthk.tab.vorhaben.indikatoren"));

        return ts;
    }

    private Component createTabStammdaten() {
        final TABMainNavigationSheet ts = new TABMainNavigationSheet();

        ts.addTab(createSubTabStammdatenGrunddaten(), this.translationService.translate("thpthk.tab.stammdaten.grunddaten"));
        ts.addTab(createSubTabStammdatenAusgaben(), this.translationService.translate("thpthk.tab.stammdaten.ausgaben"));
        ts.addTab(createSubTabStammdatenFinanzierung(), this.translationService.translate("thpthk.tab.stammdaten.finanzierung"));

        return ts;
    }

    private Component createSubTabStammdatenGrunddaten() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();
        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.pruefen.stammdaten.grunddaten.stammdaten"),
                stammdatenComponent));
        return layout;
    }

    private Component createSubTabStammdatenAusgaben() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.stammdaten.ausgaben.bewilligteAusgaben"), this.bewilligteAusgabenComponent));
        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.stammdaten.ausgaben.aktuellerBewilligungsstand"), this.aktuellerBewilligungsstandComponent));

        this.aufteilungDerAusgabenAufJahrePruefenTable.getTable().setCaption(this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre"));
        this.aufteilungDerAusgabenAufJahrePruefenTable.setTablePageLength(3);
        this.aufteilungDerAusgabenAufJahrePruefenTable.setColumnHeader("jahr",
                this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.jahr"));
        this.aufteilungDerAusgabenAufJahrePruefenTable.setColumnHeader("gesamtausgaben",
                this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.gesamtausgaben"));
        this.aufteilungDerAusgabenAufJahrePruefenTable.setColumnHeader("zuwendungsausgaben",
                this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.zuwendungsausgaben"));
        this.aufteilungDerAusgabenAufJahrePruefenTable.setVisibleColumns("jahr", "gesamtausgaben", "zuwendungsausgaben");
        this.aufteilungDerAusgabenAufJahrePruefenTable.setHeight(315, Unit.PIXELS);

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre"),
                this.aufteilungDerAusgabenAufJahrePruefenTable));

        return layout;
    }

    private Component createSubTabStammdatenFinanzierung() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();
        this.finanzierungTable.setTablePageLength(5);

        this.finanzierungTable.getTable().setCaption(this.translationService.translate("thpthk.antrag.stammdaten.finanzierung"));

        this.finanzierungTable.setColumnHeader("jahr", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.jahr"));
        this.finanzierungTable.setColumnHeader("eumittel", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.eumittel"));
        this.finanzierungTable.setColumnHeader("bundesmittel", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.bundesmittel"));
        this.finanzierungTable.setColumnHeader("landesmittel", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.landesmittel"));
		this.finanzierungTable.setColumnHeader("zuschuss", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.zuschuss"));
        this.finanzierungTable.setColumnHeader("grunderwerb", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.grunderwerb"));
        this.finanzierungTable.setColumnHeader("kommunalmittel", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.kommunalmittel"));
        this.finanzierungTable.setColumnHeader("privatemittel", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.privatemittel"));
        this.finanzierungTable.setColumnHeader("foerderfaehigInvest", this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.foerderfaehigInvest"));
        this.finanzierungTable.setVisibleColumns("jahr", "eumittel", "bundesmittel", "landesmittel", "zuschuss", "grunderwerb", "kommunalmittel", "privatemittel", "foerderfaehigInvest");
        this.finanzierungTable.getTable().setFooterVisible(true);
        this.finanzierungTable.setHeight(315, Unit.PIXELS);

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.stammdaten.finanzierung"), this.finanzierungTable));

        return layout;
    }

    private Component createTabBearbeiter() {
        final TABMainNavigationSheet ts = new TABMainNavigationSheet();
        ts.addTab(createSubTabBearbeiterAnsprechpartnerMeldestelle(), this.translationService.translate("thpthk.tab.bearbeiter.ansprechpartnerMeldestelle"));
        return ts;
    }

    private Component createSubTabBearbeiterAnsprechpartnerMeldestelle() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();
        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.bearbeiter.ansprechpartner.meldestelle"), this.ansprechpartnerMeldestelleComponent));
        return layout;
    }

    private Component createTabTest(){
        final TABMainNavigationSheet ts = new TABMainNavigationSheet();
        ts.addTab(createSubTabTest(), this.translationService.translate("thpthk.tab.test.subtest"));
        return ts;
    }

    private Component createSubTabTest(){
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.tab.test.subtest.vorhabensbeschreibung"),
                this.vorhabensbeschreibungComponent));

        this.vertretungsberechtigerTable.setTablePageLength(2);
        this.vertretungsberechtigerTable.getTable().setCaption(translationService.translate("thpthk.tab.test.subtest.vertretungsberechtiger"));
        this.vertretungsberechtigerTable.setColumnHeader("name", this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.kontakt.name"));
        this.vertretungsberechtigerTable.setColumnHeader("rolle", this.translationService.translate("thpthk.tab.test.subtest.vertretungsberechtiger.rolle"));
        this.vertretungsberechtigerTable.setVisibleColumns("name", "rolle");
        this.vertretungsberechtigerTable.setHeight(315, Unit.PIXELS);

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.tab.test.subtest.vertretungsberechtiger"),
                this.vertretungsberechtigerTable));

        return layout;
    }

    private Component createSubTabAntragstellerGrunddaten() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.adresse"),
                this.adresseComponent));
        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.postfach"),
                this.postfachComponent));

        this.kontaktTable.setTablePageLength(5);
        this.kontaktTable.getTable().setCaption(translationService.translate("thpthk.antrag.antragsteller.grunddaten.kontakt"));
        this.kontaktTable.setColumnHeader("name", this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.kontakt.name"));
        this.kontaktTable.setColumnHeader("telefon",this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.kontakt.telefon"));
        this.kontaktTable.setColumnHeader("fax",this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.kontakt.fax"));
        this.kontaktTable.setColumnHeader("email",this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.kontakt.email"));
        this.kontaktTable.setColumnHeader("rolle",this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.kontakt.rolle"));
        this.kontaktTable.setVisibleColumns("name", "telefon", "fax", "email", "rolle");
        this.kontaktTable.setHeight(315, Unit.PIXELS);

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.kontakt"),
                this.kontaktTable));

        return layout;
    }

    private Component createSubTabAntragstellerSpezifischeDaten() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();
        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.antragsteller.grunddaten.spezifischeDaten"),
                this.spezifischeDatenComponent));
        return layout;
    }

    private Component createSubTabVorhabenGrunddaten() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.vorhabenszeitraum"),
                this.vorhabenszeitraumComponent));

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.branche"),
                this.brancheComponent));
        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.ortdesvorhabens"),
                this.ortDesVorhabensTable));

        this.ortDesVorhabensTable.getTable().setCaption(this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.ortdesvorhabens"));
        this.ortDesVorhabensTable.setColumnHeader("strassenr",
                this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.ort.strasse.header"));
        this.ortDesVorhabensTable.setColumnHeader("plz", this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.ort.plz.header"));
        this.ortDesVorhabensTable.setColumnHeader("ort", this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.ort.ort.header"));
        this.ortDesVorhabensTable.setColumnHeader("land", this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.ort.land.header"));
        this.ortDesVorhabensTable.setVisibleColumns("strassenr", "plz", "ort", "land");
        this.ortDesVorhabensTable.setTablePageLength(4);
        this.ortDesVorhabensTable.setHeight(315, Unit.PIXELS);

        return layout;
    }

    private Component createSubTabVorhabenInhalt() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();
        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.vorhaben.inhalt.aufbewahrungsortDerBelegeComponent"), this.aufbewahrungsortDerBelegeComponent));


        return layout;
    }

    private Component createSubTabVorhabenAusgaben() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.geplanteausgaben"), this.geplanteAusgabenComponent));

        this.aufteilungDerAusgabenAufJahreStellenTable.getTable().setCaption(this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre"));
        this.aufteilungDerAusgabenAufJahreStellenTable.setTablePageLength(3);
        this.aufteilungDerAusgabenAufJahreStellenTable.setColumnHeader("jahr",
                this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.jahr"));
        this.aufteilungDerAusgabenAufJahreStellenTable.setColumnHeader("gesamtausgaben",
                this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.gesamtausgaben"));
        this.aufteilungDerAusgabenAufJahreStellenTable.setColumnHeader("zuwendungsausgaben",
                this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.zuwendungsausgaben"));
        this.aufteilungDerAusgabenAufJahreStellenTable.setVisibleColumns("jahr", "gesamtausgaben");
        this.aufteilungDerAusgabenAufJahreStellenTable.setHeight(315, Unit.PIXELS);

        layout.addComponent(VaadinUtils.buildBasicPanel(this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre"),
                this.aufteilungDerAusgabenAufJahreStellenTable));

        return layout;
    }

    private Component createSubTabVorhabenIndikatoren() {
        final VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();

        this.indikatorTable.getTable().setCaption(this.translationService.translate("thpthk.tab.vorhaben.indikatoren"));
        layout.addComponent(VaadinUtils.buildBasicPanel(
                this.translationService.translate("thpthk.tab.vorhaben.indikatoren"),
                this.indikatorTable));

        return layout;
    }

    private void onEnter() {
        this.aufteilungDerAusgabenAufJahreStellenTable.setNewItemsEnabled(this.presenter.getFinanzierungJahrContainer().size() != 0);
        this.aufteilungDerAusgabenAufJahrePruefenTable.setNewItemsEnabled(this.presenter.getFinanzierungJahrContainer().size() != 0);
        this.finanzierungTable.setNewItemsEnabled(this.presenter.getFinanzierungJahrContainer().size() != 0);

        this.stammdatenTab.setVisible(this.isAufgabeAntragPruefen());
        this.bearbeiterTab.setVisible(this.isAufgabeAntragPruefen());
        this.indikatorTable.setShowIstWert(this.isAufgabeAntragPruefen());

        this.stammdatenComponent.getFoerderperiode().setEnabled(false);
//        this.stammdatenComponent.setValidationType(this.presenter.getVorhaben().massnahme.getKey());

        this.ortDesVorhabensTable.getTable().setConverter("land", this.landToStringConverter);
        this.kontaktTable.getTable().setConverter("rolle", this.rolleToStringConverter);

        this.populateJahrComboboxes();

        this.updateFinazierungSummeFields();

        String currentViewName = null;
        switch ("VERKON") {
            case "VERKON":
                currentViewName = translationService.translate("thpthk.tab.verkon");
                break;
            case "EDA":
                currentViewName = translationService.translate("thpthk.tab.eda");
                break;
            case "MONEV":
                currentViewName = translationService.translate("thpthk.tab.monev");
                break;
            case "INKOMM":
                currentViewName = translationService.translate("thpthk.tab.inkomm");
                break;
        }
        this.currentViewNameLabel.setValue(currentViewName);
    }

    private void populateJahrComboboxes() {
        if (this.vorhabenszeitraumComponent.getBeginn().getValue() != null && this.vorhabenszeitraumComponent.getEnde().getValue() != null) {

            final java.util.Calendar beginnCal = new GregorianCalendar();
            beginnCal.setTime(this.vorhabenszeitraumComponent.getBeginn().getValue());

            final java.util.Calendar endeCal = new GregorianCalendar();
            endeCal.setTime(this.vorhabenszeitraumComponent.getEnde().getValue());

            this.presenter.populateJahrComboboxes(beginnCal, endeCal);
        } else {
            this.presenter.getFinanzierungJahrContainer().removeAllItems();
            this.presenter.getFinanzierungJahrContainer().removeAllItems();
        }

        this.aufteilungDerAusgabenAufJahreStellenTable.setNewItemsEnabled(this.presenter.getFinanzierungJahrContainer().size() != 0);
		this.aufteilungDerAusgabenAufJahreStellenComponent.setEdit(false);
        this.aufteilungDerAusgabenAufJahrePruefenComponent.setEdit(false);

        this.finanzierungTable.setNewItemsEnabled(this.presenter.getFinanzierungJahrContainer().size() != 0);
        this.finanzierungComponent.setEdit(false);
    }

    private void initConverters() {
        this.aktuellerBewilligungsstandComponent.initConverters();
        this.aufteilungDerAusgabenAufJahreStellenComponent.initConverters();
        this.aufteilungDerAusgabenAufJahrePruefenComponent.initConverters();
        this.bewilligteAusgabenComponent.initConverters();
        this.geplanteAusgabenComponent.initConverters();
        this.finanzierungComponent.initConverters();

        this.finanzierungTable.getTable().setConverter("eumittel", VaadinUtils.FORMAT_CURRENCY);
        this.finanzierungTable.getTable().setConverter("bundesmittel", VaadinUtils.FORMAT_CURRENCY);
        this.finanzierungTable.getTable().setConverter("landesmittel", VaadinUtils.FORMAT_CURRENCY);
        this.finanzierungTable.getTable().setConverter("grunderwerb", VaadinUtils.FORMAT_CURRENCY);
        this.finanzierungTable.getTable().setConverter("kommunalmittel", VaadinUtils.FORMAT_CURRENCY);
        this.finanzierungTable.getTable().setConverter("privatemittel", VaadinUtils.FORMAT_CURRENCY);
        this.finanzierungTable.getTable().setConverter("foerderfaehigInvest", VaadinUtils.FORMAT_CURRENCY);
        this.finanzierungTable.getTable().setConverter("zuschuss", VaadinUtils.FORMAT_CURRENCY);

        this.aufteilungDerAusgabenAufJahrePruefenTable.getTable().setConverter("gesamtausgaben", VaadinUtils.FORMAT_CURRENCY);
        this.aufteilungDerAusgabenAufJahrePruefenTable.getTable().setConverter("zuwendungsausgaben", VaadinUtils.FORMAT_CURRENCY);

        this.aufteilungDerAusgabenAufJahreStellenTable.getTable().setConverter("gesamtausgaben", VaadinUtils.FORMAT_CURRENCY);
        this.aufteilungDerAusgabenAufJahreStellenTable.getTable().setConverter("zuwendungsausgaben", VaadinUtils.FORMAT_CURRENCY);
    }

    private void initValidatorsAndListeners() {
        this.adresseComponent.setPlzValidator(this.translationService.translate("tab.validation.Number.message"));

        Property.ValueChangeListener listener = new Property.ValueChangeListener() {
            private static final long serialVersionUID = -2925522593747410485L;

            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                ThpthkAntragView.this.populateJahrComboboxes();
            }
        };

        this.vorhabenszeitraumComponent.getBeginn().addValueChangeListener(listener);
        this.vorhabenszeitraumComponent.getEnde().addValueChangeListener(listener);

        this.vorhabenszeitraumComponent.getBeginn().setDateOutOfRangeMessage(translationService.translate("thpthk.antrag.vorhaben.grunddaten.vorhabenszeitraum.invalid2"));
        this.vorhabenszeitraumComponent.getEnde().setDateOutOfRangeMessage(translationService.translate("thpthk.antrag.vorhaben.grunddaten.vorhabenszeitraum.invalid"));

        this.aufteilungDerAusgabenAufJahreStellenTable.getContainerDataSource().addItemSetChangeListener(new Container.ItemSetChangeListener() {
            private static final long serialVersionUID = -2085961681921017077L;

            @Override
            public void containerItemSetChange(Container.ItemSetChangeEvent itemSetChangeEvent) {
                ThpthkAntragView.this.populateJahrComboboxes();
            }
        });

        this.aufteilungDerAusgabenAufJahrePruefenTable.getContainerDataSource().addItemSetChangeListener(new Container.ItemSetChangeListener() {
            private static final long serialVersionUID = -2085961681921017077L;

            @Override
            public void containerItemSetChange(Container.ItemSetChangeEvent itemSetChangeEvent) {
                ThpthkAntragView.this.populateJahrComboboxes();
            }
        });

        ConfirmEvents.ConfirmListener confirmListenerStellen = new ConfirmEvents.ConfirmListener() {
            private static final long serialVersionUID = 646331414926321874L;

            @Override
            public void confirm(ConfirmEvents.ConfirmEvent confirmEvent) {
                if (!confirmEvent.isSuccess()) {
                    ThpthkAntragView.this.populateJahrComboboxes();
                }
            }
        };

        this.aufteilungDerAusgabenAufJahreStellenTable.getConfirmWindow().addConfirmListener(confirmListenerStellen);

        ConfirmEvents.ConfirmListener confirmListenerPruefen = new ConfirmEvents.ConfirmListener() {
            private static final long serialVersionUID = 646331414926321874L;

            @Override
            public void confirm(ConfirmEvents.ConfirmEvent confirmEvent) {
                if (!confirmEvent.isSuccess()) {
                    ThpthkAntragView.this.populateJahrComboboxes();
                }
            }
        };

        this.aufteilungDerAusgabenAufJahrePruefenTable.getConfirmWindow().addConfirmListener(confirmListenerPruefen);

        this.finanzierungTable.getContainerDataSource().addItemSetChangeListener(new Container.ItemSetChangeListener() {
            @Override
            public void containerItemSetChange(Container.ItemSetChangeEvent itemSetChangeEvent) {
                ThpthkAntragView.this.updateFinazierungSummeFields();
                ThpthkAntragView.this.populateJahrComboboxes();
            }
        });

        ConfirmEvents.ConfirmListener confirmListenerFinanzierung = new ConfirmEvents.ConfirmListener() {
            private static final long serialVersionUID = 646331414926321874L;

            @Override
            public void confirm(ConfirmEvents.ConfirmEvent confirmEvent) {
                if (!confirmEvent.isSuccess()) {
                    ThpthkAntragView.this.populateJahrComboboxes();
                }
            }
        };

        this.finanzierungTable.getConfirmWindow().addConfirmListener(confirmListenerFinanzierung);

        this.ortDesVorhabensTable.getTable().addValidator(new Validator() {
            private static final long serialVersionUID = 4728205946460847154L;

            @Override
            public void validate(final Object o) throws InvalidValueException {
                if (ThpthkAntragView.this.ortDesVorhabensTable.getTable().size() == 0) {
                    throw new InvalidValueException(ThpthkAntragView.this.translationService.translate("thpthk.antrag.vorhaben.grunddaten.ort.validationMessage"));
                }
            }
        });

        this.aufteilungDerAusgabenAufJahrePruefenTable.getTable().addValidator(new Validator() {
            @Override
            public void validate(Object o) throws InvalidValueException {
                if (ThpthkAntragView.this.aufteilungDerAusgabenAufJahrePruefenComponent.getJahr().getContainerDataSource().size() != 0) {
                    throw new InvalidValueException(translationService.translate(
                            "thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.error"));
                }
            }
        });

        this.aufteilungDerAusgabenAufJahrePruefenTable.getTable().addValidator(new Validator() {
            private static final long serialVersionUID = 4728205946460847154L;

            @Override
            public void validate(final Object o) throws InvalidValueException {
                if (ThpthkAntragView.this.aufteilungDerAusgabenAufJahrePruefenTable.getTable().size() == 0) {
                    throw new InvalidValueException(ThpthkAntragView.this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.validationMessage"));
                }
            }
        });

        this.finanzierungTable.getTable().addValidator(new Validator() {
            @Override
            public void validate(Object o) throws InvalidValueException {
                if (ThpthkAntragView.this.finanzierungComponent.getJahr().getContainerDataSource().size() != 0) {
                    throw new InvalidValueException(translationService.translate(
                            "thpthk.antrag.stammdaten.finanzierung.error"));
                }
            }
        });

        this.finanzierungTable.getTable().addValidator(new Validator() {
            private static final long serialVersionUID = 4728205946460847154L;

            @Override
            public void validate(final Object o) throws InvalidValueException {
                if (ThpthkAntragView.this.finanzierungTable.getTable().size() == 0) {
                    throw new InvalidValueException(ThpthkAntragView.this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.validationMessage"));
                }
            }
        });

        this.aufteilungDerAusgabenAufJahreStellenTable.getTable().addValidator(new Validator() {
            @Override
            public void validate(Object o) throws InvalidValueException {
                if (ThpthkAntragView.this.aufteilungDerAusgabenAufJahreStellenComponent.getJahr().getContainerDataSource().size() != 0) {
                    throw new InvalidValueException(translationService.translate(
                            "thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.error"));
                }
            }
        });

        this.aufteilungDerAusgabenAufJahreStellenTable.getTable().addValidator(new Validator() {
            private static final long serialVersionUID = 4728205946460847154L;

            @Override
            public void validate(final Object o) throws InvalidValueException {
                if (ThpthkAntragView.this.aufteilungDerAusgabenAufJahreStellenTable.getTable().size() == 0) {
                    throw new InvalidValueException(ThpthkAntragView.this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.validationMessage"));
                }
            }
        });
    }

    private void setEveryFieldWithContainer() {
        //assign catalogs
        this.adresseComponent.setLandContainer(this.katalogPresenter.getLandKatalog());
        this.ortDesVorhabens.setLandContainer(this.katalogPresenter.getLandKatalog());
        this.kontaktComponent.setRolleContainer(this.katalogPresenter.getRollenAnsprechpartnerKatalog());
        this.spezifischeDatenComponent.setRechtsformContainer(this.katalogPresenter.getRechtsformKatalog());
        this.spezifischeDatenComponent.setEinordnungAntragstellerContainer(this.katalogPresenter.getBeguenstigtenEinordnungKatalog());
        this.spezifischeDatenComponent.setBrancheAntragstellerContainer(this.katalogPresenter.getBrancheKatalog());
        this.brancheComponent.setBrancheContainer(this.katalogPresenter.getBrancheKatalog());
        this.stammdatenComponent.setFoerderperiodeContainer(this.katalogPresenter.getFoerderperiodeKatalog());

        //components
        this.adresseComponent.setItemDataSource(this.presenter.getAdresseBeanItem());
        this.vorhabenszeitraumComponent.setItemDataSource(this.presenter.getVorhabenszeitraumBeanItem());
        this.postfachComponent.setItemDataSource(this.presenter.getPostfachBeanItem());
        this.spezifischeDatenComponent.setItemDataSource(this.presenter.getSpezifischeDatenBeanItem());
        this.brancheComponent.setItemDataSource(this.presenter.getBrancheBeanItem());
        this.aufbewahrungsortDerBelegeComponent.setItemDataSource(this.presenter.getAufbewahrungsortBeanItem());
        this.geplanteAusgabenComponent.setItemDataSource(this.presenter.getGeplanteAusgabenBeanItem());
        this.aufteilungDerAusgabenAufJahreStellenComponent.setModelDataSource(this.presenter.getAufteilungDerAusgabenAufJahreContainer());
        this.aufteilungDerAusgabenAufJahrePruefenComponent.setModelDataSource(this.presenter.getAufteilungDerAusgabenAufJahreContainer());
        this.bewilligteAusgabenComponent.setItemDataSource(this.presenter.getBewilligteAusgabenBeanItem());
        this.aktuellerBewilligungsstandComponent.setItemDataSource(this.presenter.getAktuellerBewilligungsstandBeanItem());
        this.stammdatenComponent.setItemDataSource(this.presenter.getStammdatenBeanItem());
        this.ansprechpartnerMeldestelleComponent.setItemDataSource(this.presenter.getAnsprechpartnerMeldestelleBeanItem());
        this.finanzierungComponent.setModelDataSource(this.presenter.getFinanzierungContainer());

        //tables
        this.indikatorTable.setContainerDataSource(this.presenter.getIndikatorContainer());
        this.ortDesVorhabensTable.setContainerDataSource(this.presenter.getOrtDesVorhabensContainer());
        this.aufteilungDerAusgabenAufJahreStellenTable.setContainerDataSource(this.presenter.getAufteilungDerAusgabenAufJahreContainer());
        this.aufteilungDerAusgabenAufJahrePruefenTable.setContainerDataSource(this.presenter.getAufteilungDerAusgabenAufJahreContainer());
        this.finanzierungTable.setContainerDataSource(this.presenter.getFinanzierungContainer());
        this.kontaktTable.setContainerDataSource(this.presenter.getKontaktContainer());

        //comboboxes
        this.finanzierungComponent.getJahr().setContainerDataSource(this.presenter.getFinanzierungJahrContainer());
        this.aufteilungDerAusgabenAufJahrePruefenComponent.getJahr().setContainerDataSource(this.presenter.getAufteilungDerAusgabenAufJahreJahrContainer());
        this.aufteilungDerAusgabenAufJahreStellenComponent.getJahr().setContainerDataSource(this.presenter.getAufteilungDerAusgabenAufJahreJahrContainer());

        //converters
        this.landToStringConverter.setContainerDataSource(this.katalogPresenter.getLandKatalog());
        this.rolleToStringConverter.setContainerDataSource(this.katalogPresenter.getRollenAnsprechpartnerKatalog());
    }

    private void initializeComponents(final HasComponents components) {

        for (final Component c : components) {

            if (c instanceof AbstractCustomComponent) {
                ((AbstractCustomComponent) c).init();
                ((AbstractCustomComponent) c).initializeValidators();
            }

            if (c instanceof HasComponents) {
                this.initializeComponents((HasComponents) c);
            }
        }
    }

    private boolean isAufgabeAntragPruefen() {
		return true;
//        return presenter.isAufgabeAntragPruefen();
    }

    public void updateFinazierungSummeFields() {
        this.finanzierungTable.getTable().setColumnFooter("eumittel",
                VaadinUtils.FORMAT_CURRENCY.convertToPresentation(presenter.calculateFinanzierungSumme("eumittel"), String.class, Locale.getDefault()));
        this.finanzierungTable.getTable().setColumnFooter("bundesmittel",
                VaadinUtils.FORMAT_CURRENCY.convertToPresentation(presenter.calculateFinanzierungSumme("bundesmittel"), String.class, Locale.getDefault()));
        this.finanzierungTable.getTable().setColumnFooter("landesmittel",
                VaadinUtils.FORMAT_CURRENCY.convertToPresentation(presenter.calculateFinanzierungSumme("landesmittel"), String.class, Locale.getDefault()));
        this.finanzierungTable.getTable().setColumnFooter("kommunalmittel",
                VaadinUtils.FORMAT_CURRENCY.convertToPresentation(presenter.calculateFinanzierungSumme("kommunalmittel"), String.class, Locale.getDefault()));
        this.finanzierungTable.getTable().setColumnFooter("privatemittel",
                VaadinUtils.FORMAT_CURRENCY.convertToPresentation(presenter.calculateFinanzierungSumme("privatemittel"), String.class, Locale.getDefault()));
        this.finanzierungTable.getTable().setColumnFooter("foerderfaehigInvest",
                VaadinUtils.FORMAT_CURRENCY.convertToPresentation(presenter.calculateFinanzierungSumme("foerderfaehigInvest"), String.class, Locale.getDefault()));
    }
}