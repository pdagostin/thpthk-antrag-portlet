package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.KontaktComponentModel;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

/**
 * Created by Nikola Sapun on 3/1/2016.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "name",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.kontakt.name"})),
        @PropertyCaption(propertyId = "telefon",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.kontakt.telefon"})),
        @PropertyCaption(propertyId = "fax",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.kontakt.fax"})),
        @PropertyCaption(propertyId = "email",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.kontakt.email"})),
        @PropertyCaption(propertyId = "rolle",
                caption = @Caption({"thpthk.antrag.antragsteller.grunddaten.kontakt.rolle"})
        )})
public class KontaktComponent extends AbstractCustomComponent<KontaktComponentModel> {

    private static final long serialVersionUID = 7463037408378614550L;

    @PropertyId("name")
    private final TextField name = VaadinUtils.buildBasicTextField();

    @PropertyId("telefon")
    private final TextField telefon = VaadinUtils.buildBasicTextField();

    @PropertyId("fax")
    private final TextField fax = VaadinUtils.buildBasicTextField();

    @PropertyId("email")
    private final TextField email = VaadinUtils.buildBasicTextField();

    @PropertyId("rolle")
    private final ComboBox rolle = VaadinUtils.buildBasicComboBox();

    public KontaktComponent() {
        this.componentModelClass = KontaktComponentModel.class;
    }

    @Override
    public void init() {
        this.setMargin(true);
        this.setSpacing(true);

        final FormLayout layout = VaadinUtils.buildFormLayout();
        layout.addComponents(this.name, this.telefon, this.fax, this.email, this.rolle);
        this.addComponent(layout);
    }

    public void initializeValidators() {
        VaadinUtils.configField(this.name, KontaktComponentModel.class, "name");
        VaadinUtils.configField(this.telefon, KontaktComponentModel.class, "telefon");
        VaadinUtils.configField(this.rolle, KontaktComponentModel.class, "rolle");
        VaadinUtils.configField(this.fax, KontaktComponentModel.class, "fax");
        VaadinUtils.configField(this.email, KontaktComponentModel.class, "email");
    }

    public void setRolleContainer(Container c) {
        this.rolle.setContainerDataSource(c);
    }
}
