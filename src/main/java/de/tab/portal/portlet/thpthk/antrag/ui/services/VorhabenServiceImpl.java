package de.tab.portal.portlet.thpthk.antrag.ui.services;

import de.tab.portal.client.katalog.KatalogClientService;
import de.tab.portal.client.katalog.model.RestKatalogLabel;
import de.tab.portal.client.persistence.PortalPersistenceRestClient;
import de.tab.portal.client.persistence.exceptions.PortalPersistenceRestClientException;
import de.tab.portal.client.persistence.model.kontakt.Person;
import de.tab.portal.client.persistence.model.vorhaben.VorhabenIndikator;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.*;
import de.tab.portal.portlet.thpthk.antrag.ui.persistence.model.Antragsteller;
import de.tab.portal.portlet.thpthk.antrag.ui.persistence.model.ThpthkVorhaben;
import de.tab.portal.portlet.thpthk.antrag.ui.persistence.model.VorhabenOrt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.convert.ConversionService;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.Future;

/**
 * Created by Senf on 29.06.2015.
 */
@Service
public class VorhabenServiceImpl implements VorhabenService {
    private final Logger logger = LoggerFactory.getLogger(VorhabenServiceImpl.class);

    private final PortalPersistenceRestClient restClient;

    @Autowired
    private KatalogClientService katalogClient;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    public VorhabenServiceImpl(final PortalPersistenceRestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public Future<String> getFoerderFallId(final Long vorhabenId) {
        return new AsyncResult<>(getVorhaben(vorhabenId).getContent().wfFoerderfallId);
    }

    @Override
    public Future<Collection<? extends IndikatorenComponentModel>> fetchIndikatorListe(Long vorhabenId) {
        Resource<ThpthkVorhaben> v = getVorhaben(vorhabenId);
        Resources<Resource<VorhabenIndikator>> indikators = this.restClient.toResources(v.getLink("indikatoren").getHref(),
                new ParameterizedTypeReference<Resources<Resource<VorhabenIndikator>>>() {
                });

        String currentViewName = this.getFoerdergegenstand(vorhabenId);
        List<RestKatalogLabel> katalogLabels = new ArrayList<>();
        switch (currentViewName) {
            case "VERKON":
                katalogLabels = this.katalogClient.getKatalogLabels("Indikatoren_Verwaltung_und_KontrolleTHP");
                break;
            case "INKOMM":
                katalogLabels = this.katalogClient.getKatalogLabels("Indikatoren_Informations_und_KommunikationsmassnahmenTHK");
                break;
            case "MONEV":
                katalogLabels = this.katalogClient.getKatalogLabels("Indikatoren_Monitoring_und_EvaluierungTHP");
                break;
            case "EDA":
                katalogLabels = this.katalogClient.getKatalogLabels("Indikatoren_Elektronisches_THP");
                break;
        }

        List<IndikatorenComponentModel> models = new ArrayList<>();

        if (indikators != null && indikators.iterator().hasNext()) {
            Iterator<Resource<VorhabenIndikator>> iter = indikators.getContent().iterator();

            while (iter.hasNext()) {
                Resource<VorhabenIndikator> ir = iter.next();
                VorhabenIndikator extractedIr = ir.getContent();

                for (RestKatalogLabel rkl : katalogLabels) {
                    if (rkl.key.equals(extractedIr.indikator)) {
                        IndikatorenComponentModel model = this.conversionService.convert(rkl, IndikatorenComponentModel.class);

                        // If indikator has nested catalog fetch it
                        if ("DDLIST".equals(model.getIndikatorTyp()) && model.getIndikatorKatalogName() != null) {
                            model.setKatalog(this.katalogClient.getKatalogKeyText(model.getIndikatorKatalogName()));
                        }

                        model.setSollwert(extractedIr.sollwert);
                        model.setIstwert(extractedIr.istwert);
                        model.setSelfHref(ir.getId().getHref());

                        models.add(model);
                        break;
                    }
                }
            }
        } else {
            for (RestKatalogLabel rkl : katalogLabels) {
                IndikatorenComponentModel model = this.conversionService.convert(rkl, IndikatorenComponentModel.class);

                // If indikator has nested catalog fetch it
                if ("DDLIST".equals(model.getIndikatorTyp()) && model.getIndikatorKatalogName() != null) {
                    model.setKatalog(this.katalogClient.getKatalogKeyText(model.getIndikatorKatalogName()));
                }

                models.add(model);
            }
        }

        return new AsyncResult<Collection<? extends IndikatorenComponentModel>>(models);
    }

    @Override
    public Future<AdresseComponentModel> findOrCreateVorhabenAdresse(final long vorhabenId) {
        final Antragsteller antragsteller = getAntragsteller(vorhabenId);
        AdresseComponentModel adresseComponentModel;
        if (antragsteller.adresse == null) {
            adresseComponentModel = new AdresseComponentModel();
        } else {
            adresseComponentModel = antragsteller.adresse;
        }
        return new AsyncResult<>(adresseComponentModel);
    }

    @Override
    public Future<PostfachComponentModel> findOrCreatePostfach(final long vorhabenId) {
        final Antragsteller antragsteller = this.getAntragsteller(vorhabenId);

        final PostfachComponentModel result;
        if (antragsteller.postfach == null) {
            result = new PostfachComponentModel();
        } else {
            result = antragsteller.postfach;
        }

        return new AsyncResult<>(result);
    }

    @Override
    public Future<Collection<? extends KontaktComponentModel>> findOrCreateVorhabenKontakt(final Long vorhabenId) {
        final String href = this.getVorhaben(vorhabenId).getLink("ansprechpartner").getHref();
        final ArrayList<KontaktComponentModel> models = new ArrayList<>();

        final Resources<Resource<KontaktComponentModel>> resources = this.restClient.toResources(href,
                new ParameterizedTypeReference<Resources<Resource<KontaktComponentModel>>>() {
                });

        for (final Resource<KontaktComponentModel> resource : resources) {
            final KontaktComponentModel model = resource.getContent();
            model.setSelfHref(resource.getId().getHref());
            models.add(model);
        }

        return new AsyncResult<Collection<? extends KontaktComponentModel>>(models);
    }

    @Override
    public Future<SpezifischeDatenComponentModel> findOrCreateSpezifischeDaten(final Long vorhabenId) {
        final Antragsteller antragsteller = this.getAntragsteller(vorhabenId);

        final SpezifischeDatenComponentModel result;
        if (antragsteller.spezifischeDaten == null) {
            result = new SpezifischeDatenComponentModel();
        } else {
            result = antragsteller.spezifischeDaten;
        }

        return new AsyncResult<>(result);
    }

    public Future<VorhabenszeitraumComponentModel> findOrCreateVorhabenZeitraum(Long vorhabenId) {
        final ThpthkVorhaben vorhaben = getVorhaben(vorhabenId).getContent();
        VorhabenszeitraumComponentModel vorhabenszeitraumComponentModel = null;
        if (vorhaben.zeitraum == null) {
            vorhabenszeitraumComponentModel = new VorhabenszeitraumComponentModel();
        } else {
            vorhabenszeitraumComponentModel = vorhaben.zeitraum;
        }

        return new AsyncResult<>(vorhabenszeitraumComponentModel);
    }

    @Override
    public Future<BrancheComponentModel> findOrCreateBranche(final Long vorhabenId) {
        final ThpthkVorhaben vorhaben = this.getVorhaben(vorhabenId).getContent();
        final BrancheComponentModel brancheComponentModel = new BrancheComponentModel();
        brancheComponentModel.setBranche(vorhaben.branche);

        return new AsyncResult<>(brancheComponentModel);

    }

    @Override
    public void createOrtDesVorhabens(final Long vorhabenId, final OrtComponentModel item) {
        final VorhabenOrt vorhabenOrt = new VorhabenOrt();
        vorhabenOrt.adresse = item;
        final String href = this.restClient.create("vorhabenOrts", vorhabenOrt);
        item.setSelfHref(href);
        final Resource<ThpthkVorhaben> vorhaben = this.getVorhaben(vorhabenId);

        this.restClient.addAssociatedEntity(vorhaben, "orte", href);
    }

    @Override
    public void deleteOrtDesVorhabens(final OrtComponentModel item) {
        final String selfHref = item.getSelfHref();
        this.restClient.delete(selfHref);
    }

    @Override
    public void updateOrtDesVorhabens(final OrtComponentModel item) {
        this.restClient.patch(item.getSelfHref(), "adresse", item);
    }

    @Override
    public void createKontakt(Long vorhabenId, KontaktComponentModel item) {
        final String href = this.restClient.create("persons", item);
        item.setSelfHref(href);
        final Resource<ThpthkVorhaben> vorhaben = this.getVorhaben(vorhabenId);

        this.restClient.addAssociatedEntity(vorhaben, "ansprechpartner", href);
    }

    @Override
    public void deleteKontakt(final Long vorhabenId, KontaktComponentModel item) {
        final String selfHref = item.getSelfHref();
		final Resource<ThpthkVorhaben> vorhaben = this.getVorhaben(vorhabenId);
		this.restClient.removeAssociatedEntity(vorhaben, "ansprechpartner", selfHref);
		this.restClient.delete(selfHref);
    }

    @Override
    public void updateKontakt(KontaktComponentModel item) {
        this.restClient.patch(item.getSelfHref(), item);
    }

    @Override
    public Future<Collection<? extends OrtComponentModel>> findOrCreateVorhabenOrtDesVorhabens(final long vorhabenId) {
        final String vorhaben = this.getVorhaben(vorhabenId).getLink("orte").getHref();
        final ArrayList<OrtComponentModel> models = new ArrayList<>();

        final Resources<Resource<ThpthkVorhaben>> resources = this.restClient.toResources(vorhaben,
                new ParameterizedTypeReference<Resources<Resource<ThpthkVorhaben>>>() {
                });

        for (final Resource<ThpthkVorhaben> resource : resources) {
            final OrtComponentModel ort = resource.getContent().adresse;
            ort.setSelfHref(resource.getId().getHref());
            models.add(ort);
        }
        return new AsyncResult<Collection<? extends OrtComponentModel>>(models);
    }

    @Override
    public Future<Collection<? extends AufteilungDerAusgabenAufJahreComponentModel>> findOrCreateAufteilungDerAusgabenAufJahre(long vorhabenId) {
        final String href = this.getVorhaben(vorhabenId).getLink("ausgabenplan").getHref();
        final ArrayList<AufteilungDerAusgabenAufJahreComponentModel> models = new ArrayList<>();

        final Resources<Resource<AufteilungDerAusgabenAufJahreComponentModel>> resources = this.restClient.toResources(href,
                new ParameterizedTypeReference<Resources<Resource<AufteilungDerAusgabenAufJahreComponentModel>>>() {
                });

        for (final Resource<AufteilungDerAusgabenAufJahreComponentModel> resource : resources) {
            final AufteilungDerAusgabenAufJahreComponentModel model = resource.getContent();
            model.setSelfHref(resource.getId().getHref());
            models.add(model);
        }
        return new AsyncResult<Collection<? extends AufteilungDerAusgabenAufJahreComponentModel>>(models);
    }

    @Override
    public void createAufteilungDerAusgabenAufJahre(Long vorhabenId, AufteilungDerAusgabenAufJahreComponentModel item) {
        final String href = this.restClient.create("thpthkAusgabes", item);
        item.setSelfHref(href);
        final Resource<ThpthkVorhaben> vorhaben = this.getVorhaben(vorhabenId);

        this.restClient.addAssociatedEntity(vorhaben, "ausgabenplan", href);
    }

    @Override
    public void deleteAufteilungDerAusgabenAufJahre(AufteilungDerAusgabenAufJahreComponentModel item) {
        final String selfHref = item.getSelfHref();
        this.restClient.delete(selfHref);
    }

    @Override
    public void updateAufteilungDerAusgabenAufJahre(AufteilungDerAusgabenAufJahreComponentModel item) {
        this.restClient.patch(item.getSelfHref(), item);
    }

    @Override
    public Future<AufbewahrungsortDerBelegeComponentModel> findOrCreateAufbewahrungsortDerBelege(final Long vorhabenId) {
        final ThpthkVorhaben vorhaben = getVorhaben(vorhabenId).getContent();
        final AufbewahrungsortDerBelegeComponentModel aufbewahrungsortDerBelegeComponentModel = new AufbewahrungsortDerBelegeComponentModel();
        aufbewahrungsortDerBelegeComponentModel.setBelegeAufbewahrungsort(vorhaben.belegeAufbewahrungsort);

        return new AsyncResult<>(aufbewahrungsortDerBelegeComponentModel);

    }

    @Override
    public Future<GeplanteAusgabenComponentModel> findOrCreateGeplanteAusgaben(final Long vorhabenId) {
        final ThpthkVorhaben vorhaben = getVorhaben(vorhabenId).getContent();
        final GeplanteAusgabenComponentModel aufbewahrungsortDerBelegeComponentModel = new GeplanteAusgabenComponentModel();
        aufbewahrungsortDerBelegeComponentModel.setGesamtausgabenPlan(vorhaben.gesamtausgabenPlan);

        return new AsyncResult<>(aufbewahrungsortDerBelegeComponentModel);
    }

    @Override
    public Future<StammdatenComponentModel> findOrCreateStammdaten(final Long vorhabenId) {
        final ThpthkVorhaben vorhaben = getVorhaben(vorhabenId).getContent();

        final StammdatenComponentModel result;
        if (vorhaben.stammdaten == null) {
            result = new StammdatenComponentModel();
        } else {
            result = vorhaben.stammdaten;
            result.setAktenzeichen(vorhaben.aktenzeichen);
        }

        return new AsyncResult<>(result);
    }

    @Override
    public void save(final Long vorhabenId,
                     final AdresseComponentModel adresseComponentModel,
                     final PostfachComponentModel postfachComponentModel,
                     final List<IndikatorenComponentModel> indikatorList,
                     final SpezifischeDatenComponentModel spezifischeDatenComponentModel,
                     final VorhabenszeitraumComponentModel vorhabenszeitraumComponentModel,
                     final BrancheComponentModel brancheComponentModel,
                     final AufbewahrungsortDerBelegeComponentModel aufbewahrungsortDerBelegeComponentModel,
                     final GeplanteAusgabenComponentModel geplanteAusgabenComponentModel,
                     final StammdatenComponentModel stammdatenComponentModel,
                     final AktuellerBewilligungsstandComponentModel aktuellerBewilligungsstandComponentModel,
                     final BewilligteAusgabenComponentModel bewilligteAusgabenComponentModel,
                     final AnsprechpartnerMeldestelleComponentModel ansprechpartnerMeldestelleComponentModel)
            throws PortalPersistenceRestClientException {

        final Resource<ThpthkVorhaben> vorhabenResource = getVorhaben(vorhabenId);
        final String vorhabenUrl = vorhabenResource.getId().getHref();

        final Resource<Antragsteller> antragstellerResource = this.getAntragstellerResourceAndCreateIfNecessary(vorhabenId);
        final String antragstellerUrl = antragstellerResource.getId().getHref();

        this.restClient.patch(antragstellerUrl, "adresse", adresseComponentModel);
        this.restClient.patch(antragstellerUrl, "postfach", postfachComponentModel);
        this.restClient.patch(antragstellerUrl, "spezifischeDaten", spezifischeDatenComponentModel);
        this.restClient.patch(vorhabenUrl, "zeitraum", vorhabenszeitraumComponentModel);
        this.restClient.patch(vorhabenUrl, brancheComponentModel);
        this.restClient.patch(vorhabenUrl, aufbewahrungsortDerBelegeComponentModel);
        this.restClient.patch(vorhabenUrl, "gesamtausgabenPlan", geplanteAusgabenComponentModel.getGesamtausgabenPlan());
        this.restClient.patch(vorhabenUrl, "stammdaten", stammdatenComponentModel);
        this.restClient.patch(vorhabenUrl, "aktenzeichen", stammdatenComponentModel.getAktenzeichen());
        this.restClient.patch(vorhabenUrl, aktuellerBewilligungsstandComponentModel);
        this.restClient.patch(vorhabenUrl, bewilligteAusgabenComponentModel);

        Resource<Person> personResource = getAnsprechpartnerMeldestelleResourceAndCreateIfNecessary(vorhabenId);
        restClient.patch(personResource.getId().getHref(), ansprechpartnerMeldestelleComponentModel);

        for (IndikatorenComponentModel item : indikatorList) {
            createOrUpdateIndikator(vorhabenId, item);
        }
    }

    private void createOrUpdateIndikator(Long vorhabenId, IndikatorenComponentModel item) {
        if (item.getSelfHref() != null) {
            // Indikator is already created and can be safely updated
            this.restClient.patch(item.getSelfHref(), item);
        } else {
            // Indikator needs to be created on rest client to be updated
            Resource<ThpthkVorhaben> v = getVorhaben(vorhabenId);

            String href = this.restClient.create("vorhabenIndikators", item);
            item.setSelfHref(href);

            this.restClient.addAssociatedEntity(v, "indikatoren", href);
        }
    }

    private Resource<Antragsteller> getAntragstellerResourceAndCreateIfNecessary(final Long vorhabenId) {
        final Resource<ThpthkVorhaben> vorhabenResource = getVorhaben(vorhabenId);

        Resource<Antragsteller> antr =
                this.restClient.toResource(vorhabenResource.getLink("antragsteller").getHref(),
                        new ParameterizedTypeReference<Resource<Antragsteller>>() {
                        });

        if (antr == null) {
            final Antragsteller neu = new Antragsteller();
            final String url = this.restClient.create("antragstellers", neu);
            this.restClient.setAssociatedEntity(vorhabenResource, "antragsteller", url);

            antr = new Resource<>(neu);
        }

        return antr;
    }

    private Antragsteller getAntragsteller(final Long vorhabenId) {
        final Resource<Antragsteller> antragstellerResource = this.getAntragstellerResourceAndCreateIfNecessary(vorhabenId);

        return antragstellerResource.getContent();
    }

    @Override
    public Resource<ThpthkVorhaben> getVorhaben(final Long vorhabenId) {
        final String url = this.restClient.rel("thpthkVorhabens").concat(vorhabenId.toString());
        return this.restClient.toResource(url, new ParameterizedTypeReference<Resource<ThpthkVorhaben>>() {
        });
    }

    @Override
    public String getFoerdergegenstand(Long vorhabenId) {
        ThpthkVorhaben vorhaben = getVorhaben(vorhabenId).getContent();

        return vorhaben.foerdergegenstand;
    }

    @Override
    public void createFinanzierung(Long vorhabenId, FinanzierungComponentModel item) {
        final String href = this.restClient.create("thpthkFinanzierungs", item);
        item.setSelfHref(href);
        final Resource<ThpthkVorhaben> vorhaben = this.getVorhaben(vorhabenId);

        this.restClient.addAssociatedEntity(vorhaben, "finanzierungplan", href);
    }

    @Override
    public void deleteFinanzierung(FinanzierungComponentModel item) {
        final String selfHref = item.getSelfHref();
        this.restClient.delete(selfHref);
    }

    @Override
    public void updateFinanzierung(FinanzierungComponentModel item) {
        this.restClient.patch(item.getSelfHref(), item);
    }

    @Override
    public Future<Collection<? extends FinanzierungComponentModel>> findOrCreateFinazierung(long vorhabenId) {
        final String href = this.getVorhaben(vorhabenId).getLink("finanzierungplan").getHref();
        final ArrayList<FinanzierungComponentModel> models = new ArrayList<>();

        final Resources<Resource<FinanzierungComponentModel>> resources = this.restClient.toResources(href,
                new ParameterizedTypeReference<Resources<Resource<FinanzierungComponentModel>>>() {
                });

        for (final Resource<FinanzierungComponentModel> resource : resources) {
            final FinanzierungComponentModel model = resource.getContent();
            model.setSelfHref(resource.getId().getHref());
            models.add(model);
        }
        return new AsyncResult<Collection<? extends FinanzierungComponentModel>>(models);
    }

    @Override
    public Future<AktuellerBewilligungsstandComponentModel> findOrCreateAktuellerBewilligungsstand(Long vorhabenId) {
        final ThpthkVorhaben vorhaben = this.getVorhaben(vorhabenId).getContent();
        final AktuellerBewilligungsstandComponentModel aktuellerBewilligungsstandComponentModel = new AktuellerBewilligungsstandComponentModel();
        aktuellerBewilligungsstandComponentModel.setGesamtausgabenFoerderfaehig(vorhaben.gesamtausgabenFoerderfaehig);
        aktuellerBewilligungsstandComponentModel.setFoerdersatz(vorhaben.foerdersatz);
        aktuellerBewilligungsstandComponentModel.setGesamtausgaben(vorhaben.gesamtausgaben);

        return new AsyncResult<>(aktuellerBewilligungsstandComponentModel);
    }

    @Override
    public Future<BewilligteAusgabenComponentModel> findOrCreateBewilligteAusgaben(Long vorhabenId) {
        final ThpthkVorhaben vorhaben = this.getVorhaben(vorhabenId).getContent();
        final BewilligteAusgabenComponentModel bewilligteAusgabenComponentModel = new BewilligteAusgabenComponentModel();
        bewilligteAusgabenComponentModel.setFoerderAusgabenBew(vorhaben.foerderAusgabenBew);
        bewilligteAusgabenComponentModel.setFoerdersatzBew(vorhaben.foerdersatzBew);
        bewilligteAusgabenComponentModel.setGesamtausgabenBew(vorhaben.gesamtausgabenBew);

        return new AsyncResult<>(bewilligteAusgabenComponentModel);
    }

    @Override
    public Future<AnsprechpartnerMeldestelleComponentModel> findOrCreateAnsprechpartnerMeldestelle(Long vorhabenId) {
        Resource<Person> personResource = getAnsprechpartnerMeldestelleResourceAndCreateIfNecessary(vorhabenId);
        AnsprechpartnerMeldestelleComponentModel ansprechpartnerMeldestelleComponentModel = new AnsprechpartnerMeldestelleComponentModel();
        Person person = personResource.getContent();
        if (personResource.getContent() != null) {
            ansprechpartnerMeldestelleComponentModel.setEmail(person.email);
            ansprechpartnerMeldestelleComponentModel.setName(person.name);
            ansprechpartnerMeldestelleComponentModel.setTelefon(person.telefon);
        }

        return new AsyncResult<>(ansprechpartnerMeldestelleComponentModel);
    }

    private Resource<Person> getAnsprechpartnerMeldestelleResourceAndCreateIfNecessary(final Long vorhabenId) {
        final Resource<ThpthkVorhaben> vorhabenResource = this.getVorhaben(vorhabenId);

        Resource<Person> personResource =
                this.restClient.toResource(vorhabenResource.getLink("ansprechpartnerMeldestelle").getHref(),
                        new ParameterizedTypeReference<Resource<Person>>() {
                        });

        if (personResource == null) {
            final Person neu = new Person();
            final String url = this.restClient.create("persons", neu);
            this.restClient.setAssociatedEntity(vorhabenResource, "ansprechpartnerMeldestelle", url);

            personResource = new Resource<>(neu);
        }

        return personResource;
    }

    @Override
    public void saveFormularIsValidTo(final Long vorhabenId, final Date time) {
//        final Resource<ThpthkVorhaben> thpthkVorhabenResource = getVorhaben(vorhabenId);
//        this.restClient.patch(thpthkVorhabenResource.getId().getHref(), "antragFormularIsValidUntil", time);
    }
}
