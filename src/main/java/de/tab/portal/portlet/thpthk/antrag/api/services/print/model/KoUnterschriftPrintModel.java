package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

public class KoUnterschriftPrintModel {

	public String textLinksBezeichnung = "Ort, Datum";
	public String textRechtsBezeichnung =
			"rechtsverbindliche Unterschrift(en) der/des Antragsteller/s / Stempel sowie Namen in Druckbuchstaben";

}
