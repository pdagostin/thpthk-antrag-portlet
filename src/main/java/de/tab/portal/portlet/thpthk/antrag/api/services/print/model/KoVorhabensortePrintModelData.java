package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoVorhabensortePrintModelData {

	public String strassenr;
	public String plz;
	public String ort;
	public String land;

}
