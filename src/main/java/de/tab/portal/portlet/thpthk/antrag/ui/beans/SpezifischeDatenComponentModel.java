package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import de.tab.portal.commons.KeyText;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Nikola Sapun on 3/1/2016.
 */
public class SpezifischeDatenComponentModel implements Serializable {

    private static final long serialVersionUID = 5626623652064669236L;

    @NotNull
    private KeyText rechtsform;

    @NotNull
	private KeyText einordnung;

	private Date gruendungsdatum;

    @NotNull
	private String branche;

    @NotNull
	private Boolean erstattungsfaehigkeitMwst;

	public KeyText getRechtsform() {
		return this.rechtsform;
	}

	public void setRechtsform(KeyText rechtsform) {
		this.rechtsform = rechtsform;
	}

	public KeyText getEinordnung() {
		return this.einordnung;
	}

	public void setEinordnung(KeyText einordnung) {
		this.einordnung = einordnung;
	}

	public Date getGruendungsdatum() {
		return this.gruendungsdatum;
	}

	public void setGruendungsdatum(Date gruendungsdatum) {
		this.gruendungsdatum = gruendungsdatum;
	}

	public String getBranche() {
		return this.branche;
	}

	public void setBranche(String branche) {
		this.branche = branche;
	}

	public Boolean getErstattungsfaehigkeitMwst() {
		return this.erstattungsfaehigkeitMwst;
	}

	public void setErstattungsfaehigkeitMwst(Boolean erstattungsfaehigkeitMwst) {
		this.erstattungsfaehigkeitMwst = erstattungsfaehigkeitMwst;
	}

	@Override
	public String toString() {
		return "SpezifischeDatenComponentModel{" +
				"rechtsform=" + this.rechtsform +
				", einordnungAntragsteller=" + this.einordnung +
				", gruendungsdatum=" + this.gruendungsdatum +
				", brancheAntragsteller=" + this.branche +
				", erstattungsfaehigkeit=" + this.erstattungsfaehigkeitMwst +
				'}';
	}


}
