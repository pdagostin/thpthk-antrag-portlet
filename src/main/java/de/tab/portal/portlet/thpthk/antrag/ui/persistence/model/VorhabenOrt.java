package de.tab.portal.portlet.thpthk.antrag.ui.persistence.model;

import de.tab.portal.client.persistence.model.BaseEntity;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.OrtComponentModel;

/**
 * @author hrvoje.krot on 26.1.2016.
 * Model class which represents VorhabenOrt backend resource
 */
public class VorhabenOrt extends BaseEntity {
    public OrtComponentModel adresse;

    public VorhabenOrt() {
    }
}
