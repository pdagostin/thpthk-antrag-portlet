package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Nikola Sapun on 3/3/2016.
 */
public class AufteilungDerAusgabenAufJahreComponentModel implements Serializable {

    private static final long serialVersionUID = 2473357412866250330L;

    @NotNull
	private String jahr;

    @NotNull
	@DecimalMin(value = "1", message = "{tab.validation.DecimalMin.message}")
	@Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
	private BigDecimal gesamtausgaben;

    @NotNull
	@Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
	private BigDecimal zuwendungsausgaben;

	private String selfHref;

	public String getJahr() {
		return this.jahr;
	}

	public void setJahr(String jahr) {
		this.jahr = jahr;
	}

	public BigDecimal getGesamtausgaben() {
		return this.gesamtausgaben;
	}

	public void setGesamtausgaben(BigDecimal gesamtausgaben) {
		this.gesamtausgaben = gesamtausgaben;
	}

	public BigDecimal getZuwendungsausgaben() {
		return this.zuwendungsausgaben;
	}

	public void setZuwendungsausgaben(BigDecimal zuwendungsausgaben) {
		this.zuwendungsausgaben = zuwendungsausgaben;
	}

	public String getSelfHref() {
		return this.selfHref;
	}

	public void setSelfHref(String selfHref) {
		this.selfHref = selfHref;
	}

	@Override
	public String toString() {
		return "AufteilungDerAusgabenAufJahreComponentModel{" +
				"jahr=" + this.jahr +
				", gesamtausgaben=" + this.gesamtausgaben +
				", zuwendungsausgaben=" + this.zuwendungsausgaben +
				", selfHref='" + this.selfHref + '\'' +
				'}';
	}
}
