package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by ana.beblek on 1.3.2016..
 */
public class BrancheComponentModel implements Serializable {
    private static final long serialVersionUID = 429162226185925967L;

    @NotNull
    private String branche;

    public String getBranche() {
        return this.branche;
    }

    public void setBranche(String branche) {
        this.branche = branche;
    }

    @Override
    public String toString() {
        return "BrancheComponentModel [branche=" + this.branche + "]";
    }
}
