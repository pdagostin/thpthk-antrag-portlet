package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.BewilligteAusgabenComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.util.ComponentUtil;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

/**
 * @author hrvoje.krot on 14.3.2016.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "gesamtausgabenBew",
                caption = @Caption({"thpthk.antrag.stammdaten.ausgaben.bewilligteAusgaben.gesamtausgabenBew"})),
        @PropertyCaption(propertyId = "foerderAusgabenBew",
                caption = @Caption({"thpthk.antrag.stammdaten.ausgaben.bewilligteAusgaben.foerderAusgabenBew"})),
        @PropertyCaption(propertyId = "foerdersatzBew",
                caption = @Caption({"thpthk.antrag.stammdaten.ausgaben.bewilligteAusgaben.foerdersatzBew"}))})
public class BewilligteAusgabenComponent extends FormLayout implements Item.Editor {

    @PropertyId("gesamtausgabenBew")
    private final TextField gesamtausgabenBew = VaadinUtils.buildBasicTextField();

    @PropertyId("foerderAusgabenBew")
    private final TextField foerderAusgabenBew = VaadinUtils.buildBasicTextField();

    @PropertyId("foerdersatzBew")
    private final TextField foerdersatzBew = VaadinUtils.buildBasicTextField();

    private TranslationService translationService;

    public BewilligteAusgabenComponent() {
        this.buildLayout();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.gesamtausgabenBew, this.foerderAusgabenBew, this.foerdersatzBew);
    }

    private void initValidators() {
        VaadinUtils.configField(this.gesamtausgabenBew, BewilligteAusgabenComponentModel.class, "gesamtausgabenBew");
        VaadinUtils.configField(this.foerderAusgabenBew, BewilligteAusgabenComponentModel.class, "foerderAusgabenBew");
        VaadinUtils.configField(this.foerdersatzBew, BewilligteAusgabenComponentModel.class, "foerdersatzBew");
    }

    public void initConverters() {
        ComponentUtil.setCurrencyConverter(this.gesamtausgabenBew, this.foerderAusgabenBew);
        ComponentUtil.setConversionErrorMsg(translationService.translate("tab.validation.Number.message"),
                this.gesamtausgabenBew, this.foerderAusgabenBew, this.foerdersatzBew);
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }

    public void setTranslationService(TranslationService translationService) {
        this.translationService = translationService;
    }
}
