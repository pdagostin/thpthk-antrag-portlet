package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoBranchePrintModel {
	public String title = "Branche des Vorhabens";

	public String brancheBezeichnung = "Branche";
	public String branche;
}
