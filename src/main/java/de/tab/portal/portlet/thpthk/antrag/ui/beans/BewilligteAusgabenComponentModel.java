package de.tab.portal.portlet.thpthk.antrag.ui.beans;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hrvoje.krot on 14.3.2016.
 * Model for <code>Component</code> {@link de.tab.portal.portlet.thpthk.antrag.ui.components.BewilligteAusgabenComponent}
 */
public class BewilligteAusgabenComponentModel implements Serializable {
    private static final long serialVersionUID = -2863854605132656088L;

    @NotNull
    @DecimalMin(value = "0", message = "{tab.validation.DecimalMin.message}")
    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal gesamtausgabenBew;

    @NotNull
    @DecimalMin(value = "0", message = "{tab.validation.DecimalMin.message}")
    @Digits(integer = 14, fraction = 2, message = "{tab.validation.Digits.message}")
    private BigDecimal foerderAusgabenBew;

    @DecimalMin(value = "0", message = "{tab.validation.DecimalMin.message}")
    @DecimalMax(value = "100", message = "{tab.validation.DecimalMax.message}")
    @Digits(integer = 3, fraction = 0, message = "{tab.validation.Digits.message}")
    private BigDecimal foerdersatzBew;

    public BigDecimal getGesamtausgabenBew() {
        return this.gesamtausgabenBew;
    }

    public void setGesamtausgabenBew(BigDecimal gesamtausgabenBew) {
        this.gesamtausgabenBew = gesamtausgabenBew;
    }

    public BigDecimal getFoerderAusgabenBew() {
        return this.foerderAusgabenBew;
    }

    public void setFoerderAusgabenBew(BigDecimal foerderAusgabenBew) {
        this.foerderAusgabenBew = foerderAusgabenBew;
    }

    public BigDecimal getFoerdersatzBew() {
        return this.foerdersatzBew;
    }

    public void setFoerdersatzBew(BigDecimal foerdersatzBew) {
        this.foerdersatzBew = foerdersatzBew;
    }

    @Override
    public String toString() {
        return "BewilligteAusgabenComponentModel{" +
                "gesamtausgabenBew=" + gesamtausgabenBew +
                ", foerderAusgabenBew=" + foerderAusgabenBew +
                ", foerdersatzBew=" + foerdersatzBew +
                '}';
    }
}
