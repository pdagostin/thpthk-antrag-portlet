package de.tab.portal.portlet.thpthk.antrag.ui.components;

/**
 * @author IgorDjekic
 * @since 28.10.2015.
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import de.tab.vaadin.components.*;
import de.tab.vaadin.container.filter.ContainerFilter;
import de.tab.vaadin.container.filter.CustomFilterBeanItemContainer;
import de.tab.vaadin.container.listener.ContainerAddRemoveListener;
import de.tab.vaadin.container.listener.ContainerDataChangeListener;
import de.tab.vaadin.security.PermissionChecker;
import de.tab.vaadin.security.TabContainerPermission;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "newButton",
                caption = @Caption({"Neu"})),
        @PropertyCaption(propertyId = "editButton",
                caption = @Caption({"Bearbeiten"})),
        @PropertyCaption(propertyId = "deleteButton",
                caption = @Caption({"Entfernen"}))})
public class CustomEditTableComponent<K> extends AbstractCustomComponent<K> implements Button.ClickListener, ConfirmEvents.ConfirmListener, Property.ValueChangeListener {
    private static final long serialVersionUID = 5323367644629943162L;
    private EditItemDialogContent<K> dialogContent;
    private final Table table = new Table();
    @PropertyId("newButton")
    private final Button newButton = new Button();
    @PropertyId("editButton")
    private final Button editButton = new Button();
    @PropertyId("deleteButton")
    private final Button deleteButton = new Button();
    private final TextField filterField = VaadinUtils.buildBasicTextField();
    private final Button filterButton = new Button("Filter");
    @PropertyId("confirmWindow")
    private final ConfirmWindow confirmWindow = new ConfirmWindow();
    private BeanItemContainer<K> tableContainer;
    private Object[] visibleColums = new Object[0];
    private final List<ContainerDataChangeListener<K>> containerDataChangeListener = new ArrayList();
    private ContainerAddRemoveListener<K> containerAddRemoveListener;
    private EditTableComponent.ButtonsPermissions<K> buttonsPermissions;

    public CustomEditTableComponent(EditItemDialogContent<K> dialogContent) {
        this.componentModelClass = dialogContent.getComponentModelClass();
        this.containerAddRemoveListener = new ContainerAddRemoveListener(this.containerDataChangeListener);
        this.buttonsPermissions = new NotNullEditTableComponentButtonsPermissions();
        this.dialogContent = dialogContent;
        this.dialogContent.setFieldGroupBuffered(true);
    }

    public ConfirmWindow getConfirmWindow() {
        return this.confirmWindow;
    }

    @Override
    public void init() {
        this.dialogContent.init();
        this.dialogContent.initializeValidators();
        this.confirmWindow.setContent(this.dialogContent.getContent());
        this.confirmWindow.addConfirmListener(this);
        this.confirmWindow.setOkCaption("Ok");
        this.confirmWindow.setCancelCaption("Abbrechen");
        VerticalLayout layout = VaadinUtils.buildBasicVerticalLayout();
        layout.setStyleName("editTableComponent");
        layout.setSizeFull();
        this.table.setSizeFull();
        this.table.setSelectable(true);
        this.table.addValueChangeListener(this);
        layout.addComponent(this.table);
        layout.setExpandRatio(this.table, 1.0F);
        this.buildButtonGroup(layout);
        this.applyPermissions();
        this.setVisibilityFilterOption();
        this.addComponent(layout);
    }

    private void buildButtonGroup(VerticalLayout layout) {
        CssLayout buttonGroup = new CssLayout();
        buttonGroup.addStyleName("v-component-group");
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        horizontalLayout.addComponents(new Component[]{buttonGroup, this.filterField, this.filterButton});
        layout.addComponent(horizontalLayout);
        layout.setExpandRatio(horizontalLayout, 0.0F);
        this.filterButton.addClickListener(this);
        this.filterField.setInputPrompt("Filter hier eintragen");
        buttonGroup.addComponents(new Component[]{this.newButton, this.editButton, this.deleteButton});
        this.newButton.setIcon(FontAwesome.PLUS);
        this.newButton.addClickListener(this);
        this.editButton.setIcon(FontAwesome.EDIT);
        this.editButton.setEnabled(false);
        this.editButton.addClickListener(this);
        this.deleteButton.setIcon(FontAwesome.MINUS);
        this.deleteButton.setEnabled(false);
        this.deleteButton.addClickListener(this);
    }

    private void applyPermissions() {
        if(this.componentModelClass.isAnnotationPresent(TabContainerPermission.class)) {
            TabContainerPermission permission = (TabContainerPermission)this.componentModelClass.getAnnotation(TabContainerPermission.class);
            this.newButton.setEnabled(this.newButton.isEnabled() && PermissionChecker.canWrite(permission));
            this.editButton.setEnabled(this.editButton.isEnabled() && PermissionChecker.canWrite(permission));
            this.deleteButton.setEnabled(this.deleteButton.isEnabled() && PermissionChecker.canAll(permission));
        }

    }


    public void setNewItemsEnabled(boolean enabled) {
        this.newButton.setEnabled(enabled);
    }

    public void setColumnHeader(Object propertyId, String header) {
        this.table.setColumnHeader(propertyId, header);
    }

    public boolean addContainerProperty(Object propertyId, Class<?> type, Object defaultValue) throws UnsupportedOperationException {
        return this.table.addContainerProperty(propertyId, type, defaultValue);
    }

    public void setContainerDataSource(Container newDataSource) {
        if(!(newDataSource instanceof BeanItemContainer)) {
            throw new IllegalArgumentException("newDataSource must be instanceof BeanItemContainer");
        } else {
            this.tableContainer = (BeanItemContainer)newDataSource;
            this.table.setContainerDataSource(newDataSource);
            this.tableContainer.removeItemSetChangeListener(this.containerAddRemoveListener);
            this.tableContainer.addItemSetChangeListener(this.containerAddRemoveListener);
            this.setVisibilityFilterOption();
            if(this.visibleColums.length != 0) {
                this.table.setVisibleColumns(PermissionChecker.filter(this.componentModelClass, this.visibleColums));
                Object[] var2 = this.table.getVisibleColumns();
                int var3 = var2.length;

                for(int var4 = 0; var4 < var3; ++var4) {
                    Object column = var2[var4];
                    if(column instanceof String) {
                        VaadinUtils.configFormatterForTable(this.table, this.componentModelClass, (String)column);
                    }
                }
            }

        }
    }

    public void setItemDataSource(Item item) {
        throw new UnsupportedOperationException("Methode wurde nicht implementiert! Fragen Sie ihren Anwalt!");
    }

    public BeanItemContainer<?> getContainerDataSource() {
        return this.tableContainer;
    }

    public Table getTable() {
        return this.table;
    }

    public void buttonClick(Button.ClickEvent event) {
        Button clickedButton = event.getButton();
        if(clickedButton.equals(this.newButton)) {
            this.dialogContent.resetData();
            this.confirmWindow.center();
            UI.getCurrent().addWindow(this.confirmWindow);
        } else {
            Object value;
            if(clickedButton.equals(this.editButton)) {
                value = this.table.getValue();
                BeanItem item = this.tableContainer.getItem(value);
                this.dialogContent.setItemDataSource(item);
                this.confirmWindow.center();
                UI.getCurrent().addWindow(this.confirmWindow);
            } else if(clickedButton.equals(this.deleteButton)) {
                this.containerAddRemoveListener.setActive(Boolean.valueOf(true));
                value = this.table.getValue();
                this.tableContainer.removeItem(value);
                this.onSelectedValueChanged(null);
                this.containerAddRemoveListener.setActive(Boolean.valueOf(false));
            } else if(clickedButton.equals(this.filterButton)) {
                this.tableContainer.removeAllContainerFilters();
                this.tableContainer.addContainerFilter(new ContainerFilter((String)this.filterField.getValue(), this.visibleColums));
            }
        }

    }

    public void addContainerDataChangeListener(ContainerDataChangeListener<K> listener) {
        this.containerDataChangeListener.add(listener);
    }

    public void removeContainerDataChangeListener(ContainerDataChangeListener<K> listener) {
        this.containerDataChangeListener.remove(listener);
    }

    public void confirm(ConfirmEvents.ConfirmEvent event) {
        if(event.isSuccess()) {
            EditItemDialogContent iv = this.dialogContent;
            if(iv.getItemDataSource() instanceof BeanItem) {
                try {
                    this.containerAddRemoveListener.setActive(Boolean.valueOf(true));
                    this.dialogContent.commit();
                    Object e = ((BeanItem)iv.getItemDataSource()).getBean();
                    if(this.tableContainer.containsId(e)) {
                        this.fireUpdateEvent((K) e);
                    } else {
                        this.tableContainer.addBean((K) e);
                    }
                } catch (FieldGroup.CommitException var8) {
                    throw new RuntimeException(var8);
                } finally {
                    this.containerAddRemoveListener.setActive(Boolean.valueOf(false));
                }
            }
        }

    }

    public void valueChange(Property.ValueChangeEvent event) {
        if(event instanceof Field.ValueChangeEvent) {
            Field.ValueChangeEvent fvce = (Field.ValueChangeEvent)event;
            if(this.table.equals(fvce.getSource())) {
                Object value = this.table.getValue();
                this.onSelectedValueChanged((K) value);
            }
        }

    }

    private void onSelectedValueChanged(K selectedValue) {
        this.editButton.setEnabled(this.buttonsPermissions.canEdit(selectedValue));
        this.deleteButton.setEnabled(this.buttonsPermissions.canDelete(selectedValue));
        this.applyPermissions();
    }

    public void setTablePageLength(int value) {
        this.table.setPageLength(value);
    }

    public void setVisibleColumns(Object... visisbleColums) {
        this.visibleColums = visisbleColums;
    }

    private void setVisibilityFilterOption() {
        boolean enableFilter = this.tableContainer instanceof CustomFilterBeanItemContainer;
        this.filterField.setVisible(enableFilter);
        this.filterButton.setVisible(enableFilter);
    }

    private void fireUpdateEvent(K itemId) {
        if(this.tableContainer instanceof TabBeanItemContainer) {
            ((TabBeanItemContainer)this.tableContainer).fireItemUpdated(itemId);
        }

        Iterator var2 = this.containerDataChangeListener.iterator();

        while(var2.hasNext()) {
            ContainerDataChangeListener containerDataChangeListener = (ContainerDataChangeListener)var2.next();
            containerDataChangeListener.update(itemId);
        }

    }

    public void setButtonsPermissions(EditTableComponent.ButtonsPermissions<K> buttonsPermissions) {
        this.buttonsPermissions = buttonsPermissions;
    }

    public interface ButtonsPermissions<K> extends Serializable {
        boolean canEdit(K var1);

        boolean canDelete(K var1);
    }
}
