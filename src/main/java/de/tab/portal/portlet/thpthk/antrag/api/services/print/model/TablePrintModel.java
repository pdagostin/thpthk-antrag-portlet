package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import java.util.ArrayList;
import java.util.List;

public class TablePrintModel {

	public String title;

	public String titleColumn1;
	public String titleColumn2;
	public String titleColumn3;

	public List<TablePrintModelData> data = new ArrayList<TablePrintModelData>();

}
