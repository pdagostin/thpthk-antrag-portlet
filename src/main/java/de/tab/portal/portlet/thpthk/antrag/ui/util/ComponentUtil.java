package de.tab.portal.portlet.thpthk.antrag.ui.util;

import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextField;
import de.tab.vaadin.utils.VaadinUtils;

/**
 * @author hrvoje.krot on 16.3.2016.
 */
public class ComponentUtil {

    public static void setConversionErrorMsg(final String message, final AbstractField<?>... components) {
        for (final AbstractField<?> c : components) {
            c.setConversionError(message);
        }
    }

    public static void setCurrencyConverter(final AbstractField<?>... components) {
        for (final AbstractField<?> c : components) {
            if (c instanceof TextField) {
                ((TextField) c).setConverter(VaadinUtils.FORMAT_CURRENCY);
                c.setStyleName("align-right");
            }
        }
    }
}
