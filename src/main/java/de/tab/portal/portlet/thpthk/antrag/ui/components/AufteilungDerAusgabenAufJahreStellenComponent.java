package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AufteilungDerAusgabenAufJahreComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.util.ComponentUtil;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

import java.math.BigDecimal;

/**
 * Created by Nikola Sapun on 3/3/2016.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "jahr",
                caption = @Caption({"thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.jahr"})),
        @PropertyCaption(propertyId = "gesamtausgaben",
                caption = @Caption({"thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.gesamtausgaben"})),
        @PropertyCaption(propertyId = "zuwendungsausgaben",
                caption = @Caption({"thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.zuwendungsausgaben"}))
})
public class AufteilungDerAusgabenAufJahreStellenComponent extends AbstractCustomComponent<AufteilungDerAusgabenAufJahreComponentModel> {

    private static final long serialVersionUID = -6293012364165332582L;

    private BeanItemContainer<AufteilungDerAusgabenAufJahreComponentModel> modelDataSource;

    @PropertyId("jahr")
    private final ComboBox jahr = VaadinUtils.buildBasicComboBox();

    @PropertyId("gesamtausgaben")
    private final TextField gesamtausgaben = VaadinUtils.buildBasicTextField();

    private TranslationService translationService;

    private boolean edit = false;

    public AufteilungDerAusgabenAufJahreStellenComponent() {
        this.componentModelClass = AufteilungDerAusgabenAufJahreComponentModel.class;
    }

    @Override
    public void init() {
        this.setMargin(true);
        this.setSpacing(true);

        final FormLayout layout = VaadinUtils.buildFormLayout();
        layout.addComponents(this.jahr, this.gesamtausgaben);
        this.addComponent(layout);
    }

    @Override
    public void initializeValidators() {
        VaadinUtils.configField(this.jahr, AufteilungDerAusgabenAufJahreComponentModel.class, "jahr");
        VaadinUtils.configField(this.gesamtausgaben, AufteilungDerAusgabenAufJahreComponentModel.class, "gesamtausgaben");

        this.jahr.addValidator(new Validator() {
            private static final long serialVersionUID = 1469662315859969564L;

            @Override
            public void validate(Object o) throws InvalidValueException {
                for (AufteilungDerAusgabenAufJahreComponentModel model : AufteilungDerAusgabenAufJahreStellenComponent.this.modelDataSource.getItemIds()) {
                    if (model.getJahr() != null && !AufteilungDerAusgabenAufJahreStellenComponent.this.edit && model.getJahr().equals(AufteilungDerAusgabenAufJahreStellenComponent.this.jahr.getValue())) {
                        if (model.getGesamtausgaben().compareTo((BigDecimal) AufteilungDerAusgabenAufJahreStellenComponent.this.gesamtausgaben.getConvertedValue()) == 0) {
                            AufteilungDerAusgabenAufJahreStellenComponent.this.jahr.getContainerDataSource().addItem(model.getJahr());
                            AufteilungDerAusgabenAufJahreStellenComponent.this.jahr.setValue(model.getJahr());
                            AufteilungDerAusgabenAufJahreStellenComponent.this.edit = true;
                        } else
                            throw new InvalidValueException(AufteilungDerAusgabenAufJahreStellenComponent.this.translationService.translate("thpthk.antrag.vorhaben.ausgaben.aufteilungderausgabenaufJahre.jahr.error"));
                    }
                }
            }
        });
    }

    public void initConverters() {
        ComponentUtil.setCurrencyConverter(this.gesamtausgaben);
        ComponentUtil.setConversionErrorMsg(translationService.translate("tab.validation.Number.message"), this.gesamtausgaben);
    }

    public ComboBox getJahr() {
        return this.jahr;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public void setModelDataSource(BeanItemContainer<AufteilungDerAusgabenAufJahreComponentModel> modelDataSource) {
        this.modelDataSource = modelDataSource;
    }

    public void setTranslationService(TranslationService translationService) {
        this.translationService = translationService;
    }
}
