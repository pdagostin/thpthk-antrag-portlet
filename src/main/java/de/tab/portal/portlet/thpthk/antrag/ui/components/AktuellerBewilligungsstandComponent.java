package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.AktuellerBewilligungsstandComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.util.ComponentUtil;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

/**
 * @author hrvoje.krot on 14.3.2016.
 */
@ComponentTranslation(propertyCaptions = {
        @PropertyCaption(propertyId = "gesamtausgaben", caption = @Caption({"thpthk.antrag.stammdaten.ausgaben.aktuellerBewilligungsstand.gesamtausgaben"})),
        @PropertyCaption(propertyId = "gesamtausgabenFoerderfaehig", caption = @Caption({"thpthk.antrag.stammdaten.ausgaben.aktuellerBewilligungsstand.gesamtausgabenFoerderfaehig"})),
        @PropertyCaption(propertyId = "foerdersatz", caption = @Caption({"thpthk.antrag.stammdaten.ausgaben.aktuellerBewilligungsstand.foerdersatz"}))})
public class AktuellerBewilligungsstandComponent extends FormLayout implements Item.Editor {

    @PropertyId("gesamtausgaben")
    private final TextField gesamtausgaben = VaadinUtils.buildBasicTextField();

    @PropertyId("gesamtausgabenFoerderfaehig")
    private final TextField gesamtausgabenFoerderfaehig = VaadinUtils.buildBasicTextField();

    @PropertyId("foerdersatz")
    private final TextField foerdersatz = VaadinUtils.buildBasicTextField();

    private TranslationService translationService;

    public AktuellerBewilligungsstandComponent() {
        this.buildLayout();
        this.initValidators();
    }

    public void buildLayout() {
        this.setMargin(true);
        this.setSpacing(true);

        this.addComponents(this.gesamtausgaben, this.gesamtausgabenFoerderfaehig, this.foerdersatz);
    }

    private void initValidators() {
        VaadinUtils.configField(this.gesamtausgaben, AktuellerBewilligungsstandComponentModel.class, "gesamtausgaben");
        VaadinUtils.configField(this.gesamtausgabenFoerderfaehig, AktuellerBewilligungsstandComponentModel.class, "gesamtausgabenFoerderfaehig");
        VaadinUtils.configField(this.foerdersatz, AktuellerBewilligungsstandComponentModel.class, "foerdersatz");
    }

    public void initConverters() {
        ComponentUtil.setCurrencyConverter(this.gesamtausgaben, this.gesamtausgabenFoerderfaehig);
        ComponentUtil.setConversionErrorMsg(translationService.translate("tab.validation.Number.message"),
                this.gesamtausgaben, this.gesamtausgabenFoerderfaehig, this.foerdersatz);
    }

    @Override
    public void setItemDataSource(final Item newDataSource) {
        final BeanItem beanItem = (BeanItem<?>) newDataSource;
        BeanFieldGroup.bindFieldsUnbuffered(beanItem.getBean(), this);
    }

    @Override
    public Item getItemDataSource() {
        throw new UnsupportedOperationException();
    }

    public void setTranslationService(TranslationService translationService) {
        this.translationService = translationService;
    }
}
