package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoVorhabensbeschreibungKurzLangPrintModel {

	public String title = "Vorhabensbeschreibung";
	public String beschreibungLangBezeichnung =
			"Detaillierte Beschreibung";
	public String beschreibungLang;

	public String beschreibungKurzBezeichnung =
			"Kurze Beschreibung";
	public String beschreibungKurz;

}
