package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoFinanzierungsplanMittelPrintModelData {

	public String jahr;
	public String eumittel;
	public String kommunalmittel;
	public String bundesmittel;
	public String privatemittel;
	public String landesmittel;
	public String grunderwerb;
	public String foerderfaehigInvest;
	public String zuschuss;

}
