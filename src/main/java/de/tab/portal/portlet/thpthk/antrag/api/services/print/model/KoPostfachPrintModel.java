package de.tab.portal.portlet.thpthk.antrag.api.services.print.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KoPostfachPrintModel {

	public String title = "Postfach";

	public String plzBezeichnung = "PLZ";
	public String plz;

	public String postfachBezeichnung = "Postfach";
	public String postfach;

}
