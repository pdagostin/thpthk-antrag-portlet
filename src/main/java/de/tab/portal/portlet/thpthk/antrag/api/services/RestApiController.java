package de.tab.portal.portlet.thpthk.antrag.api.services;

import de.tab.portal.commons.api.PortletRestApi;
import de.tab.portal.portlet.thpthk.antrag.api.services.PdfPrinter.PrintResult;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
public class RestApiController implements PortletRestApi {

	@Inject
	private PdfPrinter pdfPrinter;

	@Inject
	private CheckValid checkValid;

	@Override
	public boolean formIsValid(final Long vorhabenId, final String aufgabenId, final String userName) {
		return this.checkValid.isValid(vorhabenId);
	}

	@Override
	public void aufgabeAbschliessen(final Long vorhabenId, final String aufgabenId, final String userName,
			final String selektierteDokumentKlassenKennung) {
		// TODO Auto-generated method stub

	}

	@Override
	public void aufgabeVerwerfen(final Long vorhabenId, final String aufgabenId, final String userName,
			final String selektierteDokumentKlassenKennung) {
		// TODO Auto-generated method stub

	}

	@Override
	public ResponseEntity<InputStreamResource> printPdfRequest(final Long vorhabenId, final String aufgabenId, final String userName,
			final String dokumentKlassenKennung,
			final String selektierteDokumentKlassenKennung) throws Exception {
		final HttpHeaders headers = this.createNoCacheHeaders();

		final PrintResult result = this.pdfPrinter.print(vorhabenId, aufgabenId, dokumentKlassenKennung);

		final InputStreamResource body = result.body;
		final long contentLength = result.contentLength;

		return ResponseEntity
				.ok()
				.headers(headers)
				.contentLength(contentLength).contentType(new MediaType("application", "pdf"))
				.body(body);
	}

	@Override
	public ResponseEntity<Void> exportXmlRequest(final Long vorhabenId, final String aufgabenId, final String userName) throws Exception {
		return ResponseEntity.noContent().build();
	}

	@Override
	public boolean importXml(@RequestParam("vorhabenId") final Long vorhabenId, @RequestParam("aufgabenId") final String aufgabenId, @RequestParam("userName") final String userName, @RequestBody final byte[] xml) throws Exception {
		return false;
	}

	@Override
	public byte[] printPdf(final Long vorhabenId, final String aufgabenId, final String userName, final String dokumentKlassenKennung, final String selektierteDokumentKlassenKennung) throws Exception {
		throw new UnsupportedOperationException();
	}

	@Override
	public byte[] exportXml(final Long vorhabenId, final String aufgabenId, final String userName) throws Exception {
		throw new UnsupportedOperationException();
	}

	private HttpHeaders createNoCacheHeaders() {
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		return headers;
	}
}