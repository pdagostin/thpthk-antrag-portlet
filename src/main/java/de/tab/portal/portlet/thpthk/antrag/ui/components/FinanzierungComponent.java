package de.tab.portal.portlet.thpthk.antrag.ui.components;

import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import de.tab.portal.portlet.thpthk.antrag.ui.beans.FinanzierungComponentModel;
import de.tab.portal.portlet.thpthk.antrag.ui.util.ComponentUtil;
import de.tab.vaadin.utils.VaadinUtils;
import de.vollmar.vaadin.spring.i18n.annotations.Caption;
import de.vollmar.vaadin.spring.i18n.annotations.ComponentTranslation;
import de.vollmar.vaadin.spring.i18n.annotations.PropertyCaption;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;

import java.math.BigDecimal;

/**
 * Created by Nikola Sapun on 14/3/2016.
 */
@ComponentTranslation(propertyCaptions = {
		@PropertyCaption(propertyId = "jahr", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.jahr"})),
		@PropertyCaption(propertyId = "eumittel", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.eumittel"})),
		@PropertyCaption(propertyId = "bundesmittel", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.bundesmittel"})),
		@PropertyCaption(propertyId = "landesmittel", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.landesmittel"})),
		@PropertyCaption(propertyId = "zuschuss", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.zuschuss"})),
		@PropertyCaption(propertyId = "grunderwerb", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.grunderwerb"})),
		@PropertyCaption(propertyId = "kommunalmittel", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.kommunalmittel"})),
		@PropertyCaption(propertyId = "privatemittel", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.privatemittel"})),
		@PropertyCaption(propertyId = "foerderfaehigInvest", caption = @Caption({"thpthk.antrag.stammdaten.finanzierung.foerderfaehigInvest"}))
})
public class FinanzierungComponent extends AbstractCustomComponent<FinanzierungComponentModel> {

	private BeanItemContainer<FinanzierungComponentModel> modelDataSource;

	@PropertyId("jahr")
	private final ComboBox jahr = VaadinUtils.buildBasicComboBox();

	@PropertyId("eumittel")
	private final TextField eumittel = VaadinUtils.buildBasicTextField();

	@PropertyId("bundesmittel")
	private final TextField bundesmittel = VaadinUtils.buildBasicTextField();

	@PropertyId("landesmittel")
	private final TextField landesmittel = VaadinUtils.buildBasicTextField();

	@PropertyId("zuschuss")
	private final TextField zuschuss = VaadinUtils.buildBasicTextField();

	@PropertyId("grunderwerb")
	private final TextField grunderwerb = VaadinUtils.buildBasicTextField();

	@PropertyId("kommunalmittel")
	private final TextField kommunalmittel = VaadinUtils.buildBasicTextField();

	@PropertyId("privatemittel")
	private final TextField privatemittel = VaadinUtils.buildBasicTextField();

	@PropertyId("foerderfaehigInvest")
	private final TextField foerderfaehigInvest = VaadinUtils.buildBasicTextField();

	final HorizontalLayout horizontalLayout = new HorizontalLayout(
			this.zuschuss,
			this.grunderwerb);

	private boolean edit = false;

	private TranslationService translationService;

	public FinanzierungComponent() {
		this.componentModelClass = FinanzierungComponentModel.class;
	}

	@Override
	public void init() {
		setMargin(true);
		setSpacing(true);

		final MarginInfo formLayout1MarginInfo = new MarginInfo(true, true, false, true);
		final FormLayout formLayout1 = VaadinUtils.buildFormLayout();
		formLayout1.setMargin(formLayout1MarginInfo);
		formLayout1.addComponents(this.jahr, this.eumittel, this.bundesmittel, this.landesmittel);
		this.addComponent(formLayout1);

		final MarginInfo horizontalLayoutMarginInfo = new MarginInfo(false, true, false, true);
		horizontalLayout.setMargin(horizontalLayoutMarginInfo);
		horizontalLayout.setSpacing(true);
		this.addComponent(horizontalLayout);

		final MarginInfo formLayout2MarginInfo = new MarginInfo(false, true, true, true);
		final FormLayout formLayout2 = VaadinUtils.buildFormLayout();
		formLayout2.setMargin(formLayout2MarginInfo);
		formLayout2.addComponents(this.kommunalmittel, this.privatemittel, this.foerderfaehigInvest);
		this.addComponent(formLayout2);

		this.zuschuss.setWidth(30, Sizeable.Unit.EX);
		this.grunderwerb.setWidth(30, Sizeable.Unit.EX);

		this.initListeners();
	}

	public void initConverters() {
		ComponentUtil.setCurrencyConverter(this.eumittel, this.bundesmittel, this.landesmittel, this.grunderwerb,
				this.zuschuss, this.kommunalmittel, this.privatemittel, this.foerderfaehigInvest);
		ComponentUtil.setConversionErrorMsg(translationService.translate("tab.validation.Number.message"),
				this.eumittel, this.bundesmittel, this.landesmittel, this.grunderwerb, this.zuschuss, this.kommunalmittel,
				this.privatemittel, this.foerderfaehigInvest);
	}

	private void initListeners() {
		this.eumittel.addBlurListener(new FieldEvents.BlurListener() {

			@Override
			public void blur(FieldEvents.BlurEvent blurEvent) {
				calculateZuschuss();
				calculateFoerderfaehigInvest();
			}
		});
		this.bundesmittel.addBlurListener(new FieldEvents.BlurListener() {
			@Override
			public void blur(FieldEvents.BlurEvent blurEvent) {
				calculateZuschuss();
				calculateFoerderfaehigInvest();
			}
		});
		this.landesmittel.addBlurListener(new FieldEvents.BlurListener() {
			@Override
			public void blur(FieldEvents.BlurEvent blurEvent) {
				calculateZuschuss();
				calculateFoerderfaehigInvest();
			}
		});

		this.kommunalmittel.addBlurListener(new FieldEvents.BlurListener() {
			@Override
			public void blur(FieldEvents.BlurEvent blurEvent) {
				calculateFoerderfaehigInvest();
			}
		});

		this.privatemittel.addBlurListener(new FieldEvents.BlurListener() {
			@Override
			public void blur(FieldEvents.BlurEvent blurEvent) {
				calculateFoerderfaehigInvest();
			}
		});
	}

	@Override
	public void initializeValidators() {
		VaadinUtils.configField(this.jahr, FinanzierungComponentModel.class, "jahr");
		VaadinUtils.configField(this.eumittel, FinanzierungComponentModel.class, "eumittel");
		VaadinUtils.configField(this.bundesmittel, FinanzierungComponentModel.class, "bundesmittel");
		VaadinUtils.configField(this.landesmittel, FinanzierungComponentModel.class, "landesmittel");
		VaadinUtils.configField(this.grunderwerb, FinanzierungComponentModel.class, "grunderwerb");
		VaadinUtils.configField(this.kommunalmittel, FinanzierungComponentModel.class, "kommunalmittel");
		VaadinUtils.configField(this.privatemittel, FinanzierungComponentModel.class, "privatemittel");

		this.jahr.addValidator(new Validator() {

			@Override
			public void validate(Object o) throws InvalidValueException {
				for (FinanzierungComponentModel model : FinanzierungComponent.this.modelDataSource.getItemIds()) {
					if (model.getJahr() != null && !FinanzierungComponent.this.edit && model.getJahr().equals(FinanzierungComponent.this.jahr.getValue())) {
						if (model.getEumittel().compareTo((BigDecimal) FinanzierungComponent.this.eumittel.getConvertedValue()) == 0 &&
								model.getBundesmittel().compareTo((BigDecimal) FinanzierungComponent.this.bundesmittel.getConvertedValue()) == 0 &&
								model.getLandesmittel().compareTo((BigDecimal) FinanzierungComponent.this.landesmittel.getConvertedValue()) == 0 &&
								(model.getGrunderwerb() == null || (model.getGrunderwerb() != null && model.getGrunderwerb().compareTo((BigDecimal) FinanzierungComponent.this.grunderwerb.getConvertedValue()) == 0)) &&
								(model.getKommunalmittel() == null || (model.getKommunalmittel() != null && model.getKommunalmittel().compareTo((BigDecimal) FinanzierungComponent.this.kommunalmittel.getConvertedValue()) == 0)) &&
								(model.getPrivatemittel() == null || (model.getPrivatemittel() != null && model.getPrivatemittel().compareTo((BigDecimal) FinanzierungComponent.this.privatemittel.getConvertedValue()) == 0))) {
							FinanzierungComponent.this.jahr.getContainerDataSource().addItem(model.getJahr());
							FinanzierungComponent.this.jahr.setValue(model.getJahr());
							FinanzierungComponent.this.edit = true;
						} else {
							throw new InvalidValueException(FinanzierungComponent.this.translationService.translate("thpthk.antrag.stammdaten.finanzierung.jahr.error"));
						}
					}
				}
			}
		});
	}

	private void calculateFoerderfaehigInvest() {
		BigDecimal foerderfaehigInvestValue = BigDecimal.ZERO;
		BigDecimal eumittelValue = this.eumittel.isValid() ? (BigDecimal) this.eumittel.getConvertedValue() : null;
		BigDecimal bundesmittelValue = this.bundesmittel.isValid() ? (BigDecimal) this.bundesmittel.getConvertedValue() : null;
		BigDecimal landesmittelValue = this.landesmittel.isValid() ? (BigDecimal) this.landesmittel.getConvertedValue() : null;
		BigDecimal kommunalmittelValue = this.kommunalmittel.isValid() ? (BigDecimal) this.kommunalmittel.getConvertedValue() : null;
		BigDecimal privatemittelValue = this.privatemittel.isValid() ? (BigDecimal) this.privatemittel.getConvertedValue() : null;

		if (eumittelValue != null) {
			foerderfaehigInvestValue = foerderfaehigInvestValue.add(eumittelValue);
		}
		if (bundesmittelValue != null) {
			foerderfaehigInvestValue = foerderfaehigInvestValue.add(bundesmittelValue);
		}
		if (landesmittelValue != null) {
			foerderfaehigInvestValue = foerderfaehigInvestValue.add(landesmittelValue);
		}
		if (kommunalmittelValue != null) {
			foerderfaehigInvestValue = foerderfaehigInvestValue.add(kommunalmittelValue);
		}
		if (privatemittelValue != null) {
			foerderfaehigInvestValue = foerderfaehigInvestValue.add(privatemittelValue);
		}
		foerderfaehigInvest.setConvertedValue(foerderfaehigInvestValue);
	}

	private void calculateZuschuss() {
		BigDecimal zuschussValue = BigDecimal.ZERO;
		BigDecimal eumittelValue = this.eumittel.isValid() ? (BigDecimal) this.eumittel.getConvertedValue() : null;
		BigDecimal bundesmittelValue = this.bundesmittel.isValid() ? (BigDecimal) this.bundesmittel.getConvertedValue() : null;
		BigDecimal landesmittelValue = this.landesmittel.isValid() ? (BigDecimal) this.landesmittel.getConvertedValue() : null;

		if (eumittelValue != null) {
			zuschussValue = zuschussValue.add(eumittelValue);
		}
		if (bundesmittelValue != null) {
			zuschussValue = zuschussValue.add(bundesmittelValue);
		}
		if (landesmittelValue != null) {
			zuschussValue = zuschussValue.add(landesmittelValue);
		}
		zuschuss.setConvertedValue(zuschussValue);
	}

	public ComboBox getJahr() {
		return this.jahr;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public void setModelDataSource(BeanItemContainer<FinanzierungComponentModel> modelDataSource) {
		this.modelDataSource = modelDataSource;
	}

	public void setTranslationService(TranslationService translationService) {
		this.translationService = translationService;
	}

	@Override
	protected void disableFields() {
		this.zuschuss.setEnabled(false);
		this.foerderfaehigInvest.setEnabled(false);
	}
}
