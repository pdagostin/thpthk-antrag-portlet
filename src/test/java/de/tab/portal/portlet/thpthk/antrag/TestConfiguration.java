package de.tab.portal.portlet.thpthk.antrag;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.tab.portal.client.katalog.KatalogClientService;
import de.tab.portal.client.katalog.model.KatalogKeyText;
import de.tab.portal.client.persistence.PortalPersistenceRestClient;
import de.tab.portal.commons.KeyText;
import de.vollmar.vaadin.spring.i18n.services.TranslationService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Configuration
public class TestConfiguration {
	@Bean
	public PortalPersistenceRestClient portalPersistenceRestClient() {
		return Mockito.mock(PortalPersistenceRestClient.class);
	}

	@Bean
	public String portalPersistenzBaseUrl() {
		return "http://notexistinghost:20001/portal-persistence-service/";
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public KatalogClientService katalogClientService() {
		return Mockito.mock(KatalogClientService.class);
	}

	@Bean
	public TranslationService translationService() {
		return Mockito.mock(TranslationService.class);
	}

	@Bean
	public ObjectMapper objectMapper() {
		final ObjectMapper objectMapper = new ObjectMapper();

		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		final SimpleModule simpleModule = new SimpleModule();

		simpleModule.addDeserializer(KeyText.class, new JsonDeserializer<KeyText>() {
			@Override
			public KeyText deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
				final String readValueAs = jp.readValueAs(String.class);
				return new KatalogKeyText(readValueAs);
			}
		});

		simpleModule.addSerializer(KeyText.class, new JsonSerializer<KeyText>() {
			@Override
			public void serialize(final KeyText value, final JsonGenerator jgen, final SerializerProvider provider) throws IOException,
					JsonProcessingException {
				jgen.writeString(value.getKey());
			}
		});

		objectMapper.registerModule(simpleModule);

		return objectMapper;
	}
}