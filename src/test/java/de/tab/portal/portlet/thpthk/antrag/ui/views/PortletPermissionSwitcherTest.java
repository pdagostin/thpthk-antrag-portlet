package de.tab.portal.portlet.thpthk.antrag.ui.views;

import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import de.tab.vaadin.security.PortletPermissionSwitcher;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PortletPermissionSwitcherTest {

	private PortletPermissionSwitcher pps;
	private final Layout layout = new VerticalLayout();

	@Before
	public void setUp() throws Exception {
		this.layout.removeAllComponents();
		this.pps = new PortletPermissionSwitcher(this.layout);
	}

	@Test
	public void test_zero_components() {
		this.pps.reset();

		assertEquals(0, this.layout.getComponentCount());

		this.pps.accessDenied();

		assertEquals(1, this.layout.getComponentCount());

		this.pps.reset();

		assertEquals(0, this.layout.getComponentCount());
	}

	@Test
	public void test_components() {
		this.layout.addComponent(new Label());
		this.layout.addComponent(new Panel());

		this.pps.reset();

		assertEquals(2, this.layout.getComponentCount());

		this.pps.reset();

		assertEquals(2, this.layout.getComponentCount());

		this.pps.reset();

		assertEquals(2, this.layout.getComponentCount());

		this.pps.reset();

		assertEquals(2, this.layout.getComponentCount());

		this.pps.accessDenied();

		assertEquals(1, this.layout.getComponentCount());

		this.pps.reset();

		assertEquals(2, this.layout.getComponentCount());
	}

}
